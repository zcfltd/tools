/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.netbeans.service.bacnet;


import java.util.Objects;


/**
 * A descriptor of a BACnet device.
 *
 * @author Claudio Rosati
 */
public class BACnetDeviceDescriptor {

	private final int deviceID;
	private final String ip;
	private final int port;

	public BACnetDeviceDescriptor( String ip, int port, int deviceID ) {
		this.ip = ip;
		this.port = port;
		this.deviceID = deviceID;
	}

	@Override
	@SuppressWarnings( "AccessingNonPublicFieldOfAnotherObject" )
	public boolean equals( Object obj ) {

		if ( obj == null ) {
			return false;
		}

		if ( getClass() != obj.getClass() ) {
			return false;
		}

		final BACnetDeviceDescriptor other = (BACnetDeviceDescriptor) obj;

		if ( this.deviceID != other.deviceID ) {
			return false;
		}

		if ( !Objects.equals(this.ip, other.ip) ) {
			return false;
		}

		if ( this.port != other.port ) {
			return false;
		}

		return true;

	}

	public int getDeviceID() {
		return deviceID;
	}

	public String getIp() {
		return ip;
	}

	public int getPort() {
		return port;
	}

	@Override
	public int hashCode() {

		int hash = 3;

		hash = 37 * hash + this.deviceID;
		hash = 37 * hash + Objects.hashCode(this.ip);
		hash = 37 * hash + this.port;

		return hash;

	}

}
