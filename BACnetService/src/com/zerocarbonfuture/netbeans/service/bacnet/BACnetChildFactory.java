/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.netbeans.service.bacnet;


import com.zerocarbonfuture.netbeans.service.bacnet.nodes.DeviceNode;
import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;


/**
 * Loader of registered (and discoverable) BACnet devices.
 *
 * @author Claudio Rosati
 */
public class BACnetChildFactory extends ChildFactory<BACnetDeviceDescriptor> {

	private BACnetService service = null;

	public static BACnetChildFactory getInstance() {
		return BACnetChildFactoryHolder.INSTANCE;
	}

	private BACnetChildFactory() {
	}

	public void setService( BACnetService service ) {
		if ( this.service == null ) {
			this.service = service;
		} else {
			throw new IllegalStateException("BACnet service already set.");
		}
	}

	public void update() {
		refresh(false);
	}

	@Override
	protected boolean createKeys( List<BACnetDeviceDescriptor> toPopulate ) {

		if ( service != null ) {
			toPopulate.addAll(service.getRemoteDeviceMap().keySet());
		}

		return true;

	}

	@Override
	protected Node createNodeForKey( BACnetDeviceDescriptor descriptor ) {

		if ( service != null ) {

			return new DeviceNode(descriptor, service.getRemoteDeviceMap().get(descriptor), service);

		}

		return null;

	}

	private interface BACnetChildFactoryHolder {

		BACnetChildFactory INSTANCE = new BACnetChildFactory();

	}

}
