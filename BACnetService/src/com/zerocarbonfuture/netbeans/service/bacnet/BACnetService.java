/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.netbeans.service.bacnet;


import com.serotonin.bacnet4j.LocalDevice;
import com.serotonin.bacnet4j.RemoteDevice;
import com.serotonin.bacnet4j.base.BACnetUtils;
import com.serotonin.bacnet4j.event.DeviceEventAdapter;
import com.serotonin.bacnet4j.exception.BACnetException;
import com.serotonin.bacnet4j.npdu.ip.IpNetwork;
import com.serotonin.bacnet4j.service.confirmed.SubscribeCOVRequest;
import com.serotonin.bacnet4j.service.unconfirmed.WhoIsRequest;
import com.serotonin.bacnet4j.transport.Transport;
import com.serotonin.bacnet4j.type.Encodable;
import com.serotonin.bacnet4j.type.constructed.Address;
import com.serotonin.bacnet4j.type.constructed.PropertyValue;
import com.serotonin.bacnet4j.type.constructed.SequenceOf;
import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;
import com.serotonin.bacnet4j.type.primitive.ObjectIdentifier;
import com.serotonin.bacnet4j.type.primitive.UnsignedInteger;
import com.serotonin.bacnet4j.util.RequestUtils;
import com.zerocarbonfuture.netbeans.service.bacnet.nodes.DeviceNode;
import com.zerocarbonfuture.netbeans.service.bacnet.nodes.ObjectNode;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import org.netbeans.api.core.ide.ServicesTabNodeRegistration;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Utilities;


/**
 * The BACnet Service main node.
 *
 * @author Claudio Rosati
 */
@SuppressWarnings( { "CloneableImplementsClone", "serial" } )
@ServicesTabNodeRegistration(
	 displayName = "BACnet",
	 iconResource = "com/zerocarbonfuture/netbeans/service/bacnet/resources/BACnet.png",
	 name = "BACnet Service"
)
public class BACnetService extends AbstractNode {

	private static final int BACNET_SERVICE_DEVICE_ID = 7777;
	private static final Logger LOGGER = Logger.getLogger(BACnetService.class.getName());

	private LocalDevice bacnetServiceLocalDevice;
	private final Map<BACnetDeviceDescriptor, RemoteDevice> remoteDeviceMap = Collections.synchronizedMap(new HashMap<>(1));
	private final Map<Integer, ObjectNode> subscribedNodeMap = new HashMap<>(1);

	@SuppressWarnings( "LeakingThisInConstructor" )
	public BACnetService() {

		super(Children.create(BACnetChildFactory.getInstance(), true));

		BACnetChildFactory.getInstance().setService(this);

		setDisplayName("BACnet");
		setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/BACnet.png");

		bacnetServiceLocalDevice = new LocalDevice(BACNET_SERVICE_DEVICE_ID, new Transport(new IpNetwork()));

		try {
			bacnetServiceLocalDevice.initialize();
			bacnetServiceLocalDevice.getEventHandler().addListener(new BACnetDeviceListener());
		} catch ( Exception ex ) {
			LOGGER.log(Level.SEVERE, null, ex);
		}

	}

	public void addDevice( BACnetDeviceDescriptor descriptor ) {

		if ( descriptor == null ) {
			return;
		}

		if ( !remoteDeviceMap.containsKey(descriptor) ) {

			Address address = new Address(BACnetUtils.dottedStringToBytes(descriptor.getIp()), descriptor.getPort());

			try {
				addDevice(
					descriptor,
					bacnetServiceLocalDevice.findRemoteDevice(address, null, descriptor.getDeviceID())
				);
			} catch ( BACnetException ex ) {
				LOGGER.log(Level.WARNING, null, ex);
			}

		}

	}

	@Override
	public Action[] getActions( boolean context ) {
		return Utilities.actionsForPath("BACnet/Actions/Root").toArray(new Action[] {});
	}

	public Map<PropertyIdentifier, Encodable> getProperties( RemoteDevice remoteDevice ) throws BACnetException {
		return RequestUtils.getProperties(bacnetServiceLocalDevice, remoteDevice, null, PropertyIdentifier.ALL);
	}

	public Map<PropertyIdentifier, Encodable> getProperties( RemoteDevice remoteDevice, ObjectIdentifier objectIdentifier ) throws BACnetException {
		return RequestUtils.getProperties(bacnetServiceLocalDevice, remoteDevice, objectIdentifier, null, PropertyIdentifier.ALL);
	}

	public Encodable getProperty( RemoteDevice remoteDevice, ObjectIdentifier objectIdentifier, PropertyIdentifier propertyIdentifier ) throws BACnetException {
		return RequestUtils.getProperty(bacnetServiceLocalDevice, remoteDevice, objectIdentifier, propertyIdentifier);
	}

	public Map<BACnetDeviceDescriptor, RemoteDevice> getRemoteDeviceMap() {
		return Collections.unmodifiableMap(remoteDeviceMap);
	}

	public void scan() {
		try {
			bacnetServiceLocalDevice.sendGlobalBroadcast(new WhoIsRequest());
		} catch ( BACnetException ex ) {
			LOGGER.log(Level.WARNING, null, ex);
		}
	}

	public void subscribeCOV( int subscriberProcessIdentifier, ObjectIdentifier objectIdentifier, RemoteDevice remoteDevice, ObjectNode node ) {

		try {

			UnsignedInteger spid = new UnsignedInteger(subscriberProcessIdentifier);

			bacnetServiceLocalDevice.send(
				remoteDevice,
				new SubscribeCOVRequest(
					spid,
					objectIdentifier,
					new com.serotonin.bacnet4j.type.primitive.Boolean(true),
					new UnsignedInteger(0)
				)
			);
			subscribedNodeMap.put(subscriberProcessIdentifier, node);

		} catch ( BACnetException ex ) {
			LOGGER.log(
				Level.WARNING,
				"{0} not subscribed [OID: {1}, PID: {2,number,##########0}].",
				new Object[] {
					node.getDisplayName(),
					objectIdentifier.toString(),
					subscriberProcessIdentifier
				}
			);
		}

	}

	private void addDevice( RemoteDevice remoteDevice ) {
		addDevice(
			new BACnetDeviceDescriptor(
				remoteDevice.getAddress().getMacAddress().getInetAddress().getHostAddress(),
				remoteDevice.getAddress().getMacAddress().getPort(),
				remoteDevice.getInstanceNumber()
			),
			remoteDevice
		);
	}

	private void addDevice( BACnetDeviceDescriptor descriptor, RemoteDevice remoteDevice ) {

		try {

			RequestUtils.getExtendedDeviceInformation(bacnetServiceLocalDevice, remoteDevice);

			remoteDeviceMap.put(descriptor, remoteDevice);
			BACnetChildFactory.getInstance().update();

		} catch ( BACnetException ex ) {
			LOGGER.log(Level.WARNING, null, ex);
		}

	}

	private class BACnetDeviceListener extends DeviceEventAdapter {

		@Override
		public void covNotificationReceived(
			UnsignedInteger subscriberProcessIdentifier,
			RemoteDevice initiatingDevice,
			ObjectIdentifier monitoredObjectIdentifier,
			UnsignedInteger timeRemaining,
			SequenceOf<PropertyValue> listOfValues
		) {

			ObjectNode node = subscribedNodeMap.get(subscriberProcessIdentifier.intValue());

			if ( node != null ) {
				node.covNotificationReceived(initiatingDevice, monitoredObjectIdentifier, timeRemaining, listOfValues);
			}

		}

		@Override
		public void iAmReceived( RemoteDevice d ) {

			for ( Node node : getChildren().getNodes() ) {
				if ( node instanceof DeviceNode ) {
					if ( d.equals(( (DeviceNode) node ).getDevice()) ) {
						return;
					}
				}
			}

			addDevice(d);

		}

	}	//	class BACnetDeviceListenerss BACnetDeviceListenerer

}	//	class BACnetService	//	class BACnetServicece
