/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.netbeans.service.bacnet.actions;


import com.zerocarbonfuture.netbeans.service.bacnet.BACnetService;
import com.zerocarbonfuture.netbeans.service.bacnet.actions.dialogs.AddDevicePanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;
import org.openide.util.Utilities;


/**
 * @author Claudio.Rosati@zerocarbonfuture.com
 */
@ActionID(
	 category = "BACnet",
	 id = "com.zerocarbonfuture.netbeans.service.bacnet.actions.AddDeviceAction"
)
@ActionRegistration(
	 displayName = "#CTL_AddDeviceAction"
)
@ActionReferences( {
	@ActionReference( path = "BACnet/Actions/Root", position = 100 )
} )
@Messages( "CTL_AddDeviceAction=Add Device…" )
public final class AddDeviceAction implements ActionListener {

	private AddDevicePanel addDevicePanel = null;

	@Override
	public void actionPerformed( ActionEvent e ) {

		addDevicePanel = new AddDevicePanel();

		DialogDescriptor dd = new DialogDescriptor(addDevicePanel, "Add BACnet Device", true, evt -> dialogClosed(evt));

		addDevicePanel.setDialogDescriptor(dd);
		DialogDisplayer.getDefault().notifyLater(dd);

	}

	private DialogDescriptor dialogClosed( ActionEvent e ) {

		if ( e.getSource() == DialogDescriptor.CANCEL_OPTION ) {
			return null;
		} else {

			BACnetService service = Utilities.actionsGlobalContext().lookup(BACnetService.class);

			service.addDevice(addDevicePanel.getDescriptor());

		}

		return null;

	}

}
