/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.netbeans.service.bacnet.actions;


import com.zerocarbonfuture.netbeans.service.bacnet.BACnetService;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;


/**
 * @author Claudio.Rosati@zerocarbonfuture.com
 */
@ActionID(
	 category = "BACnet",
	 id = "com.zerocarbonfuture.netbeans.service.bacnet.actions.ScanAction"
)
@ActionRegistration(
	 displayName = "#CTL_ScanAction"
)
@ActionReferences( {
	@ActionReference( path = "BACnet/Actions/Root", position = 1000, separatorBefore = 999 )
} )
@NbBundle.Messages( "CTL_ScanAction=Scan" )
public class ScanAction implements ActionListener {

	@Override
	public void actionPerformed( ActionEvent e ) {

		BACnetService service = Utilities.actionsGlobalContext().lookup(BACnetService.class);

		service.scan();

	}

}
