/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.netbeans.service.bacnet.nodes;


import com.serotonin.bacnet4j.RemoteDevice;
import com.serotonin.bacnet4j.RemoteObject;
import com.serotonin.bacnet4j.type.enumerated.ObjectType;
import com.serotonin.bacnet4j.type.primitive.ObjectIdentifier;
import com.zerocarbonfuture.netbeans.service.bacnet.BACnetService;
import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;


/**
 * Loader of BACnet device's objects.
 *
 * @author Claudio Rosati
 */
public class DeviceChildFactory extends ChildFactory<RemoteObject> {

	private final RemoteDevice device;
	private final BACnetService service;

	public DeviceChildFactory( RemoteDevice device, BACnetService service ) {
		this.device = device;
		this.service = service;
	}

	@Override
	protected boolean createKeys( List<RemoteObject> toPopulate ) {

		if ( device != null ) {
			device
				.getObjects()
				.stream()
				.filter(ro -> !ObjectType.device.equals(ro.getObjectIdentifier().getObjectType()))
				.sorted((ro1, ro2) -> { 

					ObjectIdentifier id1 = ro1.getObjectIdentifier();
					ObjectIdentifier id2 = ro2.getObjectIdentifier();
					int ot1 = id1.getObjectType().intValue();
					int ot2 = id2.getObjectType().intValue();

					if ( ot1 != ot2 ) {
						return ot1 - ot2;
					} else {
						return id1.getInstanceNumber() - id2.getInstanceNumber();
					}
					
				})
				.forEach(ro -> toPopulate.add(ro));
		}

		return true;

	}

	@Override
	protected Node createNodeForKey( RemoteObject remoteObject ) {
		return new ObjectNode(remoteObject, device, service);
	}

}
