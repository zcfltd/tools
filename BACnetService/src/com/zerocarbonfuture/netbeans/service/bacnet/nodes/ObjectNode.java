/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.netbeans.service.bacnet.nodes;


import com.serotonin.bacnet4j.RemoteDevice;
import com.serotonin.bacnet4j.RemoteObject;
import com.serotonin.bacnet4j.exception.BACnetException;
import com.serotonin.bacnet4j.type.Encodable;
import com.serotonin.bacnet4j.type.constructed.BACnetError;
import com.serotonin.bacnet4j.type.constructed.PropertyValue;
import com.serotonin.bacnet4j.type.constructed.SequenceOf;
import com.serotonin.bacnet4j.type.constructed.StatusFlags;
import com.serotonin.bacnet4j.type.enumerated.ObjectType;
import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;
import com.serotonin.bacnet4j.type.primitive.ObjectIdentifier;
import com.serotonin.bacnet4j.type.primitive.UnsignedInteger;
import com.serotonin.bacnet4j.util.PropertyValues;
import com.zerocarbonfuture.netbeans.service.bacnet.BACnetService;
import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.swing.SwingUtilities;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;


/**
 * A node representing a BACnet device's object.
 *
 * @author Claudio Rosati
 */
public class ObjectNode extends AbstractNode {

	private static final Logger LOGGER = Logger.getLogger(ObjectNode.class.getName());
	private static final String REMOTE_PROPERTIES = "Remote Properties";

	private static int subscriberProcessIdentifier = 0;

	private Map<PropertyIdentifier, Encodable> propertyMap = null;
	private final RemoteDevice remoteDevice;
	private final RemoteObject remoteObject;
	private final BACnetService service;

	@SuppressWarnings( { "ValueOfIncrementOrDecrementUsed", "LeakingThisInConstructor" } )
	public ObjectNode( RemoteObject remoteObject, RemoteDevice remoteDevice, BACnetService service ) {

		super(Children.LEAF);

		this.remoteObject = remoteObject;
		this.remoteDevice = remoteDevice;
		this.service = service;

		ObjectIdentifier objectIdentifier = remoteObject.getObjectIdentifier();

		try {
			propertyMap = service.getProperties(remoteDevice, objectIdentifier);
			propertyMap = propertyMap
				.entrySet()
				.stream()
				.filter(e -> ( e.getValue() != null ) && !( e.getValue() instanceof BACnetError ))
				.collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
		} catch ( BACnetException ex ) {
			LOGGER.log(Level.WARNING, null, ex);
		}

		String displayName = MessageFormat.format(
			"{0} {1,number,###############################0}",
			objectIdentifier.getObjectType().toString(),
			objectIdentifier.getInstanceNumber()
		);

		if ( propertyMap != null && !propertyMap.isEmpty() && propertyMap.containsKey(PropertyIdentifier.objectName) ) {

			String dName = PropertyValues.getString(propertyMap.get(PropertyIdentifier.objectName));

			if ( !displayName.equals(dName) ) {
				displayName = MessageFormat.format("{0} [{1}]", dName, displayName);
			}

		}

		setDisplayName(displayName);

		ObjectType objectType = remoteObject.getObjectIdentifier().getObjectType();

		if ( ObjectType.accessDoor.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-accessDoor.png");
		} else if ( ObjectType.accumulator.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-accumulator.png");
		} else if ( ObjectType.analogInput.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-analogInput.png");
		} else if ( ObjectType.analogOutput.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-analogOutput.png");
		} else if ( ObjectType.analogValue.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-analogValue.png");
		} else if ( ObjectType.averaging.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-averaging.png");
		} else if ( ObjectType.binaryInput.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-binaryInput.png");
		} else if ( ObjectType.binaryOutput.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-binaryOutput.png");
		} else if ( ObjectType.binaryValue.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-binaryValue.png");
		} else if ( ObjectType.calendar.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-calendar.png");
		} else if ( ObjectType.command.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-command.png");
		} else if ( ObjectType.device.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-device.png");
		} else if ( ObjectType.eventEnrollment.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-eventEnrollment.png");
		} else if ( ObjectType.eventLog.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-eventLog.png");
		} else if ( ObjectType.file.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-file.png");
		} else if ( ObjectType.group.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-group.png");
		} else if ( ObjectType.lifeSafetyPoint.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-lifeSafetyPoint.png");
		} else if ( ObjectType.lifeSafetyZone.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-lifeSafetyZone.png");
		} else if ( ObjectType.loadControl.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-loadControl.png");
		} else if ( ObjectType.loop.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-loop.png");
		} else if ( ObjectType.multiStateInput.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-multiStateInput.png");
		} else if ( ObjectType.multiStateOutput.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-multiStateOutput.png");
		} else if ( ObjectType.multiStateValue.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-multiStateValue.png");
		} else if ( ObjectType.notificationClass.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-notificationClass.png");
		} else if ( ObjectType.program.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-program.png");
		} else if ( ObjectType.pulseConverter.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-pulseConverter.png");
		} else if ( ObjectType.schedule.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-schedule.png");
		} else if ( ObjectType.structuredView.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-structuredView.png");
		} else if ( ObjectType.trendLog.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-trendLog.png");
		} else if ( ObjectType.trendLogMultiple.equals(objectType) ) {
			setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/object-trendLogMultiple.png");
		}

		service.subscribeCOV(subscriberProcessIdentifier++, remoteObject.getObjectIdentifier(), remoteDevice, this);

	}

	public void covNotificationReceived(
		RemoteDevice initiatingDevice,
		ObjectIdentifier monitoredObjectIdentifier,
		UnsignedInteger timeRemaining,
		SequenceOf<PropertyValue> listOfValues
	) {

		if ( !remoteObject.getObjectIdentifier().equals(monitoredObjectIdentifier) ) {
			return;
		}

		if ( listOfValues == null || listOfValues.getCount() <= 0 ) {
			return;
		}

		synchronized ( this ) {

			if ( propertyMap == null ) {
				return;
			}

			listOfValues
				.getValues()
				.stream()
				.filter(pv -> propertyMap.containsKey(pv.getPropertyIdentifier()))
				.forEach(pv -> {

					PropertyIdentifier pid = pv.getPropertyIdentifier();
					Encodable oldValue = propertyMap.get(pid);
					Encodable newValue = pv.getValue();

					propertyMap.put(pid, newValue);
					SwingUtilities.invokeLater(() -> firePropertyChange(pid.toString(), oldValue, newValue));

				});

		}

	}

	@Override
	protected Sheet createSheet() {

		Sheet sheet = super.createSheet();
		Sheet.Set properties = sheet.get(Sheet.PROPERTIES);

		if ( properties == null ) {

			properties = Sheet.createPropertiesSet();

			sheet.put(properties);

		}

		properties.put(
			new PropertySupport.ReadOnly<String>("objectName", String.class, "Object Name", null) {
				@Override
				public String getValue() throws IllegalAccessException, InvocationTargetException {
					return remoteObject.getObjectName();
				}
			}
		);
		properties.put(
			new PropertySupport.ReadOnly<String>("objectType", String.class, "Object Type", null) {
				@Override
				public String getValue() throws IllegalAccessException, InvocationTargetException {
					return remoteObject.getObjectIdentifier().getObjectType().toString();
				}
			}
		);

		Set<PropertyIdentifier> localPropertySet = new TreeSet<>((pid1, pid2) -> pid1.toString().compareTo(pid2.toString()));

		synchronized ( this ) {
			if ( propertyMap != null && !propertyMap.isEmpty() ) {
				localPropertySet.addAll(propertyMap.keySet());
			}
		}

		if ( !localPropertySet.isEmpty() ) {

			Sheet.Set remoteProperties = sheet.get(REMOTE_PROPERTIES);

			if ( remoteProperties == null ) {

				remoteProperties = new Sheet.Set();

				remoteProperties.setDisplayName(REMOTE_PROPERTIES);
				remoteProperties.setName(REMOTE_PROPERTIES);
				remoteProperties.setShortDescription("Properties of the remote object.");
				sheet.put(remoteProperties);

				for ( PropertyIdentifier pid : localPropertySet ) {
					remoteProperties.put(createROProperty(pid, propertyMap.get(pid)));
				}

			}

		}

		return sheet;

	}

	private PropertySupport.ReadOnly<String> createROProperty( final PropertyIdentifier pid, final Encodable value ) {

		String name = pid.toString();

		return new PropertySupport.ReadOnly<String>(name, String.class, name, null) {
			@Override
			public String getValue() throws IllegalAccessException, InvocationTargetException {

				Encodable value;

				synchronized ( ObjectNode.this ) {
					value = propertyMap.get(pid);
				}

				if ( value == null ) {
					return null;
				} else if ( value instanceof StatusFlags ) {
					return getPropertyAsString((StatusFlags) value);
				} else {
					return getPropertyAsString(value);
				}

			}
		};

	}

	private String getPropertyAsString( Encodable property ) {
		return PropertyValues.getString(property);
	}

	private String getPropertyAsString( StatusFlags property ) {

		StringBuilder builder = new StringBuilder(32);
		String separator = ", ";

		if ( property.isInAlarm() ) {
			builder.append("IN ALARM").append(separator);
		}
		if ( property.isFault() ) {
			builder.append("FAULT").append(separator);
		}
		if ( property.isOverridden() ) {
			builder.append("OVERRIDDEN").append(separator);
		}
		if ( property.isOutOfService() ) {
			builder.append("OUT OF SERVICE").append(separator);
		}

		if ( builder.length() > 0 ) {
			builder.delete(builder.length() - separator.length(), builder.length());
		} else {
			builder.append("NORMAL");
		}

		return builder.toString();

	}

}
