/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.netbeans.service.bacnet.nodes;


import com.serotonin.bacnet4j.RemoteDevice;
import com.serotonin.bacnet4j.exception.BACnetException;
import com.serotonin.bacnet4j.type.Encodable;
import com.serotonin.bacnet4j.type.constructed.Address;
import com.serotonin.bacnet4j.type.constructed.BACnetError;
import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;
import com.zerocarbonfuture.netbeans.service.bacnet.BACnetDeviceDescriptor;
import com.zerocarbonfuture.netbeans.service.bacnet.BACnetService;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.text.MessageFormat;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;


/**
 * A node representing a BACnet remote device.
 *
 * @author Claudio Rosati
 */
public class DeviceNode extends AbstractNode {

	private static final Logger LOGGER = Logger.getLogger(DeviceNode.class.getName());
	private static final String SERVICES = "Services";

	private final BACnetDeviceDescriptor descriptor;
	private final RemoteDevice device;
	private Map<PropertyIdentifier, Encodable> propertyMap = null;

	public DeviceNode( BACnetDeviceDescriptor descriptor, RemoteDevice device, BACnetService service ) {

		super(Children.create(new DeviceChildFactory(device, service), true));

		this.descriptor = descriptor;
		this.device = device;

		try {
			propertyMap = service.getProperties(device);
			propertyMap = propertyMap
				.entrySet()
				.stream()
				.filter(e -> ( e.getValue() != null ) && !( e.getValue() instanceof BACnetError ))
				.collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
		} catch ( BACnetException ex ) {
			LOGGER.log(Level.WARNING, null, ex);
		}

		setDisplayName(device.getName());
		setIconBaseWithExtension("com/zerocarbonfuture/netbeans/service/bacnet/resources/device.png");

	}

	public RemoteDevice getDevice() {
		return device;
	}

	@Override
	protected Sheet createSheet() {

		Sheet sheet = super.createSheet();
		Sheet.Set properties = sheet.get(Sheet.PROPERTIES);

		if ( properties == null ) {

			properties = Sheet.createPropertiesSet();

			sheet.put(properties);

		}

		final Address address = device.getAddress();

		properties.put(
			new PropertySupport.ReadOnly<String>("address", String.class, "Address", address.getDescription()) {
				@Override
				public String getValue() throws IllegalAccessException, InvocationTargetException {
					return MessageFormat.format(
						"{0}:{1,number,####0}",
						address.getMacAddress().getInetAddress().getHostAddress(),
						address.getMacAddress().getPort()
					);
				}
			}
		);
		properties.put(
			new PropertySupport.ReadOnly<String>("applicationSoftwareVersion", String.class, "Application Software Version", null) {
				@Override
				public String getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getApplicationSoftwareVersion();
				}
			}
		);
		properties.put(
			new PropertySupport.ReadOnly<String>("firmwareRevision", String.class, "Firmware Revision", null) {
				@Override
				public String getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getFirmwareRevision();
				}
			}
		);
		properties.put(
			new PropertySupport.ReadOnly<Integer>("instanceNumber", Integer.class, "Instance Number", null) {
				@Override
				public Integer getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getInstanceNumber();
				}
			}
		);
		properties.put(
			new PropertySupport.ReadOnly<String>("modelName", String.class, "Model Name", null) {
				@Override
				public String getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getModelName();
				}
			}
		);
		properties.put(
			new PropertySupport.ReadOnly<String>("name", String.class, "Name", null) {
				@Override
				public String getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getName();
				}
			}
		);
		properties.put(
			new PropertySupport.ReadOnly<Integer>("protocolRevision", Integer.class, "Protocol Revision", null) {
				@Override
				public Integer getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getProtocolRevision().intValue();
				}
			}
		);
		properties.put(
			new PropertySupport.ReadOnly<Integer>("protocolVersion", Integer.class, "Protocol Version", null) {
				@Override
				public Integer getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getProtocolVersion().intValue();
				}
			}
		);
		properties.put(
			new PropertySupport.ReadOnly<String>("segmentationSupported", String.class, "Suppoorted Segmentation", null) {
				@Override
				public String getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getSegmentationSupported().toString();
				}
			}
		);
		properties.put(
			new PropertySupport.ReadOnly<Integer>("vendorId", Integer.class, "Vendor ID", null) {
				@Override
				public Integer getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getVendorId();
				}
			}
		);
		properties.put(
			new PropertySupport.ReadOnly<String>("vendorName", String.class, "Vendor Name", null) {
				@Override
				public String getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getVendorName();
				}
			}
		);

		Sheet.Set services = sheet.get(SERVICES);

		if ( services == null ) {

			services = new Sheet.Set();

			services.setDisplayName(SERVICES);
			services.setName(SERVICES);
			services.setShortDescription("Supported services.");
			sheet.put(services);

		}

		services.put(
			new PropertySupport.ReadOnly<Boolean>("acknowledgeAlarm", Boolean.class, "Acknowledge Alarm", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isAcknowledgeAlarm();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("addListElement", Boolean.class, "Add List Element", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isAddListElement();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("atomicReadFile", Boolean.class, "Atomic Read File", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isAtomicReadFile();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("atomicWriteFile", Boolean.class, "Atomic Write File", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isAtomicWriteFile();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("authenticate", Boolean.class, "Authenticate", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isAuthenticate();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("confirmedCovNotification", Boolean.class, "Confirmed COV Notification", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isConfirmedCovNotification();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("confirmedEventNotification", Boolean.class, "Confirmed Event Notification", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isConfirmedEventNotification();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("confirmedPrivateTransfer", Boolean.class, "Confirmed Private Transfer", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isConfirmedPrivateTransfer();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("confirmedTextMessage", Boolean.class, "Confirmed Text Message", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isConfirmedTextMessage();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("createObject", Boolean.class, "Create Object", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isCreateObject();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("deleteObject", Boolean.class, "Delete Object", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isDeleteObject();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("deviceCommunicationControl", Boolean.class, "Device Communication Control", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isDeviceCommunicationControl();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("getAlarmSummary", Boolean.class, "Get Alarm Summary", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isGetAlarmSummary();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("getEnrollmentSummary", Boolean.class, "Get Enrollment Summary", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isGetEnrollmentSummary();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("getEventInformation", Boolean.class, "Get Event Information", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isGetEventInformation();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("iAm", Boolean.class, "I Am", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isIAm();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("iHave", Boolean.class, "I Have", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isIHave();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("lifeSafetyOperation", Boolean.class, "Life Safety Operation", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isLifeSafetyOperation();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("readProperty", Boolean.class, "Read Property", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isReadProperty();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("readPropertyConditional", Boolean.class, "Read Property Conditional", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isReadPropertyConditional();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("readPropertyMultiple", Boolean.class, "Read Property Multiple", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isReadPropertyMultiple();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("readRange", Boolean.class, "Read Range", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isReadRange();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("reinitializeDevice", Boolean.class, "Reinitialize Device", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isReinitializeDevice();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("removeListElement", Boolean.class, "Remove List Element", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isRemoveListElement();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("requestKey", Boolean.class, "Request Key", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isRequestKey();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("subscribeCov", Boolean.class, "Subscribe COV", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isSubscribeCov();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("subscribeCovProperty", Boolean.class, "Subscribe COV Property", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isSubscribeCovProperty();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("timeSynchronization", Boolean.class, "Time Synchronization", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isTimeSynchronization();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("unconfirmedCovNotification", Boolean.class, "Unconfirmed COV Notification", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isUnconfirmedCovNotification();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("unconfirmedEventNotification", Boolean.class, "Unconfirmed Event Notification", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isUnconfirmedEventNotification();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("unconfirmedPrivateTransfer", Boolean.class, "Unconfirmed Private Transfer", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isUnconfirmedPrivateTransfer();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("unconfirmedTextMessage", Boolean.class, "Unconfirmed Text Message", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isUnconfirmedTextMessage();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("utcTimeSynchronization", Boolean.class, "UTC Time Synchronization", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isUtcTimeSynchronization();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("vtClose", Boolean.class, "VT Close", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isVtClose();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("vtData", Boolean.class, "VT Data", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isVtData();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("vtOpen", Boolean.class, "VT Open", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isVtOpen();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("whoHas", Boolean.class, "Who Has", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isWhoHas();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("whoIs", Boolean.class, "Who Is", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isWhoIs();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("writeProperty", Boolean.class, "Write Property", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isWriteProperty();
				}
			}
		);
		services.put(
			new PropertySupport.ReadOnly<Boolean>("writePropertyMultiple", Boolean.class, "Write Property Multiple", null) {
				@Override
				public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
					return device.getServicesSupported().isWritePropertyMultiple();
				}
			}
		);

		return sheet;

	}

}
