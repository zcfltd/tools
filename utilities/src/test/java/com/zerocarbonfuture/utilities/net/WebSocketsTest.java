/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.net;


import com.zerocarbonfuture.utilities.net.event.WebSocketsCloseListener;
import com.zerocarbonfuture.utilities.net.event.WebSocketsErrorListener;
import com.zerocarbonfuture.utilities.net.event.WebSocketsOpenListener;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * @author Claudio.Rosati@zerocarbonfuture.com
 */
@SuppressWarnings( { "UseOfSystemOutOrSystemErr", "CallToPrintStackTrace" } )
public class WebSocketsTest {

	private static final int PORT = 34567;

	private WebSocketsAccessor wsClient = null;
	private final WebSocketsCloseListener wsClientCloseListener = e -> System.out.println(MessageFormat.format("WS Client - Connection closed: {0}", e.getConnection().getRemoteSocketAddress().toString()));
	private final WebSocketsErrorListener wsClientErrorListener = e -> System.out.println(MessageFormat.format("WS Client - Error occurred: {0}", e.getException().getMessage()));
	private final WebSocketsOpenListener wsClientOpenListener = e -> System.out.println(MessageFormat.format("WS Client - Connection opened: {0}", e.getConnection().getRemoteSocketAddress().toString()));
	private WebSocketsAccessor wsServer = null;
	private final WebSocketsCloseListener wsServerCloseListener = e -> System.out.println(MessageFormat.format("WS Server - Connection closed: {0}", e.getConnection().getLocalSocketAddress().toString()));
	private final WebSocketsErrorListener wsServerErrorListener = e -> System.out.println(MessageFormat.format("WS Server - Error occurred: {0}", e.getException().getMessage()));
	private final WebSocketsOpenListener wsServerOpenListener = e -> System.out.println(MessageFormat.format("WS Server - Connection opened: {0}", e.getConnection().getLocalSocketAddress().toString()));

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	public WebSocketsTest() {
	}

	@Before
	public void setUp() throws URISyntaxException {

		wsServer = WebSocketsFactory.createServer(PORT, "APP-CODE");

		wsServer.addWebSocketsErrorListener(wsServerErrorListener);
		wsServer.addWebSocketsOpenListener(wsServerOpenListener);
		wsServer.addWebSocketsCloseListener(wsServerCloseListener);

		wsClient = WebSocketsFactory.createClient("localhost", PORT, "APP-CODE");

		wsClient.addWebSocketsErrorListener(wsClientErrorListener);
		wsClient.addWebSocketsOpenListener(wsClientOpenListener);
		wsClient.addWebSocketsCloseListener(wsClientCloseListener);

	}

	@After
	public void tearDown() {

		wsServer.removeWebSocketsCloseListener(wsServerCloseListener);
		wsServer.removeWebSocketsOpenListener(wsServerOpenListener);
		wsServer.removeWebSocketsErrorListener(wsServerErrorListener);

		wsServer = null;

		wsClient.removeWebSocketsCloseListener(wsClientCloseListener);
		wsClient.removeWebSocketsOpenListener(wsClientOpenListener);
		wsClient.removeWebSocketsErrorListener(wsClientErrorListener);

		wsClient = null;

	}

	@Test
	public void test3AsynchronousMessages() throws InterruptedException, IOException, URISyntaxException {
		
		System.out.println("singleAsynchronousMessage");
		
		final Map<Long, String> receivedMessages = new HashMap<>(1);
		
		wsClient.registerCallback("MSG1", new AsynchronousCallback<String>(String.class) {
			@Override
			@SuppressWarnings( "CallToPrintStackTrace" )
			public void call( String messageObject ) {
				receivedMessages.put(System.nanoTime(), messageObject);
			}
		});

		wsServer.startUp();

		try {
			Thread.sleep(1000L);
		} catch ( InterruptedException ex ) {
			ex.printStackTrace();
		}
		
		wsClient.startUp();

		wsServer.sendAsynchronous("MSG1", "Message #1");
		wsServer.sendAsynchronous("MSG2", "Message #2");
		wsServer.sendAsynchronous("MSG3", "Message #3");
		
		try {
			Thread.sleep(1000L);
		} catch ( InterruptedException ex ) {
			ex.printStackTrace();
		}
		
		assertEquals("Number of received messages", 1, receivedMessages.size());
		
		wsClient.unregisterCallback("MSG1");

		wsClient.tearDown(5, TimeUnit.SECONDS);
		wsServer.tearDown(5, TimeUnit.SECONDS);

	}

	@Test
	public void testSingleAsynchronousMessage() throws InterruptedException, IOException, URISyntaxException {
		
		System.out.println("singleAsynchronousMessage");
		
		final SynchronousQueue<String> queue = new SynchronousQueue<>(true);
		
		wsClient.registerCallback("MSG1", new AsynchronousCallback<String>(String.class) {
			@Override
			@SuppressWarnings( "CallToPrintStackTrace" )
			public void call( String messageObject ) {
				try {
					assertTrue("No one reading the queue.", queue.offer(messageObject, 1L, TimeUnit.SECONDS));
				} catch ( InterruptedException ex ) {
					ex.printStackTrace();
				}
			}
		});

		wsServer.startUp();

		try {
			Thread.sleep(1000L);
		} catch ( InterruptedException ex ) {
			ex.printStackTrace();
		}
		
		wsClient.startUp();

		wsServer.sendAsynchronous("MSG1", "Message #1");
		
		assertEquals("Received message differs from the sent one.", "Message #1", queue.take());
		
		wsClient.unregisterCallback("MSG1");

		wsClient.tearDown(5, TimeUnit.SECONDS);
		wsServer.tearDown(5, TimeUnit.SECONDS);

	}

	@Test
	public void testStartUpAndTearDown() throws InterruptedException, IOException, URISyntaxException {

		System.out.println("startUpAndTearDown");

		wsServer.startUp();

		try {
			Thread.sleep(1000L);
		} catch ( InterruptedException ex ) {
			ex.printStackTrace();
		}
		
		wsClient.startUp();

		assertTrue("Started up", true);

		wsClient.tearDown(5, TimeUnit.SECONDS);
		wsServer.tearDown(5, TimeUnit.SECONDS);

		assertTrue("Torn down", true);

	}

}
