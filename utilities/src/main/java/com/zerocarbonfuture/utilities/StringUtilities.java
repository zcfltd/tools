/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities;


/**
 * Various utilities handling strings
 *
 * @author Claudio Rosati
 */
public class StringUtilities {

	/**
	 * Creates a new string splitting a given camel-case string.
	 *
	 * @param camelCaseString The camel-case string to be split.
	 * @return A new {@link String} obtained splitting the camel-case one.
	 * @throws NullPointerException If {@code camelCaseString} is {@code null}.
	 */
	public static String splitCamelCase( String camelCaseString ) {

		if ( camelCaseString == null ) {
			throw new NullPointerException("camelCaseString");
		}

		String ret = camelCaseString.replaceAll("(?<=[A-Z])(?=[A-Z][a-z])|(?<=[^A-Z])(?=[A-Z])|(?<=[A-Za-z])(?=[^A-Za-z])", " ");

		if ( !ret.isEmpty() && Character.isLowerCase(ret.charAt(0)) ) {
			ret = Character.toString(ret.charAt(0)).toUpperCase() + ret.substring(1);
		}

		return ret;

	}

	private StringUtilities() {
	}

}
