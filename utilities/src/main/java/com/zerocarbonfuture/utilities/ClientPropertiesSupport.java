/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities;


import java.beans.PropertyChangeEvent;
import java.io.Serializable;
import java.util.Collections;
import java.util.SortedMap;
import java.util.TreeMap;


/**
 * A support class to add client properties to other classes.
 *
 * @author Claudio Rosati
 */
public class ClientPropertiesSupport implements Serializable {

	private static final long serialVersionUID = -5931512963372261469L;

	private final transient SortedMap<Object, Object> clientProperties = new TreeMap<>();
	private final ClientPropertyChangeClient clientPropertyChangeClient;
	private final PropertyChangeSource propertyChangeSource;

	/**
	 * Creates a new instance of {@link ClientPropertiesSupport} class.
	 */
	public ClientPropertiesSupport() {
		this(null, null);
	}

	/**
	 * Creates a new instance of {@link ClientPropertiesSupport} class.
	 *
	 * @param clientPropertyChangeClient The client interested in client property change notifications.
	 */
	public ClientPropertiesSupport( ClientPropertyChangeClient clientPropertyChangeClient ) {
		this(null, clientPropertyChangeClient);
	}

	/**
	 * Creates a new instance of {@link ClientPropertiesSupport} class.
	 *
	 * @param propertyChangeSource The source of {@link PropertyChangeEvent}s.
	 */
	public ClientPropertiesSupport( PropertyChangeSource propertyChangeSource ) {
		this(propertyChangeSource, null);
	}

	/**
	 * Creates a new instance of {@link ClientPropertiesSupport} class.
	 *
	 * @param propertyChangeSource       The source of {@link PropertyChangeEvent}s.
	 * @param clientPropertyChangeClient The client interested in client property change notifications.
	 */
	public ClientPropertiesSupport( PropertyChangeSource propertyChangeSource, ClientPropertyChangeClient clientPropertyChangeClient ) {
		this.propertyChangeSource = propertyChangeSource;
		this.clientPropertyChangeClient = clientPropertyChangeClient;
	}

	/**
	 * Returns an unmodifiable copy of the {@link SortedMap} used to store
	 * key/value "client properties".
	 *
	 * @return A never {@code null} {Link SortedMap}.
	 * @see #putClientProperty
	 * @see #getClientProperty
	 */
	public SortedMap<Object, Object> getClientProperties() {
		synchronized ( clientProperties ) {
			return Collections.unmodifiableSortedMap(clientProperties);
		}
	}

	/**
	 * Returns the value of the property with the specified key. Only
	 * properties added with {@link #putClientProperty} will return
	 * a non-{@code null} value.
	 *
	 * @param key The being queried.
	 * @return The value of this property or {@code null}
	 * @see #putClientProperty
	 */
	public final Object getClientProperty( Object key ) {
		synchronized ( clientProperties ) {
			return clientProperties.get(key);
		}
	}

	/**
	 * Adds an arbitrary key/value "client property" to this support object.
	 * <p>
	 * The {@code get/putClientProperty} methods provide access to
	 * a small per-instance hashtable. Callers can use {@code get/putClientProperty}
	 * to annotate components that were created by another module.
	 * <p>
	 * If value is {@code null} this method will remove the property.
	 * <p>
	 * Changes to client properties are reported with
	 * {@link PropertyChangeEvent}s to the {@link PropertyChangeSource}
	 * implementer passed to this object's constructor.
	 * The name of the property (for the sake of
	 * {@link PropertyChangeEvent}s) is {@code key.toString()}.
	 * <p>
	 * Changes to client properties are also reported to the {@link ClientPropertyChangeClient}
	 * implementer passed to this object's constructor.
	 *
	 * @param key   The new client property key.
	 * @param value The new client property value; if {@code null}
	 *              this method will remove the property.
	 * @see #getClientProperty
	 */
	public final void putClientProperty( Object key, Object value ) {

		Object oldValue;

		synchronized ( clientProperties ) {

			oldValue = clientProperties.get(key);

			if ( value != null ) {
				clientProperties.put(key, value);
			} else if ( oldValue != null ) {
				clientProperties.remove(key);
			} else {
				// old == new == null
				return;
			}

		}

		if ( clientPropertyChangeClient != null ) {
			clientPropertyChangeClient.clientPropertyChanged(key, oldValue, value);
		}

		if ( propertyChangeSource != null ) {
			propertyChangeSource.firePropertyChange(key.toString(), oldValue, value);
		}

	}

	/**
	 * Represents a client interested in client property change notifications.
	 * <p>
	 * This is a functional interface whose functional method is
	 * {@link #clientPropertyChanged(java.lang.Object, java.lang.Object, java.lang.Object)}.
	 */
	@FunctionalInterface
	@SuppressWarnings( "PublicInnerClass" )
	public interface ClientPropertyChangeClient {

		/**
		 * Invoked when a client property changed.
		 *
		 * @param key      The client property key.
		 * @param oldValue The old client property value.
		 * @param newValue The new client property value.
		 */
		public void clientPropertyChanged( Object key, Object oldValue, Object newValue );

	}   //  interface ClientPropertyChangeClient

	/**
	 * Represents a source of {@link PropertyChangeEvent}s.
	 * <p>
	 * This is a functional interface whose functional method is
	 * {@link #firePropertyChange(java.lang.String, java.lang.Object, java.lang.Object)}.
	 */
	@FunctionalInterface
	@SuppressWarnings( "PublicInnerClass" )
	public interface PropertyChangeSource {

		/**
		 * Reports a bound property update to listeners that have been registered to track updates
		 * of all properties or a property with the specified name. No event is fired if old and
		 * new values are equal and non-{@code null}.
		 *
		 * @param propertyName The programmatic name of the property that was changed.
		 * @param oldValue     The old value of the property.
		 * @param newValue     The new value of the property.
		 */
		public void firePropertyChange( String propertyName, Object oldValue, Object newValue );

	}   //  interface PropertyChangeSource

}   //  class ClientPropertiesSupport
