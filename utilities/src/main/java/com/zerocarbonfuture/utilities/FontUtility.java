/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities;


import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


/**
 * Various utilities for {@link Font}.
 *
 * @author Claudio Rosati
 */
public class FontUtility {

	public final static String MONOSPACED = "monospaced";
	public final static String SANSSERIF = "sansserif";
	public final static String SERIF = "serif";

	/**
	 * Returns the displayable name of the given <CODE>font</CODE>.
	 *
	 * @param font The {@link Font} whose display name must be returned.
	 * @return The displayable name of the given <CODE>font</CODE>.
	 */
	public static String displayFontName( Font font ) {
		return font.getFontName() + " " + font.getSize() + "pt";
	}

	/**
	 * Encode the given {@code font} in a way symmetrical to the
	 * {@link Font#decode(java.lang.String) } method.
	 *
	 * @param font The {@link Font} to be encoded.
	 * @return The encoded string.
	 */
	public static String encode( Font font ) {

		if ( font == null ) {
			return "";
		} else {
			return font.getFontName()
				+ "-"
				+ (font.isPlain() ? "PLAIN" : font.isBold() ? (font.isItalic() ? "BOLDITALIC" : "BOLD") : "ITALIC")
				+ "-"
				+ font.getSize();
		}

	}

	/**
	 * Returns a newly created plain monospaced font at the given <CODE>size</CODE>.
	 *
	 * @param size The font size.
	 * @return A {@link Font} instance.
	 */
	public static Font getMonospacedFont( int size ) {
		return new Font(MONOSPACED, Font.PLAIN, size);
	}

	/**
	 * Returns a newly created plain sans serif font at the given <CODE>size</CODE>.
	 *
	 * @param size The font size.
	 * @return A {@link Font} instance.
	 */
	public static Font getSansSerifFont( int size ) {
		return new Font(SANSSERIF, Font.PLAIN, size);
	}

	/**
	 * Returns a newly created plain serif font at the given <CODE>size</CODE>.
	 *
	 * @param size The font size.
	 * @return A {@link Font} instance.
	 */
	public static Font getSerifFont( int size ) {
		return new Font(SERIF, Font.PLAIN, size);
	}

	/**
	 * Tries to load a TrueType font. If loading failed, a replacement font
	 * is returned instead.
	 *
	 * @param fontFile        The font file used to open the font input stream.
	 * @param style	          The style used to render the loaded font. The style argument
	 *                        is an integer bitmask that may be <CODE>Font.PLAIN</CODE>,
	 *                        or a bitwise union of <CODE>Font.BOLD</CODE> and/or
	 *                        <CODE>Font.ITALIC</CODE> (for example, <CODE>Font.ITALIC</CODE>
	 *                        or <CODE>Font.BOLD | Font.ITALIC</CODE>). If the style
	 *                        argument does not conform to one of the expected integer
	 *                        bitmasks then the style is set to <CODE>Font.PLAIN</CODE>.
	 * @param size            The point size used to render the loaded font.
	 * @param replacementFont The font returned if loading failed.
	 * @return The loaded font or the <CODE>replacementFont</CODE> if loading
	 *         failed.
	 */
	public static Font loadTrueTypeFont( File fontFile, int style, float size, Font replacementFont ) {
		try {
			return loadTrueTypeFont(new FileInputStream(fontFile), style, size, replacementFont);
		} catch ( FileNotFoundException e ) {
			return replacementFont;
		}
	}

	/**
	 * Tries to load a TrueType font. If loading failed, a replacement font
	 * is returned instead.
	 *
	 * @param fontResourceName The name of the font as a resource in the current
	 *                         classpath. It will be opened as an <CODE>InputStream</CODE>
	 *                         by means of the <CODE>getClass().getResourceAsStream()</CODE>
	 *                         method.
	 * @param style	           The style used to render the loaded font. The style argument
	 *                         is an integer bitmask that may be <CODE>Font.PLAIN</CODE>,
	 *                         or a bitwise union of <CODE>Font.BOLD</CODE> and/or
	 *                         <CODE>Font.ITALIC</CODE> (for example, <CODE>Font.ITALIC</CODE>
	 *                         or <CODE>Font.BOLD | Font.ITALIC</CODE>). If the style
	 *                         argument does not conform to one of the expected integer
	 *                         bitmasks then the style is set to <CODE>Font.PLAIN</CODE>.
	 * @param size             The point size used to render the loaded font.
	 * @param replacementFont  The font returned if loading failed.
	 * @return The loaded font or the <CODE>replacementFont</CODE> if loading
	 *         failed.
	 */
	public static Font loadTrueTypeFont( String fontResourceName, int style, float size, Font replacementFont ) {
		try {
			return loadTrueTypeFont(fontResourceName, FontUtility.class, style, size, replacementFont);
		} catch ( Exception e ) {
			return replacementFont;
		}
	}

	/**
	 * Tries to load a TrueType font. If loading failed, a replacement font
	 * is returned instead.
	 *
	 * @param fontResourceName The name of the font as a resource in the current
	 *                         classpath. It will be opened as an <CODE>InputStream</CODE>
	 *                         by means of the <CODE>anchorClass.getResourceAsStream()</CODE>
	 *                         method.
	 * @param anchorClass      The class used to call the <CODE>getResourceAsStream</CODE>
	 *                         method to open the font input stream.
	 * @param style	           The style used to render the loaded font. The style argument
	 *                         is an integer bitmask that may be <CODE>Font.PLAIN</CODE>,
	 *                         or a bitwise union of <CODE>Font.BOLD</CODE> and/or
	 *                         <CODE>Font.ITALIC</CODE> (for example, <CODE>Font.ITALIC</CODE>
	 *                         or <CODE>Font.BOLD | Font.ITALIC</CODE>). If the style
	 *                         argument does not conform to one of the expected integer
	 *                         bitmasks then the style is set to <CODE>Font.PLAIN</CODE>.
	 * @param size             The point size used to render the loaded font.
	 * @param replacementFont  The font returned if loading failed.
	 * @return The loaded font or the <CODE>replacementFont</CODE> if loading
	 *         failed.
	 */
	public static Font loadTrueTypeFont( String fontResourceName, Class<?> anchorClass, int style, float size, Font replacementFont ) {
		try {
			return loadTrueTypeFont(anchorClass.getResourceAsStream(fontResourceName), style, size, replacementFont);
		} catch ( Exception e ) {
			return replacementFont;
		}
	}

	/**
	 * Tries to load a TrueType font. If loading failed, a replacement font
	 * is returned instead.
	 *
	 * @param fontStream      The <CODE>InputStream</CODE> used to load the font.
	 * @param style		         The style used to render the loaded font. The style argument
	 *                        is an integer bitmask that may be <CODE>Font.PLAIN</CODE>,
	 *                        or a bitwise union of <CODE>Font.BOLD</CODE> and/or
	 *                        <CODE>Font.ITALIC</CODE> (for example, <CODE>Font.ITALIC</CODE>
	 *                        or <CODE>Font.BOLD | Font.ITALIC</CODE>). If the style
	 *                        argument does not conform to one of the expected integer
	 *                        bitmasks then the style is set to <CODE>Font.PLAIN</CODE>.
	 * @param size            The point size used to render the loaded font.
	 * @param replacementFont The font returned if loading failed.
	 * @return The loaded font or the <CODE>replacementFont</CODE> if loading
	 *         failed.
	 */
	public static Font loadTrueTypeFont( InputStream fontStream, int style, float size, Font replacementFont ) {
		try {
			return Font.createFont(Font.TRUETYPE_FONT, fontStream).deriveFont(style, size);
		} catch ( FontFormatException | IOException e ) {
			return replacementFont;
		}
	}

	private FontUtility() {
	}

}
