/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.net;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zerocarbonfuture.utilities.EventListenerList;
import com.zerocarbonfuture.utilities.LogUtilities;
import com.zerocarbonfuture.utilities.net.event.WebSocketsCloseEvent;
import com.zerocarbonfuture.utilities.net.event.WebSocketsCloseListener;
import com.zerocarbonfuture.utilities.net.event.WebSocketsErrorEvent;
import com.zerocarbonfuture.utilities.net.event.WebSocketsErrorListener;
import com.zerocarbonfuture.utilities.net.event.WebSocketsOpenEvent;
import com.zerocarbonfuture.utilities.net.event.WebSocketsOpenListener;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.Handshakedata;

import static com.zerocarbonfuture.utilities.net.WebSocketsAccessor.DEFAULT_TIMEOUT;
import static java.util.concurrent.TimeUnit.SECONDS;


/**
 * AccessorSupport class implementing common client and server behaviors.
 *
 * @author Claudio Rosati
 */
class AccessorSupport {

	public enum Mode {

		ASYNCH,
		SYNCH_CALL,
		SYNCH_RETURN

	}

	static final String HEADER_START = "<<";
	static final String HEADER_END = ">>";
	static final String HEADER_SEPARATOR = ":";

	/**
	 * The {@link MessageFormat} pattern to be used to format asynchronous messages.
	 * <PRE>
	 *   {0} - The application code.
	 *   {1} - The mode.
	 *   {2} - The message type.
	 *   {3} - The JSON fragment.
	 * </PRE>
	 */
	static final String ASYNCH_FORMAT = HEADER_START + "{0}" + HEADER_SEPARATOR + Mode.ASYNCH.name() + HEADER_SEPARATOR + "{1}" + HEADER_END + "{2}";

	/**
	 * The {@link MessageFormat} pattern to be used to format synchronous messages.
	 * <PRE>
	 *   {0} - The application code.
	 *   {1} - The mode.
	 *   {2} - The message type.
	 *   {3} - The message identifier.
	 *   {4} - The JSON fragment.
	 * </PRE>
	 */
	static final String SYNCH_FORMAT = HEADER_START + "{0}" + HEADER_SEPARATOR + "{1}" + HEADER_SEPARATOR + "{2}" + HEADER_SEPARATOR + "{3}" + HEADER_END + "{4}";

	private static final Logger LOGGER = Logger.getLogger(AccessorSupport.class.getName());
	private static final int MD5_LENGTH = 32;

	private final WebSocketsAccessor accessor;
	private final String applicationCode;
	private final Map<String, List<AsynchronousCallback<?>>> asynchronousCallbacks = new TreeMap<>();
	private final ExecutorService executor = Executors.newCachedThreadPool(r -> {

		Thread t = Executors.defaultThreadFactory().newThread(r);

		t.setPriority(Thread.NORM_PRIORITY - 1);
		t.setName(AccessorSupport.class.getSimpleName() + " " + UUID.randomUUID());

		return t;

	});
	private final EventListenerList listenerList = new EventListenerList();
	private final ObjectMapper mapper = new ObjectMapper();
	private final Map<String, List<SynchronousCallback<?, ?>>> synchronousCallbacks = new TreeMap<>();
	private final Map<String, SynchronousQueue<String>> waitingCalls = new TreeMap<>();

	AccessorSupport( String applicationCode, WebSocketsAccessor accessor ) {
		this.applicationCode = applicationCode;
		this.accessor = accessor;
	}

	void addWebSocketsCloseListener( WebSocketsCloseListener listener ) {
		listenerList.add(WebSocketsCloseListener.class, listener);
	}

	void addWebSocketsErrorListener( WebSocketsErrorListener listener ) {
		listenerList.add(WebSocketsErrorListener.class, listener);
	}

	void addWebSocketsOpenListener( WebSocketsOpenListener listener ) {
		listenerList.add(WebSocketsOpenListener.class, listener);
	}

	void fireConnectionClosed( WebSocket conn, int code, String reason, boolean remote ) {

		WebSocketsCloseEvent event = null;

		// Guaranteed to return a non-null array
		Object[] listeners = listenerList.getListenerList();

		// Process the listeners last to first, notifying those that are interested in this event.
		for ( int i = listeners.length - 2; i >= 0; i -= 2 ) {
			if ( listeners[i] == WebSocketsCloseListener.class ) {

				// Lazily create the event:
				if ( event == null ) {
					event = new WebSocketsCloseEvent(accessor, conn, code, reason, remote);
				}

				((WebSocketsCloseListener) listeners[i + 1]).connectionClosed(event);

			}
		}

	}

	void fireConnectionError( WebSocket conn, Exception exception ) {

		WebSocketsErrorEvent event = null;

		// Guaranteed to return a non-null array
		Object[] listeners = listenerList.getListenerList();

		// Process the listeners last to first, notifying those that are interested in this event.
		for ( int i = listeners.length - 2; i >= 0; i -= 2 ) {
			if ( listeners[i] == WebSocketsErrorListener.class ) {

				// Lazily create the event:
				if ( event == null ) {
					event = new WebSocketsErrorEvent(accessor, conn, exception);
				}

				((WebSocketsErrorListener) listeners[i + 1]).connectionError(event);

			}
		}

	}

	void fireConnectionOpen( WebSocket conn, Handshakedata handshake ) {

		WebSocketsOpenEvent event = null;

		// Guaranteed to return a non-null array
		Object[] listeners = listenerList.getListenerList();

		// Process the listeners last to first, notifying those that are interested in this event.
		for ( int i = listeners.length - 2; i >= 0; i -= 2 ) {
			if ( listeners[i] == WebSocketsOpenListener.class ) {

				// Lazily create the event:
				if ( event == null ) {
					event = new WebSocketsOpenEvent(accessor, conn, handshake);
				}

				((WebSocketsOpenListener) listeners[i + 1]).connectionOpen(event);

			}
		}

	}

	String getApplicationCode() {
		return applicationCode;
	}

	void onClose( Logger acessorLogger, WebSocket conn, int code, String reason, boolean remote ) {
		fireConnectionClosed(conn, code, reason, remote);
	}

	void onError( Logger acessorLogger, WebSocket conn, Exception ex ) {
		fireConnectionError(conn, ex);
	}

	void onMessage( Logger acessorLogger, WebSocket conn, String message ) {
		try {
			handleMessage(conn, message);
		} catch ( Exception ex ) {
			LogUtilities.log(
				acessorLogger,
				Level.SEVERE,
				ex,
				"Error handling message {0} [{1}].",
				conn.getLocalSocketAddress().toString(),
				message
			);
		}
	}

	void onOpen( Logger acessorLogger, WebSocket conn, Handshakedata handshake ) {
		fireConnectionOpen(conn, handshake);
	}

	<T> void registerCallback( String messageType, AsynchronousCallback<T> callback ) throws IllegalArgumentException {

		if ( messageType == null ) {
			throw new IllegalArgumentException("'messageType' is null.");
		}

		if ( callback == null ) {
			throw new IllegalArgumentException("'callback' is null.");
		}

		synchronized ( asynchronousCallbacks ) {

			List<AsynchronousCallback<?>> callbacks = asynchronousCallbacks.get(messageType);

			if ( callbacks == null ) {

				callbacks = new ArrayList<>(1);

				asynchronousCallbacks.put(messageType, callbacks);

			}

			if ( !callbacks.contains(callback) ) {
				callbacks.add(callback);
			}

		}

	}

	<T, R> void registerCallback( String messageType, SynchronousCallback<T, R> callback ) throws IllegalArgumentException {

		if ( messageType == null ) {
			throw new IllegalArgumentException("'messageType' is null.");
		}

		if ( callback == null ) {
			throw new IllegalArgumentException("'callback' is null.");
		}

		synchronized ( synchronousCallbacks ) {

			List<SynchronousCallback<?, ?>> callbacks = synchronousCallbacks.get(messageType);

			if ( callbacks == null ) {

				callbacks = new ArrayList<>(1);

				synchronousCallbacks.put(messageType, callbacks);

			}

			if ( !callbacks.contains(callback) ) {
				callbacks.add(callback);
			}

		}

	}

	void removeWebSocketsCloseListener( WebSocketsCloseListener listener ) {
		listenerList.remove(WebSocketsCloseListener.class, listener);
	}

	void removeWebSocketsErrorListener( WebSocketsErrorListener listener ) {
		listenerList.remove(WebSocketsErrorListener.class, listener);
	}

	void removeWebSocketsOpenListener( WebSocketsOpenListener listener ) {
		listenerList.remove(WebSocketsOpenListener.class, listener);
	}

	<T> void sendAsynchronous( String messageType, T messageObject ) throws JsonProcessingException {
		sendAsynchronousJSONString(messageType, mapper.writeValueAsString(messageObject));
	}

	<T, R> R sendSynchronous( String messageType, T messageObject, Class<R> returnType, long timeout, TimeUnit unit ) throws IOException {

		String returnedJSON = sendSynchronousJSONString(messageType, mapper.writeValueAsString(messageObject), timeout, unit);

		return mapper.readValue(returnedJSON, returnType);

	}

	@SuppressWarnings( "UnusedAssignment" )
	void shutDown( long timeout, TimeUnit unit ) throws InterruptedException {

		SynchronousQueue<String> blockingQueue;

		do {

			String messageID;

			synchronized ( waitingCalls ) {
				if ( !waitingCalls.isEmpty() ) {
					messageID = waitingCalls.keySet().iterator().next();
					blockingQueue = waitingCalls.remove(messageID);
				} else {
					messageID = null;
					blockingQueue = null;
				}
			}

			if ( blockingQueue != null ) {
				blockingQueue.poll(timeout, unit);
				blockingQueue.offer(StringUtils.EMPTY, timeout, unit);
			}

		} while ( blockingQueue != null );

		executor.shutdownNow();
		executor.awaitTermination(timeout, unit);

	}

	void unregisterCallback( String messageType ) throws IllegalArgumentException {

		if ( messageType == null ) {
			throw new IllegalArgumentException("'messageType' is null.");
		}

		synchronized ( asynchronousCallbacks ) {
			if ( asynchronousCallbacks.containsKey(messageType) ) {
				asynchronousCallbacks.remove(messageType);
			}
		}

		synchronized ( synchronousCallbacks ) {
			if ( synchronousCallbacks.containsKey(messageType) ) {
				synchronousCallbacks.remove(messageType);
			}
		}

	}

	private String extractApplicationCodeFromHeader( String header ) {

		int separatorIndex = header.indexOf(HEADER_SEPARATOR);

		if ( separatorIndex != -1 ) {
			return header.substring(0, separatorIndex);
		} else {
			return null;
		}

	}

	private String extractHeaderFromMessage( String message ) {

		int start = message.indexOf(HEADER_START);
		int end = message.indexOf(HEADER_END);

		if ( start != -1 && end != -1 ) {
			return message.substring(start + HEADER_START.length(), end);
		} else {
			return null;
		}

	}

	private String extractJSONFragmentFromMessage( String message ) {

		int end = message.indexOf(HEADER_END);

		if ( end != -1 ) {
			return message.substring(end + HEADER_END.length());
		} else {
			return null;
		}

	}

	private String extractMessageIdentifierFromHeader( String header ) {

		int appCode = header.indexOf(HEADER_SEPARATOR);
		int mode = header.indexOf(HEADER_SEPARATOR, appCode + 1);
		int start = header.indexOf(HEADER_SEPARATOR, mode + 1);
		int end = header.length();

		if ( start != -1 && end != -1 ) {
			return header.substring(start + HEADER_SEPARATOR.length(), end);
		} else {
			return null;
		}

	}

	private String extractMessageTypeFromHeader( String header, boolean asynch ) {

		int appCode = header.indexOf(HEADER_SEPARATOR);
		int start = header.indexOf(HEADER_SEPARATOR, appCode + 1);
		int end = asynch ? header.length() : header.indexOf(HEADER_SEPARATOR, start + 1);

		if ( start != -1 && end != -1 ) {
			return header.substring(start + HEADER_SEPARATOR.length(), end);
		} else {
			return null;
		}

	}

	private String extractModeFromHeader( String header ) {

		int start = header.indexOf(HEADER_SEPARATOR);
		int end = header.indexOf(HEADER_SEPARATOR, start + 1);

		if ( start != -1 && end != -1 ) {
			return header.substring(start + HEADER_SEPARATOR.length(), end);
		} else {
			return null;
		}

	}

	@SuppressWarnings( "AssignmentToMethodParameter" )
	private void handleMessage( WebSocket connection, String message ) {

		if ( message == null ) {
			throw new IllegalArgumentException("Null 'message' argument.");
		} else if ( message.isEmpty() ) {
			throw new IllegalArgumentException("Empty 'message' argument.");
		} else if ( message.length() <= MD5_LENGTH ) {
			throw new IllegalArgumentException("'message' too short.");
		}

		//	MD5 check.
		String readMD5 = message.substring(message.length() - MD5_LENGTH);
		
		message = message.substring(0, message.length() - MD5_LENGTH);
		
		String computedMD5 = DigestUtils.md5Hex(message);
		
		if ( !readMD5.equalsIgnoreCase(computedMD5) ) {
			throw new IllegalArgumentException("MD5 mismatch in 'message'.");
		}
		
		String header = extractHeaderFromMessage(message);

		if ( header == null ) {
			throw new IllegalMessageFormat(MessageFormat.format("Malformed or null 'message' header [{0}].", message));
		}

		String appCode = extractApplicationCodeFromHeader(header);

		if ( appCode == null ) {
			throw new IllegalMessageFormat(MessageFormat.format("Malformed 'message' header: missing application code [{0}].", message));
		} else if ( !StringUtils.equals(applicationCode, appCode) ) {
			throw new IllegalMessageFormat(MessageFormat.format("Malformed 'message' header: invalid application code [{0}].", message));
		}

		Mode mode;
		String modeString = extractModeFromHeader(header);

		if ( modeString == null ) {
			throw new IllegalMessageFormat(MessageFormat.format("Malformed 'message' header: missing mode [{0}].", message));
		} else {
			try {
				mode = Mode.valueOf(modeString);
			} catch ( IllegalArgumentException iaex ) {
				throw new IllegalMessageFormat(MessageFormat.format("Malformed 'message' header: wrong mode [{0}].", message));
			}
		}

		switch ( mode ) {
			case ASYNCH:
				handleMessageAsynch(message, header);
				break;
			case SYNCH_CALL:
				handleMessageSynchCall(connection, message, header);
				break;
			case SYNCH_RETURN:
				handleMessageSynchReturn(message, header);
				break;
		}

	}

	private void handleMessageAsynch( String message, String header ) {

		String messageType = extractMessageTypeFromHeader(header, true);

		if ( messageType == null ) {
			throw new IllegalMessageFormat(MessageFormat.format("Malformed 'message' header: missing message type [{0}].", message));
		}

		List<AsynchronousCallback<?>> callbacks = null;

		synchronized ( asynchronousCallbacks ) {

			List<AsynchronousCallback<?>> cbs = asynchronousCallbacks.get(messageType);

			if ( cbs != null ) {
				callbacks = new ArrayList<>(cbs);
			}

		}

		if ( callbacks == null || callbacks.isEmpty() ) {
			LogUtilities.log(LOGGER, Level.FINE, "No callbacks for message type: {0} [{1}].", messageType, message);
		} else {

			final String json = extractJSONFragmentFromMessage(message);

			if ( json == null ) {
				throw new IllegalMessageFormat(MessageFormat.format("Invalid or null JSON fragment [{0}].", message));
			}

			for ( AsynchronousCallback<?> c : callbacks ) {

				final AsynchronousCallback<?> callback = c;

				executor.execute(() -> {
					try {
						callback.call(json, mapper);
					} catch ( IOException ex ) {
						LogUtilities.log(LOGGER, Level.SEVERE, ex, "Unable to invoke callback: {0} [{1}].", ex.getMessage(), json);
					}
				});

			}

		}

	}

	private void handleMessageSynchCall( final WebSocket connection, String message, String header ) {

		final String messageType = extractMessageTypeFromHeader(header, false);

		if ( messageType == null ) {
			throw new IllegalMessageFormat(MessageFormat.format("Malformed 'message' header: missing message type [{0}].", message));
		}

		final String messageIdentifier = extractMessageIdentifierFromHeader(header);

		if ( messageIdentifier == null ) {
			throw new IllegalMessageFormat(MessageFormat.format("Malformed 'message' header: missing message identifier [{0}].", message));
		}

		List<SynchronousCallback<?, ?>> callbacks = null;

		synchronized ( synchronousCallbacks ) {

			List<SynchronousCallback<?, ?>> cbs = synchronousCallbacks.get(messageType);

			if ( cbs != null ) {
				callbacks = new ArrayList<>(cbs);
			}

		}

		if ( callbacks == null || callbacks.isEmpty() ) {
			LogUtilities.log(LOGGER, Level.FINE, "No callbacks for message type: {0} [{1}].", messageType, message);
		} else {

			final String json = extractJSONFragmentFromMessage(message);

			if ( json == null ) {
				throw new IllegalMessageFormat(MessageFormat.format("Invalid or null JSON fragment [{0}].", message));
			}

			if ( callbacks.size() > 1 ) {
				LogUtilities.log(LOGGER, Level.WARNING, "Too many callbacks to send message: the first one will be used [{0}].", message);
			}

			final SynchronousCallback<?, ?> callback = callbacks.iterator().next();

			executor.submit(() -> {
				try {

					String jsonResponse = callback.call(json, mapper);
					String message1 = MessageFormat.format(SYNCH_FORMAT, applicationCode, Mode.SYNCH_RETURN.name(), messageType, messageIdentifier, jsonResponse);

					connection.send(message1 + DigestUtils.md5Hex(message1));

				} catch ( IOException ex ) {
					LogUtilities.log(LOGGER, Level.SEVERE, ex, "Unable to invoke callback: {0} [{1}].", ex.getMessage(), json);
				}
			});

		}

	}

	private void handleMessageSynchReturn( String message, String header ) {

		String messageIdentifier = extractMessageIdentifierFromHeader(header);

		if ( messageIdentifier == null ) {
			throw new IllegalMessageFormat(MessageFormat.format("Malformed 'message' header: missing message identifier [{0}].", message));
		}

		SynchronousQueue<String> blockingQueue;

		synchronized ( waitingCalls ) {
			blockingQueue = waitingCalls.get(messageIdentifier);
		}

		if ( blockingQueue == null ) {
			throw new IllegalStateException(MessageFormat.format("No blocking queue waiting for this synchronous response [{0}].", message));
		}

		//	Awaking the original caller.
		try {
			blockingQueue.poll(DEFAULT_TIMEOUT, SECONDS);
		} catch ( InterruptedException ex ) {
			LogUtilities.log(LOGGER, Level.SEVERE, ex, "Unblocking caller thread interrupted [{0}].", message);
		}

		//	Passing back the response.
		try {
			blockingQueue.offer(extractJSONFragmentFromMessage(message), DEFAULT_TIMEOUT, SECONDS);
		} catch ( InterruptedException ex ) {
			LogUtilities.log(LOGGER, Level.SEVERE, ex, "Waiting for synchronous response be consumed interrupted [{0}].", message);
		}

	}

	private void sendAsynchronousJSONString( String messageType, String json ) {

		String message = MessageFormat.format(ASYNCH_FORMAT, applicationCode, messageType, json);
		Collection<WebSocket> sockets = accessor.getConnections();

		if ( !sockets.isEmpty() ) {
			for ( WebSocket socket : sockets ) {
				if ( socket.isOpen() ) {
					socket.send(message + DigestUtils.md5Hex(message));
				}
			}
		} else {
			LogUtilities.log(LOGGER, Level.FINE, "No available WebSockets to send message [{0}]", message);
		}

	}

	private String sendSynchronousJSONString( String messageType, String json, long timeout, TimeUnit unit ) {

		String messageIdentifier = UUID.nameUUIDFromBytes(messageType.getBytes()).toString();
		final String message = MessageFormat.format(SYNCH_FORMAT, applicationCode, Mode.SYNCH_CALL.name(), messageType, messageIdentifier, json);
		Collection<WebSocket> sockets = accessor.getConnections();

		if ( !sockets.isEmpty() ) {

			SynchronousQueue<String> blockingQueue = new SynchronousQueue<>(true);

			synchronized ( waitingCalls ) {
				waitingCalls.put(messageIdentifier, blockingQueue);
			}

			if ( sockets.size() > 1 ) {
				LogUtilities.log(LOGGER, Level.WARNING, "Too many WebSockets to send message: the first one will be used [{0}].", message);
			}

			final WebSocket socket = sockets.iterator().next();

			if ( socket.isOpen() ) {
				socket.send(message + DigestUtils.md5Hex(message));
			}

			//	Wait for the response.
			try {
				blockingQueue.offer(message, timeout, unit);
			} catch ( InterruptedException ex ) {
				LogUtilities.log(LOGGER, Level.SEVERE, ex, "Waiting for a synchronous response interrupted [{0}].", message);
			}

			//	Response received: get it.
			String response = null;

			try {
				response = blockingQueue.poll(timeout, unit);
			} catch ( InterruptedException ex ) {
				LogUtilities.log(LOGGER, Level.SEVERE, ex, "Reading a synchronous response interrupted [{0}].", message);
			}

			synchronized ( waitingCalls ) {
				waitingCalls.remove(messageIdentifier);
			}

			return response;

		} else {
			LogUtilities.log(LOGGER, Level.FINE, "No available WebSockets to send message [{0}].", message);
		}

		return null;

	}

}
