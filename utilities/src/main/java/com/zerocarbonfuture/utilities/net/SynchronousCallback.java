/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.net;


import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;


/**
 * The behavior of a synchronous callback.
 *
 * @param <T> The type of the {@link #call(java.lang.Object)} method parameter.
 * @param <R> The return type of the {@link #call(java.lang.Object)} method.
 * @author Claudio Rosati
 */
public abstract class SynchronousCallback<T, R> {

	private final Class<T> parameterClass;

	/**
	 * @param parameterClass The type of the {@link #call(java.lang.Object)} method.
	 */
	public SynchronousCallback( Class<T> parameterClass ) {
		this.parameterClass = parameterClass;
	}

	/**
	 * Callback invocation.
	 *
	 * @param messageObject The object sent asynchronously.
	 * @return The object to be sent back asynchronously.
	 */
	public abstract R call( T messageObject );

	String call( String json, ObjectMapper mapper ) throws IOException {
		return mapper.writeValueAsString(call(mapper.readValue(json, parameterClass)));
	}

}
