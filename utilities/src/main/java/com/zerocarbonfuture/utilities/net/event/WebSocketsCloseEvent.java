/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.net.event;


import java.util.EventObject;
import org.java_websocket.WebSocket;


/**
 * WebSockets close event.
 *
 * @author Claudio Rosati
 */
public class WebSocketsCloseEvent extends EventObject {

	private static final long serialVersionUID = 2327248612527753929L;
	private final int code;
	private final WebSocket connection;
	private final String reason;
	private final boolean remote;

	public WebSocketsCloseEvent( Object source, WebSocket connection, int code, String reason, boolean remote ) {

		super(source);

		this.connection = connection;
		this.code = code;
		this.reason = reason;
		this.remote = remote;

	}

	public int getCode() {
		return code;
	}

	public WebSocket getConnection() {
		return connection;
	}

	public String getReason() {
		return reason;
	}

	public boolean isRemote() {
		return remote;
	}

}
