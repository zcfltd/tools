/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.net;


import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;


/**
 * The behavior of an asynchronous callback.
 *
 * @param <T> The type of the {@link #call(java.lang.Object)} method parameter.
 * @author Claudio Rosati
 */
public abstract class AsynchronousCallback<T> {

	private final Class<T> parameterClass;

	/**
	 * @param parameterClass The type of the {@link #call(java.lang.Object)} method.
	 */
	public AsynchronousCallback( Class<T> parameterClass ) {
		this.parameterClass = parameterClass;
	}

	/**
	 * Callback invocation.
	 *
	 * @param messageObject The object sent asynchronously.
	 */
	public abstract void call( T messageObject );

	void call( String json, ObjectMapper mapper ) throws IOException {
		call(mapper.readValue(json, parameterClass));
	}

}
