/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.net;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.zerocarbonfuture.utilities.net.event.WebSocketsCloseListener;
import com.zerocarbonfuture.utilities.net.event.WebSocketsErrorListener;
import com.zerocarbonfuture.utilities.net.event.WebSocketsOpenListener;
import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import org.java_websocket.WebSocket;


/**
 * Common behavior of a web socket server/client implementation.
 *
 * @author Claudio Rosati
 */
public interface WebSocketsAccessor {

	/**
	 * Default timeout in seconds.
	 */
	public final static long DEFAULT_TIMEOUT = 7;

	/**
	 * Add a {@link WebSocketsCloseListener} to this object's listeners list.
	 *
	 * @param listener The {@link WebSocketsCloseListener} instance.
	 */
	public void addWebSocketsCloseListener( WebSocketsCloseListener listener );

	/**
	 * Add a {@link WebSocketsErrorListener} to this object's listeners list.
	 *
	 * @param listener The {@link WebSocketsErrorListener} instance.
	 */
	public void addWebSocketsErrorListener( WebSocketsErrorListener listener );

	/**
	 * Add a {@link WebSocketsOpenListener} to this object's listeners list.
	 *
	 * @param listener The {@link WebSocketsErrorListener} instance.
	 */
	public void addWebSocketsOpenListener( WebSocketsOpenListener listener );

	/**
	 * @return The string used to identify the application.
	 */
	public String getApplicationCode();

	/**
	 * @return A {@link Collection} of currently connected {@link WebSocket}s.
	 */
	public Collection<WebSocket> getConnections();

	/**
	 * Registers a {@code callback} associated with the given message type.
	 * <P>
	 * When a message is received, its message type will be used to retrieve
	 * all registered callback to be executed.
	 *
	 * @param <T>         The type of the {@link AsynchronousCallback#call(java.lang.Object)} method parameter.
	 * @param messageType The message type associated with the given {@code callback}.
	 * @param callback    The {@link AsynchronousCallback} to be registered
	 *                    and associated with the given message type.
	 * @throws IllegalArgumentException If one of the 2 parameters is {@code null}.
	 */
	public <T> void registerCallback( String messageType, AsynchronousCallback<T> callback ) throws IllegalArgumentException;

	/**
	 * Registers a {@code callback} associated with the given message type.
	 * <P>
	 * When a message is received, its message type will be used to retrieve
	 * all registered callbacks to be executed.
	 *
	 * @param <T>         The type of the {@link SynchronousCallback#call(java.lang.Object)} method parameter.
	 * @param <R>         The return type of the {@link SynchronousCallback#call(java.lang.Object)} method.
	 * @param messageType The message type associated with the given {@code callback}.
	 * @param callback    The {@link SynchronousCallback} to be registered
	 *                    and associated with the given message type.
	 */
	public <T, R> void registerCallback( String messageType, SynchronousCallback<T, R> callback ) throws IllegalArgumentException;

	/**
	 * Remove a previously registered {@link WebSocketsCloseListener}
	 * from this object's listeners list.
	 *
	 * @param listener The {@link WebSocketsCloseListener} instance.
	 */
	public void removeWebSocketsCloseListener( WebSocketsCloseListener listener );

	/**
	 * Remove a previously registered {@link WebSocketsErrorListener}
	 * from this object's listeners list.
	 *
	 * @param listener The {@link WebSocketsErrorListener} instance.
	 */
	public void removeWebSocketsErrorListener( WebSocketsErrorListener listener );

	/**
	 * Remove a previously registered {@link WebSocketsOpenListener}
	 * from this object's listeners list.
	 *
	 * @param listener The {@link WebSocketsOpenListener} instance.
	 */
	public void removeWebSocketsOpenListener( WebSocketsOpenListener listener );

	/**
	 * Send an asynchronous message, whose data is the given JSON-encoded string.
	 *
	 * @param <T>           The type of the object to be sent.
	 * @param messageType   The type of the message to be sent.
	 * @param messageObject The object to be sent.
	 * @throws com.fasterxml.jackson.core.JsonProcessingException If errors occur coding
	 *                                                            the given message object
	 *                                                            into a JSON string.
	 */
	public <T> void sendAsynchronous( String messageType, T messageObject ) throws JsonProcessingException;

	/**
	 * Send a synchronous message, whose data is the given JSON-encoded string,
	 * and wait for a returned value.
	 *
	 * @param <T>           The type of the object to be sent.
	 * @param <R>           The type of the object to be returned.
	 * @param messageType   The type of the message to be sent.
	 * @param messageObject The object to be sent.
	 * @param returnType    The {@link Class} of the object to be returned.
	 * @param timeout       How long to wait before giving up, in units of {@code unit}.
	 * @param unit          A {@link TimeUnit} determining how to interpret the {@code timeout} parameter.
	 * @return A string containing the JSON-encoded object.
	 * @throws java.io.IOException If errors occur coding the given message object
	 *                             into a JSON string, of decoding the returned
	 *                             string into the object to be returned.
	 */
	public <T, R> R sendSynchronous( String messageType, T messageObject, Class<R> returnType, long timeout, TimeUnit unit ) throws IOException;

	/**
	 * Initialise this WebSocket party.
	 *
	 * @return {@code true} if successfully started up.
	 * @throws java.lang.InterruptedException If start up was interrupted.
	 */
	public boolean startUp() throws InterruptedException;

	/**
	 * Terminate this WebSocket party.
	 *
	 * @param timeout How long to wait before giving up, in units of {@code unit}.
	 * @param unit    A {@link TimeUnit} determining how to interpret the {@code timeout} parameter.
	 * @throws java.io.IOException            If an I/O error occurred.
	 * @throws java.lang.InterruptedException If tear down was interrupted.
	 */
	public void tearDown( long timeout, TimeUnit unit ) throws IOException, InterruptedException;

	/**
	 * Unregister all callbacks associated with the given message type.
	 *
	 * @param messageType The message type associated with the callbacks to be removed.
	 * @throws IllegalArgumentException If the parameter is {@code null}.
	 */
	public void unregisterCallback( String messageType ) throws IllegalArgumentException;

}
