/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.net;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.zerocarbonfuture.utilities.net.event.WebSocketsCloseListener;
import com.zerocarbonfuture.utilities.net.event.WebSocketsErrorListener;
import com.zerocarbonfuture.utilities.net.event.WebSocketsOpenListener;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;


/**
 * A WebSockets server able to send messages to all connected clients,
 * and to invoke registered callbacks on incoming messages.
 *
 * @author Claudio Rosati
 */
class WebSocketsServer extends WebSocketServer implements WebSocketsAccessor {

	private static final Logger LOGGER = Logger.getLogger(WebSocketsServer.class.getName());

	private final AccessorSupport accessorSupport;

	WebSocketsServer( int port, String applicationCode ) {

		super(new InetSocketAddress(port));

		this.accessorSupport = new AccessorSupport(applicationCode, this);

	}

	@Override
	public void addWebSocketsCloseListener( WebSocketsCloseListener listener ) {
		accessorSupport.addWebSocketsCloseListener(listener);
	}

	@Override
	public void addWebSocketsErrorListener( WebSocketsErrorListener listener ) {
		accessorSupport.addWebSocketsErrorListener(listener);
	}

	@Override
	public void addWebSocketsOpenListener( WebSocketsOpenListener listener ) {
		accessorSupport.addWebSocketsOpenListener(listener);
	}

	@Override
	public String getApplicationCode() {
		return accessorSupport.getApplicationCode();
	}

	@Override
	public Collection<WebSocket> getConnections() {

		Collection<WebSocket> cons = connections();

		synchronized ( cons ) {
			return new ArrayList<>(cons);
		}

	}

	@Override
	public void onClose( WebSocket conn, int code, String reason, boolean remote ) {
		accessorSupport.onClose(LOGGER, conn, code, reason, remote);
	}

	@Override
	public void onError( WebSocket conn, Exception ex ) {
		accessorSupport.onError(LOGGER, conn, ex);
	}

	@Override
	public void onMessage( WebSocket conn, String message ) {
		accessorSupport.onMessage(LOGGER, conn, message);
	}

	@Override
	public void onOpen( WebSocket conn, ClientHandshake handshake ) {
		accessorSupport.onOpen(LOGGER, conn, handshake);
	}

	@Override
	public <T> void registerCallback( String messageType, AsynchronousCallback<T> callback ) throws IllegalArgumentException {
		accessorSupport.registerCallback(messageType, callback);
	}

	@Override
	public <T, R> void registerCallback( String messageType, SynchronousCallback<T, R> callback ) throws IllegalArgumentException {
		accessorSupport.registerCallback(messageType, callback);
	}

	@Override
	public void removeWebSocketsCloseListener( WebSocketsCloseListener listener ) {
		accessorSupport.removeWebSocketsCloseListener(listener);
	}

	@Override
	public void removeWebSocketsErrorListener( WebSocketsErrorListener listener ) {
		accessorSupport.removeWebSocketsErrorListener(listener);
	}

	@Override
	public void removeWebSocketsOpenListener( WebSocketsOpenListener listener ) {
		accessorSupport.removeWebSocketsOpenListener(listener);
	}

	@Override
	public <T> void sendAsynchronous( String messageType, T messageObject ) throws JsonProcessingException {
		accessorSupport.sendAsynchronous(messageType, messageObject);
	}

	@Override
	public <T, R> R sendSynchronous( String messageType, T messageObject, Class<R> returnType, long timeout, TimeUnit unit ) throws IOException {
		return accessorSupport.sendSynchronous(messageType, messageObject, returnType, timeout, unit);
	}

	@Override
	public boolean startUp() {

		start();

		return true;

	}

	@Override
	public void tearDown( long timeout, TimeUnit unit ) throws IOException, InterruptedException {

		try {

			stop(1000 * (int) DEFAULT_TIMEOUT);

			Collection<WebSocket> connections = new HashSet<>(connections());

			for ( WebSocket ws : connections ) {
				ws.close();
				removeConnection(ws);
				releaseBuffers(ws);
			}

		} finally {
			accessorSupport.shutDown(timeout, unit);
		}

	}

	@Override
	public void unregisterCallback( String messageType ) throws IllegalArgumentException {
		accessorSupport.unregisterCallback(messageType);
	}

}
