/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.net;


import java.net.URI;
import java.net.URISyntaxException;


/**
 * Factory class to create WebSockets servers and clients.
 *
 * @author Claudio Rosati
 */
public class WebSocketsFactory {

	/**
	 * Creates a client {@link WebSocketsAccessor}.
	 *
	 * @param host            The name of IP of the remote server.
	 * @param port            The port of the remote server.
	 * @param applicationCode The code used to identify messages to/from this client.
	 *                        Clients and server must user the same code, otherwise
	 *                        messages will be discarded.
	 * @return The created client {@link WebSocketsAccessor}.
	 * @throws URISyntaxException If the "ws//{@code host}:{@code port}" {@link URI}
	 *                            is wrong.
	 */
	public static WebSocketsAccessor createClient( String host, int port, String applicationCode ) throws URISyntaxException {
		return new WebSocketsClient(host, port, applicationCode);
	}

	/**
	 * Creates a server {@link WebSocketsAccessor}.
	 *
	 * @param port            The communication port.
	 * @param applicationCode The code used to identify messages to/from this server.
	 *                        Clients and server must user the same code, otherwise
	 *                        messages will be discarded.
	 * @return The created server {@link WebSocketsAccessor}.
	 */
	public static WebSocketsAccessor createServer( int port, String applicationCode ) {
		return new WebSocketsServer(port, applicationCode);
	}

	private WebSocketsFactory() {
	}

}
