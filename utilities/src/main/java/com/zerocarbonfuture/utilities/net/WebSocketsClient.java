/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.net;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.zerocarbonfuture.utilities.net.event.WebSocketsCloseListener;
import com.zerocarbonfuture.utilities.net.event.WebSocketsErrorListener;
import com.zerocarbonfuture.utilities.net.event.WebSocketsOpenListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ServerHandshake;


/**
 * A WebSockets client able to send messages to a connected server,
 * and to invoke registered callbacks on incoming messages.
 *
 * @author Claudio Rosati
 */
class WebSocketsClient extends WebSocketClient implements WebSocketsAccessor {

	private static final Logger LOGGER = Logger.getLogger(WebSocketsClient.class.getName());

	private final AccessorSupport accessorSupport;

	WebSocketsClient( String host, int port, String applicationCode ) throws URISyntaxException {

		super(new URI(MessageFormat.format("ws://{0}:{1,number,#####}", host, port)), new Draft_17());

		this.accessorSupport = new AccessorSupport(applicationCode, this);

	}

	@Override
	public void addWebSocketsCloseListener( WebSocketsCloseListener listener ) {
		accessorSupport.addWebSocketsCloseListener(listener);
	}

	@Override
	public void addWebSocketsErrorListener( WebSocketsErrorListener listener ) {
		accessorSupport.addWebSocketsErrorListener(listener);
	}

	@Override
	public void addWebSocketsOpenListener( WebSocketsOpenListener listener ) {
		accessorSupport.addWebSocketsOpenListener(listener);
	}

	@Override
	public String getApplicationCode() {
		return accessorSupport.getApplicationCode();
	}

	@Override
	public Collection<WebSocket> getConnections() {
		return Collections.singleton(getConnection());
	}

	@Override
	public void onClose( int code, String reason, boolean remote ) {
		accessorSupport.onClose(LOGGER, getConnection(), code, reason, remote);
	}

	@Override
	public void onError( Exception ex ) {
		accessorSupport.onError(LOGGER, getConnection(), ex);
	}

	@Override
	public void onMessage( String message ) {
		accessorSupport.onMessage(LOGGER, getConnection(), message);
	}

	@Override
	public void onOpen( ServerHandshake handshake ) {
		accessorSupport.onOpen(LOGGER, getConnection(), handshake);
	}

	@Override
	public <T> void registerCallback( String messageType, AsynchronousCallback<T> callback ) throws IllegalArgumentException {
		accessorSupport.registerCallback(messageType, callback);
	}

	@Override
	public <T, R> void registerCallback( String messageType, SynchronousCallback<T, R> callback ) throws IllegalArgumentException {
		accessorSupport.registerCallback(messageType, callback);
	}

	@Override
	public void removeWebSocketsCloseListener( WebSocketsCloseListener listener ) {
		accessorSupport.removeWebSocketsCloseListener(listener);
	}

	@Override
	public void removeWebSocketsErrorListener( WebSocketsErrorListener listener ) {
		accessorSupport.removeWebSocketsErrorListener(listener);
	}

	@Override
	public void removeWebSocketsOpenListener( WebSocketsOpenListener listener ) {
		accessorSupport.removeWebSocketsOpenListener(listener);
	}

	@Override
	public <T> void sendAsynchronous( String messageType, T messageObject ) throws JsonProcessingException {
		accessorSupport.sendAsynchronous(messageType, messageObject);
	}

	@Override
	public <T, R> R sendSynchronous( String messageType, T messageObject, Class<R> returnType, long timeout, TimeUnit unit ) throws IOException {
		return accessorSupport.sendSynchronous(messageType, messageObject, returnType, timeout, unit);
	}

	@Override
	public boolean startUp() throws InterruptedException {
		return connectBlocking();
	}

	@Override
	public void tearDown( long timeout, TimeUnit unit ) throws IOException, InterruptedException {
		try {
			closeBlocking();
			getConnection().close();
		} finally {
			accessorSupport.shutDown(timeout, unit);
		}
	}

	@Override
	public void unregisterCallback( String messageType ) throws IllegalArgumentException {
		accessorSupport.unregisterCallback(messageType);
	}

}
