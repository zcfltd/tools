/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.net.event;


import java.util.EventObject;
import org.java_websocket.WebSocket;


/**
 * WebSockets error event.
 *
 * @author Claudio Rosati
 */
public class WebSocketsErrorEvent extends EventObject {

	private static final long serialVersionUID = -6207433938164600509L;
	private final WebSocket connection;
	private final Exception exception;

	public WebSocketsErrorEvent( Object source, WebSocket connection, Exception exception ) {

		super(source);

		this.connection = connection;
		this.exception = exception;

	}

	public WebSocket getConnection() {
		return connection;
	}

	public Exception getException() {
		return exception;
	}

}
