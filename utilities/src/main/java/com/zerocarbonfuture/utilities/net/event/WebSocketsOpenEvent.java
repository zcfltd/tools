/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.net.event;


import java.util.EventObject;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.Handshakedata;


/**
 * WebSockets open event.
 *
 * @author Claudio Rosati
 */
public class WebSocketsOpenEvent extends EventObject {

	private static final long serialVersionUID = -8976008054748022161L;
	private final WebSocket connection;
	private final Handshakedata handshake;

	public WebSocketsOpenEvent( Object source, WebSocket connection, Handshakedata handshake ) {

		super(source);

		this.connection = connection;
		this.handshake = handshake;

	}

	public WebSocket getConnection() {
		return connection;
	}

	public Handshakedata getHandshake() {
		return handshake;
	}

}
