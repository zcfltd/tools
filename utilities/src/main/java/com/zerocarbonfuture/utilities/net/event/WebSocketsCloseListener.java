/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.net.event;


import java.util.EventListener;


/**
 * An {@link EventListener} of WebSockets close events.
 *
 * @author Claudio Rosati
 */
@FunctionalInterface
public interface WebSocketsCloseListener extends EventListener {

	public void connectionClosed( WebSocketsCloseEvent event );

}
