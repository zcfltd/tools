/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.net;


/**
 * @author Claudio Rosati
 */
public class IllegalMessageFormat extends IllegalArgumentException {

	private static final long serialVersionUID = -928759639424759394L;

	public IllegalMessageFormat( String reason ) {
		super(reason);
	}

	public IllegalMessageFormat( String reason, Throwable cause ) {
		super(reason, cause);
	}

}
