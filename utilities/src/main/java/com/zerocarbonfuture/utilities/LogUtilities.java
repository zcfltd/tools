/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities;


import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Utilities to log messages.
 *
 * @author Claudio Rosati
 */
public final class LogUtilities {

	/**
	 * Log a message, with associated {@link Throwable} information.
	 *
	 * @param logger The {@link Logger} to log the message.
	 * @param level  One of the message level identifiers, e.g., {@link Level#SEVERE}.
	 * @param msg    The string message.
	 */
	public static void log( Logger logger, Level level, String msg ) {
		logger.log(level, msg);
	}

	/**
	 * Log a message, with associated {@link Throwable} information.
	 *
	 * @param logger The {@link Logger} to log the message.
	 * @param level  One of the message level identifiers, e.g., {@link Level#SEVERE}.
	 * @param msg    The string message.
	 * @param params Array of parameters to the message.
	 */
	public static void log( Logger logger, Level level, String msg, Object... params ) {
		logger.log(level, MessageFormat.format(msg, params));
	}

	/**
	 * Log a message, with associated {@link Throwable} information.
	 *
	 * @param logger The {@link Logger} to log the message.
	 * @param level  One of the message level identifiers, e.g., {@link Level#SEVERE}.
	 * @param thrown {@link Throwable} associated with log message.
	 * @param msg    The string message.
	 */
	public static void log( Logger logger, Level level, Throwable thrown, String msg ) {
		logger.log(level, msg, thrown);
	}

	/**
	 * Log a message, with associated {@link Throwable} information.
	 *
	 * @param logger The {@link Logger} to log the message.
	 * @param level  One of the message level identifiers, e.g., {@link Level#SEVERE}.
	 * @param thrown {@link Throwable} associated with log message.
	 * @param msg    The string message.
	 * @param params Array of parameters to the message.
	 */
	public static void log( Logger logger, Level level, Throwable thrown, String msg, Object... params ) {
		logger.log(level, MessageFormat.format(msg, params), thrown);
	}

	private LogUtilities() {
	}

}
