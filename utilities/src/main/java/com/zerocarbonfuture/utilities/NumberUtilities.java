/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities;


import org.apache.commons.lang3.BooleanUtils;


/**
 * Various utilities handling numbers.
 *
 * @author Claudio Rosati
 */
public class NumberUtilities {

	/**
	 * Converts a boolean to a {@link Number} using the convention that
	 * 0 is {@code false} and 1 is {@code true}.
	 *
	 * @param bool The {@link Boolean} to be converted.
	 * @return 0 if {@code bool} is {@code false}, 1 if {@code bool} is {@code true}.
	 * @throws NullPointerException If {@code bool} is {@code null}.
	 */
	public static Number toNumber( Boolean bool ) {
		if ( bool == null ) {
			throw new NullPointerException("object");
		} else {
			return BooleanUtils.toInteger(bool);
		}
	}

	/**
	 * Returns the given {@code number}. This method exists to allow
	 * a unified handling of {@link Number}s and {@link Boolean}s.
	 *
	 * @param number The {@link Number} to be returned.
	 * @return The given {@code number}.
	 * @throws NullPointerException If (@code number} is {@code null}.
	 */
	public static Number toNumber( Number number ) {
		if ( number == null ) {
			throw new NullPointerException("object");
		} else {
			return number;
		}
	}

	/**
	 * Returns the given {@code object} as a {@link Number}.
	 *
	 * @param object The object to be returned as a number.
	 * @return The given {@code object} as a {@link Number}.
	 * @throws NullPointerException     If (@code object} is {@code null}.
	 * @throws IllegalArgumentException If (@code object} is neither a
	 *                                  {@link Number}, nor a {@link Boolean}.
	 */
	public static Number toNumber( Object object ) {
		if ( object == null ) {
			throw new NullPointerException("object");
		} else if ( object instanceof Number ) {
			return toNumber((Number) object);
		} else if ( object instanceof Boolean ) {
			return toNumber((Boolean) object);
		} else {
			throw new IllegalArgumentException("'object' not a number.");
		}
	}

	private NumberUtilities() {
	}

}
