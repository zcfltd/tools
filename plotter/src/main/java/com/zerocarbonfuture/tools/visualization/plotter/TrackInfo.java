/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.tools.visualization.plotter;


import com.zerocarbonfuture.utilities.ColorUtilities;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Stroke;
import java.text.NumberFormat;
import org.jfree.chart.LegendItem;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardCrosshairLabelGenerator;
import org.jfree.chart.plot.Crosshair;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.ui.RectangleAnchor;


/**
 * Data for a single chart track.
 *
 * @author Claudio Rosati
 */
public class TrackInfo {

	private static final Stroke LEGEND_LINE_STROKE = new BasicStroke(3.333F);

	private final NumberAxis axis;
	private final Color color;
	private final Crosshair crosshair;
	private final TimeSeriesCollection dataset;
	private final int index;
	private final XYPlot plot;
	private final int plotIndex;
	private final TimeSeries timeSeries;

	/**
	 * Creates a new TrackInfo wrapping the given parameters.
	 *
	 * @param plot      The {@link XYPlot} where the track will be drawn.
	 * @param plotIndex The index of the {@code plot} inside its container.
	 * @param left      Whether the {@link NumberAxis} will be drawn on the
	 *                  left or right side of the chart.
	 * @param color     The {@link Color} of the track.
	 * @return A new TrackInfo wrapping the given parameters.
	 */
	public static TrackInfo create(
		XYPlot plot, int plotIndex, boolean left, Color color ) {
		return create(null, Integer.MAX_VALUE, plot, plotIndex, left, color);
	}

	/**
	 * Creates a new TrackInfo wrapping the given parameters.
	 *
	 * @param name      The name of the track. If {@code null},
	 *                  {@code descriptor.getName()} will be used.
	 * @param plot      The {@link XYPlot} where the track will be drawn.
	 * @param plotIndex The index of the {@code plot} inside its container.
	 * @param left      Whether the {@link NumberAxis} will be drawn on the
	 *                  left or right side of the chart.
	 * @param color     The {@link Color} of the track.
	 * @return A new TrackInfo wrapping the given parameters.
	 */
	public static TrackInfo create( String name, XYPlot plot, int plotIndex, boolean left, Color color ) {
		return create(name, Integer.MAX_VALUE, plot, plotIndex, left, color);
	}

	/**
	 * Creates a new TrackInfo wrapping the given parameters.
	 *
	 * @param maxItemsCount The maximum number of entries in the track
	 *                      {@link TimeSeries}.
	 * @param plot          The {@link XYPlot} where the track will be drawn.
	 * @param plotIndex     The index of the {@code plot} inside its container.
	 * @param left          Whether the {@link NumberAxis} will be drawn on the
	 *                      left or right side of the chart.
	 * @param color         The {@link Color} of the track.
	 * @return A new TrackInfo wrapping the given parameters.
	 */
	public static TrackInfo create( int maxItemsCount, XYPlot plot, int plotIndex, boolean left, Color color ) {
		return create(null, maxItemsCount, plot, plotIndex, left, color);
	}

	/**
	 * Creates a new TrackInfo wrapping the given parameters.
	 *
	 * @param name          The name of the track. If {@code null},
	 *                      {@code descriptor.getName()} will be used.
	 * @param maxItemsCount The maximum number of entries in the track
	 *                      {@link TimeSeries}.
	 * @param plot          The {@link XYPlot} where the track will be drawn.
	 * @param plotIndex     The index of the {@code plot} inside its container.
	 * @param left          Whether the {@link NumberAxis} will be drawn on the
	 *                      left or right side of the chart.
	 * @param color         The {@link Color} of the track.
	 * @return A new TrackInfo wrapping the given parameters.
	 */
	public static TrackInfo create( String name, int maxItemsCount, XYPlot plot, int plotIndex, boolean left, Color color ) {

		TimeSeries timeSeries = new TimeSeries(name);

		timeSeries.setMaximumItemCount(maxItemsCount);

		return create(name, timeSeries, plot, plotIndex, left, color);

	}

	/**
	 * Creates a new TrackInfo wrapping the given parameters.
	 *
	 * @param timeSeries The {@link TimeSeries} of the track.
	 * @param plot       The {@link XYPlot} where the track will be drawn.
	 * @param plotIndex  The index of the {@code plot} inside its container.
	 * @param left       Whether the {@link NumberAxis} will be drawn on the
	 *                   left or right side of the chart.
	 * @param color      The {@link Color} of the track.
	 * @return A new TrackInfo wrapping the given parameters.
	 */
	public static TrackInfo create( TimeSeries timeSeries, XYPlot plot, int plotIndex, boolean left, Color color ) {
		return create(null, timeSeries, plot, plotIndex, left, color);
	}

	/**
	 * Creates a new TrackInfo wrapping the given parameters.
	 *
	 * @param name       The name of the track. If {@code null},
	 *                   {@code descriptor.getName()} will be used.
	 * @param timeSeries The {@link TimeSeries} of the track.
	 * @param plot       The {@link XYPlot} where the track will be drawn.
	 * @param plotIndex  The index of the {@code plot} inside its container.
	 * @param left       Whether the {@link NumberAxis} will be drawn on the
	 *                   left or right side of the chart.
	 * @param color      The {@link Color} of the track.
	 * @return A new TrackInfo wrapping the given parameters.
	 */
	public static TrackInfo create( String name, TimeSeries timeSeries, XYPlot plot, int plotIndex, boolean left, Color color ) {

		TimeSeriesCollection dataset = new TimeSeriesCollection(timeSeries);
		NumberAxis axis = new NumberAxis(name);

		axis.setAutoRangeIncludesZero(false);
		axis.setLabelFont(axis.getLabelFont().deriveFont(Font.BOLD, 8.5F));
		axis.setTickLabelFont(axis.getTickLabelFont().deriveFont(6.5F));

		int index = plot.getDatasetCount();

		plot.setDataset(index, dataset);
		plot.setRenderer(
			index,
			new XYLineAndShapeRenderer(true, false){
				private static final long serialVersionUID = 2285026210345009329L;

				@Override
				public LegendItem getLegendItem( int datasetIndex, int series ) {

					LegendItem litem = super.getLegendItem(datasetIndex, series);

					litem.setLineStroke(LEGEND_LINE_STROKE);

					return litem;

				}
			}
		);
		plot.setRangeAxis(index, axis);
		plot.setRangeAxisLocation(index, left ? AxisLocation.TOP_OR_LEFT : AxisLocation.BOTTOM_OR_RIGHT);
		plot.mapDatasetToRangeAxis(index, index);
		plot.getRenderer(index).setSeriesPaint(0, color);
		plot.getRangeAxis(index).setLabelPaint(color);
		plot.getRangeAxis(index).setTickLabelPaint(color);

		Crosshair crosshair = new Crosshair(Double.NaN, color, new BasicStroke(0f));

		crosshair.setLabelBackgroundPaint(ColorUtilities.makeTransparent(color, 63));
		crosshair.setLabelOutlinePaint(ColorUtilities.darker(color, 0.25));
		crosshair.setLabelGenerator(new StandardCrosshairLabelGenerator(" \u200A{0} ", NumberFormat.getNumberInstance()));
		crosshair.setLabelVisible(true);
		crosshair.setLabelAnchor(left ? RectangleAnchor.TOP_LEFT : RectangleAnchor.BOTTOM_RIGHT);
		crosshair.setVisible(false);

		return new TrackInfo(timeSeries, dataset, axis, plot, plotIndex, index, color, crosshair);

	}

	/**
	 * Creates a new instance of {@link TrackInfo}.
	 *
	 * @param timeSeries The {@link TimeSeries} containing the data of the track.
	 * @param dataset    The {@link TimeSeriesCollection} of the track.
	 * @param axis       The {@link NumberAxis} of the track.
	 * @param plot       The {@link XYPlot} where the track will be drawn.
	 * @param plotIndex  The index of the {@code plot} inside its container.
	 * @param index      The index inside {@code plot} of the track data.
	 * @param color      The {@link Color} of the track.
	 * @param crosshair  The {@link Crosshair} of the track.
	 */
	TrackInfo( TimeSeries timeSeries, TimeSeriesCollection dataset, NumberAxis axis, XYPlot plot, int plotIndex, int index, Color color, Crosshair crosshair ) {
		this.timeSeries = timeSeries;
		this.dataset = dataset;
		this.axis = axis;
		this.plot = plot;
		this.plotIndex = plotIndex;
		this.index = index;
		this.color = color;
		this.crosshair = crosshair;
	}

	/**
	 * @return The {@link NumberAxis} of the track.
	 */
	public NumberAxis getAxis() {
		return axis;
	}

	/**
	 * @return The {@link Color} of the track.
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * @return The {@link Crosshair} of the track.
	 */
	public Crosshair getCrosshair() {
		return crosshair;
	}

	/**
	 * @return The {@link TimeSeriesCollection} of the track.
	 */
	public TimeSeriesCollection getDataset() {
		return dataset;
	}

	/**
	 * @return The index inside {@code plot} of the track data.
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @return The {@link XYPlot} where the track will be drawn.
	 */
	public XYPlot getPlot() {
		return plot;
	}

	/**
	 * @return The index of the {@code plot} inside its container.
	 */
	public int getPlotIndex() {
		return plotIndex;
	}

	/**
	 * @return The {@link TimeSeries} containing the data of the track.
	 */
	public TimeSeries getTimeSeries() {
		return timeSeries;
	}

}
