/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.tools.visualization.plotter;


import com.zerocarbonfuture.utilities.ColorUtilities;
import com.zerocarbonfuture.utilities.LogUtilities;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PatternOptionBuilder;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.lang3.ArrayUtils;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.Axis;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.entity.AxisEntity;
import org.jfree.chart.panel.CrosshairOverlay;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.Crosshair;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.general.DatasetUtilities;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.ui.RectangleEdge;

import static com.zerocarbonfuture.tools.visualization.plotter.TrackInfo.create;


/**
 * Plots data reading CSV file.
 *
 * @author Claudio Rosati
 */
public class Plotter extends JFrame implements ChartMouseListener {

	public static final DateFormat TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	private static final Logger LOGGER = Logger.getLogger(Plotter.class.getName());
	private static final String SEPARATOR_CHAR = "\t";
	private static final long serialVersionUID = -1488377379726088775L;

	private ChartPanel chartPanel;
	private CrosshairOverlay crosshairOverlay;
	private NumberAxis firstSelectedAxis = null;
	private String firstSelectedAxisLabel = null;
	private final JToggleButton hCrosshairButton = new JToggleButton(
		new ImageIcon(Plotter.class.getResource("resources/h-crosshair.png")),
		false
	);
	private final JToggleButton legendButton = new JToggleButton(
		new ImageIcon(Plotter.class.getResource("resources/legend.png")),
		false
	);
	private final JToggleButton lockVerticalZoomButton = new JToggleButton(
		new ImageIcon(Plotter.class.getResource("resources/lock-v-zoom.png")),
		true
	);
	private CombinedDomainXYPlot masterPlot;
	private final List<XYPlot> plots = new ArrayList<>(1);
	private DateAxis timeAxis;
	private Crosshair timeCrosshair;
	private final JToolBar toolbar = new JToolBar();
	private final Set<TrackInfo> trackInfos = new HashSet<>(1);
	private final JToggleButton vCrosshairButton = new JToggleButton(
		new ImageIcon(Plotter.class.getResource("resources/v-crosshair.png")),
		false
	);

	public static void main ( String[] args) {

		Options options = new Options();

		try {

			String separator = SEPARATOR_CHAR;
			int headerLines = 1;
			File csvFile = null;
			TimeValue time = TimeValue.SECONDS;
			CommandLine commandLine = parse(args, options);

			if ( commandLine.hasOption('h') ) {
				new HelpFormatter().printHelp("Usage: java -jar csv-plotter.jar ", options);
				return;
			} else {
				if ( commandLine.hasOption('f') ) {
					csvFile = (File) commandLine.getParsedOptionValue("f");
				}
				if ( commandLine.hasOption('l') ) {
					headerLines = ((Number) commandLine.getParsedOptionValue("l")).intValue();
				}
				if ( commandLine.hasOption('s') ) {
					separator = commandLine.getOptionValue('s');
				}
				if ( commandLine.hasOption('t') ) {
					if ( TimeValue.HOURS.name().toUpperCase().equals(commandLine.getOptionValue('t').toUpperCase()) ) {
						time = TimeValue.HOURS;
					}
				}
			}

			final Plotter plotter = new Plotter(csvFile);

			SwingUtilities.invokeAndWait(() -> {
				plotter.pack();
				plotter.setVisible(true);
			});

			plotter.plot(csvFile, headerLines, time, separator);

		} catch ( ParseException | InterruptedException | InvocationTargetException ex ) {
			JOptionPane.showMessageDialog(
				null,
				MessageFormat.format(
					"Unable to run Plotter with arguments: {0}.\n{1}",
					ArrayUtils.toString(args),
					ex.getMessage()
				),
				"Plotter",
				JOptionPane.ERROR_MESSAGE
			);
		}

	}

	private static CommandLine parse ( String[] args, Options options) throws ParseException {

		@SuppressWarnings( "static-access" )
			Option fileOption = OptionBuilder
				.withArgName("csv-file")
				.hasArg()
				.isRequired()
				.withLongOpt("file")
				.withDescription("The CSV file to be plotted.")
				.withType(PatternOptionBuilder.FILE_VALUE)
				.create('f');
		@SuppressWarnings( "static-access" )
			Option headerOption = OptionBuilder
				.withArgName("header-lines")
				.hasArg()
				.withLongOpt("header")
				.withDescription("The number of lines in header. Default value is 1.")
				.withType(PatternOptionBuilder.NUMBER_VALUE)
				.create('l');
		@SuppressWarnings( "static-access" )
			Option separatorOption = OptionBuilder
				.withArgName("separator-string")
				.hasArg()
				.withLongOpt("separator")
				.withDescription("The separator char/string. Default is tab.")
				.withType(PatternOptionBuilder.STRING_VALUE)
				.create('s');
		@SuppressWarnings( "static-access" )
			Option timeOption = OptionBuilder
				.withArgName("'hours'|'seconds'")
				.hasArg()
				.withLongOpt("time")
				.withDescription("How time is represented. Valid values are 'hours' and 'seconds'. Default is 'seconds'.")
				.withType(PatternOptionBuilder.STRING_VALUE)
				.create('t');
		@SuppressWarnings( "static-access" )
			Option helpOption = OptionBuilder
				.hasArg(false)
				.withLongOpt("help")
				.withDescription("Prints this help message.")
				.create('h');

		options.addOption(fileOption);
		options.addOption(headerOption);
		options.addOption(separatorOption);
		options.addOption(timeOption);
		options.addOption(helpOption);

		CommandLineParser parser = new PosixParser();

		return parser.parse(options, args);

	}

	private Plotter ( File csvFile) {

		super("Plotter [" + csvFile.toPath() + "]");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationByPlatform(true);
		setPreferredSize(new Dimension(800, 600));
		setExtendedState(JFrame.MAXIMIZED_BOTH);

		initComponents(csvFile);

	}

	@Override
	public void chartMouseClicked ( ChartMouseEvent event ) {

		if ( event.getEntity() instanceof AxisEntity ) {

			Axis axis = ((AxisEntity) event.getEntity()).getAxis();

			if ( axis instanceof NumberAxis ) {

				NumberAxis naxis = (NumberAxis) axis;

				MouseEvent mevent = event.getTrigger();

				if ( mevent.isAltDown() && mevent.isControlDown() ) {
					if ( mevent.isShiftDown() ) {
						naxis.setAutoRange(true);
					} else {
						if ( firstSelectedAxis == null ) {

							firstSelectedAxis = naxis;
							firstSelectedAxisLabel = naxis.getLabel();

							naxis.setLabel("\u2588\u2588\u2588 " + naxis.getLabel() + " \u2588\u2588\u2588");

							return;

						} else {

							double lowerBound = Math.min(firstSelectedAxis.getRange().getLowerBound(), naxis.getRange().getLowerBound());
							double upperBound = Math.max(firstSelectedAxis.getRange().getUpperBound(), naxis.getRange().getUpperBound());

							firstSelectedAxis.setRange(lowerBound, upperBound);
							naxis.setRange(lowerBound, upperBound);

						}
					}
				}

			}

		}

		if ( firstSelectedAxis != null && firstSelectedAxisLabel != null ) {
			firstSelectedAxis.setLabel(firstSelectedAxisLabel);
		}

		firstSelectedAxis = null;
		firstSelectedAxisLabel = null;

	}

	@Override
	public void chartMouseMoved ( ChartMouseEvent event ) {

		Rectangle2D dataArea = chartPanel.getScreenDataArea();
		double time = timeAxis.java2DToValue(event.getTrigger().getX(), dataArea, RectangleEdge.TOP);

		timeCrosshair.setValue(time);

		trackInfos.stream().forEach(trackInfo -> {
			trackInfo.getCrosshair().setValue(DatasetUtilities.findYValue(trackInfo.getDataset(), 0, time));
		});

	}

	private TrackInfo addTrackInfo ( TrackInfo trackInfo ) {

		trackInfos.add(trackInfo);

		Crosshair crosshair = trackInfo.getCrosshair();

		crosshair.setVisible(hCrosshairButton.isSelected());
		crosshairOverlay.addRangeCrosshair(crosshair);

		return trackInfo;

	}

	private void createPlot () {

		XYPlot plot = new XYPlot();

		plot.setBackgroundPaint(ColorUtilities.P_MERCURY);
		masterPlot.add(plot);
		plots.add(plot);

	}

	@SuppressWarnings( "NestedAssignment" )
	private void fillTimeSeries ( File file, int hLines, TimeSeries timeSeries, int valueIndex, TimeValue time, String separator ) {

		final int timeIndex = 0;

		try ( BufferedReader reader = new BufferedReader(new FileReader(file)) ) {

			for ( int h = 0; h < hLines; h++ ) {
				reader.readLine();
			}

			String line;

			while ( (line = reader.readLine()) != null ) {
				try {

					String[] values = line.split(separator);
					double value = Double.parseDouble(values[valueIndex]);
					double dtime = Double.parseDouble(values[timeIndex]);
					long ltime = (time == TimeValue.SECONDS) ? (long) dtime : (long) (dtime * 60 * 60);

					timeSeries.addOrUpdate(new Second(new Date(1000L * ltime)), value);

				} catch ( NumberFormatException ex ) {
					LogUtilities.log(LOGGER, Level.WARNING, ex, "Unable to read data file line {0}: {1}.", line, file.getPath());
				}
			}

		} catch ( FileNotFoundException ex ) {
			LogUtilities.log(LOGGER, Level.WARNING, ex, "Unable to find data file: {0}.", file.getPath());
		} catch ( IOException ex ) {
			LogUtilities.log(LOGGER, Level.WARNING, ex, "Problems accessing data file: {1}.", file.getPath());
		}

	}

	private void initComponents ( File csvFile ) {

		vCrosshairButton.addActionListener(e -> {

			if ( (e.getModifiers() & ActionEvent.CTRL_MASK) == ActionEvent.CTRL_MASK
				&& hCrosshairButton.isSelected() != vCrosshairButton.isSelected() ) {
				hCrosshairButton.doClick();
			}

			timeCrosshair.setVisible(vCrosshairButton.isSelected());

		});
		vCrosshairButton.setToolTipText("Time Crosshair");

		hCrosshairButton.addActionListener(e -> {

			if ( (e.getModifiers() & ActionEvent.CTRL_MASK) == ActionEvent.CTRL_MASK
				&& hCrosshairButton.isSelected() != vCrosshairButton.isSelected() ) {
				vCrosshairButton.doClick();
			}

			trackInfos.stream().forEach(t -> {
				if ( t.getCrosshair() != null ) {
					t.getCrosshair().setVisible(hCrosshairButton.isSelected());
				}
			});

		});
		hCrosshairButton.setToolTipText("Value Crosshairs");

		legendButton.addActionListener(e -> chartPanel.getChart().getLegend().setVisible(legendButton.isSelected()));
		legendButton.setToolTipText("Legend");

		lockVerticalZoomButton.addActionListener(e -> chartPanel.setRangeZoomable(!lockVerticalZoomButton.isSelected()));
		lockVerticalZoomButton.setToolTipText("Lock Vertical Zoom");

		toolbar.addSeparator();
		toolbar.add(vCrosshairButton);
		toolbar.add(hCrosshairButton);
		toolbar.addSeparator();
		toolbar.add(lockVerticalZoomButton);
		toolbar.addSeparator();
		toolbar.add(legendButton);
		toolbar.setFloatable(false);

		timeAxis = new DateAxis("Time");

		timeAxis.setLowerMargin(0.02);    //    Reduce the default margins.
		timeAxis.setUpperMargin(0.02);
		timeAxis.setAutoRange(true);
		timeAxis.setDateFormatOverride(TIME_FORMAT);
		timeAxis.setLabelFont(timeAxis.getLabelFont().deriveFont(Font.BOLD, 8.5F));
		timeAxis.setLabelPaint(ColorUtilities.P_LEAD);
		timeAxis.setTickLabelFont(timeAxis.getTickLabelFont().deriveFont(6.5F));
		timeAxis.setTickMarkPaint(ColorUtilities.P_LEAD);

		crosshairOverlay = new MultiPlotCrosshairOverlay();
		timeCrosshair = new Crosshair(Double.NaN, ColorUtilities.P_MAGNESIUM, new BasicStroke(0F));

		timeCrosshair.setLabelBackgroundPaint(ColorUtilities.makeTransparent(ColorUtilities.P_MAGNESIUM, 63));
		timeCrosshair.setLabelOutlinePaint(ColorUtilities.darker(ColorUtilities.P_MAGNESIUM, 0.25));
		timeCrosshair.setLabelGenerator(c -> "\u200A " + TIME_FORMAT.format(c.getValue()) + " ");
		timeCrosshair.setLabelVisible(true);
		timeCrosshair.setVisible(vCrosshairButton.isSelected());
		crosshairOverlay.addDomainCrosshair(timeCrosshair);

		masterPlot = new CombinedDomainXYPlot(timeAxis);

		masterPlot.setGap(10);
		masterPlot.setDomainPannable(true);
		masterPlot.setRangePannable(true);

		JFreeChart chart = new JFreeChart(csvFile.getName(), JFreeChart.DEFAULT_TITLE_FONT, masterPlot, true);

		chart.setAntiAlias(true);
		chart.setTextAntiAlias(true);
		chart.setBackgroundPaint(ColorUtilities.P_WHITE);
		chart.getLegend().setVisible(false);

		TextTitle title = chart.getTitle();

		title.setFont(title.getFont().deriveFont(13F));
		title.setPaint(ColorUtilities.P_LEAD);

		chartPanel = new ChartPanel(chart);

		chartPanel.addChartMouseListener(this);
		chartPanel.addOverlay(crosshairOverlay);
		chartPanel.setDomainZoomable(true);
		chartPanel.setRangeZoomable(false);

		setLayout(new BorderLayout());
		add(toolbar, BorderLayout.NORTH);
		add(chartPanel, BorderLayout.CENTER);

	}

	private void loadTrackInfo ( File csvFile, int headerLines, TimeValue time, String separator ) {

		String[] headers = null;

		try ( BufferedReader reader = new BufferedReader(new FileReader(csvFile)) ) {

			String line = reader.readLine();

			headers = line.split(separator);

			for ( int i = 0; i < headers.length; i++ ) {
				headers[i] = headers[i].trim();
			}

			if ( headerLines > 1 ) {

				for ( int i = 0; i < headers.length; i++ ) {
					headers[i] += " [";
				}

				for ( int hl = 1; hl < headerLines; hl++ ) {

					line = reader.readLine();

					String[] hdrs = line.split(separator);

					for ( int i = 0; i < Math.min(headers.length, hdrs.length); i++ ) {

						headers[i] += hdrs[i].trim();

						if ( hl < headerLines - 1 ) {
							headers[i] += ", ";
						}

					}

				}

				for ( int i = 0; i < headers.length; i++ ) {
					headers[i] += "]";
				}

			}

		} catch ( IOException ex ) {
			JOptionPane.showMessageDialog(
				null,
				MessageFormat.format(
					"Errors running Plotter with arguments: -f {0} -l {1}.\n{2}",
					csvFile.toPath(),
					headerLines,
					ex.getMessage()
				),
				"Plotter",
				JOptionPane.ERROR_MESSAGE
			);
		}

		createPlot();

		if ( headers != null ) {
			for ( int i = 1; i < headers.length; i++ ) {

				final String name = headers[i];
				final TimeSeries timeSeries = new TimeSeries(name);
				final int index = i - 1;
				final Color color = ColorUtilities.offsetHSB(ColorUtilities.P_MAC_RED, (float) index / (headers.length - 1), 0.0F, -0.2F);

				fillTimeSeries(csvFile, headerLines, timeSeries, i, time, separator);

				SwingUtilities.invokeLater(() -> addTrackInfo(create(name, timeSeries, plots.get(0), 0, (index % 2) == 0, color)));

			}
		}

	}

	private void plot ( File file, int hLines, TimeValue time, String separator ) {

		try {
			SwingUtilities.invokeAndWait(() -> setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR)));
		} catch ( InterruptedException | InvocationTargetException ex ) {
		}

		try {
			loadTrackInfo(file, hLines, time, separator);
		} catch ( Exception ex ) {
			LogUtilities.log(LOGGER, Level.FINE, ex, "Interruped while creating plot track.");
		} finally {
			SwingUtilities.invokeLater(() -> setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR)));
		}

	}

	private enum TimeValue {

		HOURS, SECONDS

	}	//	enum TimeValue

	private class MultiPlotCrosshairOverlay extends CrosshairOverlay {

		private static final long serialVersionUID = 6363986638628637871L;

		@Override
		public Object clone () throws CloneNotSupportedException {
			return super.clone();
		}

		@Override
		public void paintOverlay ( Graphics2D g2, ChartPanel chartPanel ) {

			Shape savedClip = g2.getClip();
			Rectangle2D dataArea = chartPanel.getScreenDataArea();

			g2.clip(dataArea);

			RectangleEdge timeAxisEdge = masterPlot.getDomainAxisEdge();

			if ( timeCrosshair.isVisible() ) {

				double time = timeCrosshair.getValue();
				double x = timeAxis.valueToJava2D(time, dataArea, timeAxisEdge);

				drawVerticalCrosshair(g2, dataArea, x, timeCrosshair);

			}

			final PlotRenderingInfo prinfo = chartPanel.getChartRenderingInfo().getPlotInfo();

			trackInfos.stream().forEach(trackInfo -> {

				Crosshair crosshair = trackInfo.getCrosshair();
				XYPlot plot = trackInfo.getPlot();
				Rectangle2D area = prinfo.getSubplotInfo(trackInfo.getPlotIndex()).getDataArea();
				Point origin = chartPanel.translateJava2DToScreen(new Point2D.Double(area.getX(), area.getY()));
				Point dimension = chartPanel.translateJava2DToScreen(new Point2D.Double(area.getWidth(), area.getHeight()));
				Rectangle2D plotArea = new Rectangle2D.Double(origin.getX(), origin.getY(), dimension.getX(), dimension.getY());
				NumberAxis axis = trackInfo.getAxis();
				RectangleEdge axisEdge = plot.getRangeAxisEdge(trackInfo.getIndex());

				if ( crosshair.isVisible() ) {

					double value = crosshair.getValue();
					double y = axis.valueToJava2D(value, plotArea, axisEdge);

					drawHorizontalCrosshair(g2, plotArea, y, crosshair);

				}

			});

			g2.setClip(savedClip);

		}

	}   //  class MultiPlotCrosshairOverlay

}	//	class Plotter
