<#if licenseFirst??>
${licenseFirst}
</#if>
${licensePrefix}Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
${licensePrefix}Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
<#if licenseLast??>
${licenseLast}
</#if>