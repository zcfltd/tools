/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.GeneralPath;

/**
 * A class which implements a simple 1-pixel wide bevel border.
 *
 * @author Claudio Rosati
 */
public class ThinRoundBevelBorder extends ThinBevelBorder {

	private static final long serialVersionUID = -702984928138099445L;

    /**
     * Creates a bevel rounded border with the specified type, with a 1-pixel wide
     * line width, and whose colors will be derived from the background
     * color of the component passed into the paintBorder method
     *
     * @param bevelType The type of bevel for the border.
     */
    public ThinRoundBevelBorder ( int bevelType ) {
        super(bevelType);
    }

    /**
     * Creates a bevel rounded border with the specified type, highlight and
     * shadow colors, and with a 1-pixel wide line width.
     *
     * @param bevelType The type of bevel for the border.
     * @param highlight The color to use for the bevel highlight.
     * @param shadow    The color to use for the bevel shadow.
     */
    public ThinRoundBevelBorder ( int bevelType, Color highlight, Color shadow ) {
        super(bevelType, highlight, shadow);
    }

    @Override
    public Insets getBorderInsets ( final Component c ) {
        return new Insets(1, c.getHeight() / 3, 1, c.getHeight() / 3);
    }

    @Override
    @SuppressWarnings( "NestedAssignment" )
    public Insets getBorderInsets ( final Component c, final Insets insets ) {

        insets.top = insets.bottom = 1;
        insets.left = insets.right = c.getHeight() / 3;

        return insets;

    }

    @Override
    protected void paintBorder ( Graphics g, int x, int y, int w, int h, Color sColor, Color hColor ) {

        Graphics2D g2d = (Graphics2D) g;
        Shape clip = g2d.getClip();
        GeneralPath path = new GeneralPath();

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.translate(x, y);

        path.moveTo(-0.5, -0.5);
        path.lineTo(-0.5, h - 0.5);
        path.lineTo(w - 0.5, -0.5);
        path.closePath();

        g2d.clip(path);

        g2d.setColor(sColor);
        g2d.drawRoundRect(0, 0, w - 1, h - 1, h - 1, h - 1);

        path.reset();
        path.moveTo(w + 0.5, h + 0.5);
        path.lineTo(w + 0.5, -0.5);
        path.lineTo(-0.5, h + 0.5);
        path.closePath();

        g2d.setClip(clip);
        g2d.clip(path);

        g2d.setColor(hColor);
        g2d.drawRoundRect(0, 0, w - 1, h - 1, h - 1, h - 1);

    }

}
