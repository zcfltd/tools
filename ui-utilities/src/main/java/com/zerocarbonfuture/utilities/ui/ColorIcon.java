/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.ui;


import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import javax.swing.Icon;
import javax.swing.event.SwingPropertyChangeSupport;


/**
 * A square, colored box that satisfy the <CODE>Icon</CODE> interface.
 *
 * @author Claudio Rosati
 */
@SuppressWarnings( "serial" )
public class ColorIcon implements Icon, Serializable {

	/**
	 * The default icon height.
	 */
	protected static final int DEFAULT_HEIGHT = 12;
	
	/**
	 * The default icon width.
	 */
	protected static final int DEFAULT_WIDTH = 12;

	public static final String PROP_BORDER_COLOR = "borderColor";
	public static final String PROP_BORDER_PAINTED = "borderPainted";
	public static final String PROP_COLOR = "color";
	public static final String PROP_HEIGHT = "height";
	public static final String PROP_RECTANGULAR = "rectangular";
	public static final String PROP_WIDTH = "width";

	/**
	 * The icon border color.
	 */
	private Color borderColor;

	/**
	 * Tell if the icon border must be drawn.
	 */
	private boolean borderPainted;

	/**
	 * The icon color.
	 */
	private Color color;
	/**
	 * The icon height.
	 */
	private int height;
	/**
	 * Property change listener support.
	 */
	private SwingPropertyChangeSupport propertySupport;

	/**
	 * The icon shape flag.
	 */
	private boolean rectangular;

	/**
	 * The icon width.
	 */
	protected int width;

	/**
	 * Creates a new rectangular <CODE>ColorIcon</CODE> with default size and colors.
	 * The icon border will be painted.
	 */
	public ColorIcon() {
		this(Color.white, true);
	}

	/**
	 * Creates a new rectangular <CODE>ColorIcon</CODE> with default size, border color
	 * and given icon <CODE>color</CODE>.
	 * The icon border will be painted.
	 *
	 * @param color The icon color.
	 */
	public ColorIcon( Color color ) {
		this(color, Color.black, true, true);
	}

	/**
	 * Creates a new rectangular <CODE>ColorIcon</CODE> with default size,
	 * and given icon <CODE>color</CODE> and <CODE>borderColor</CODE>.
	 * The icon border will be painted if <CODE>borderPainted == true</CODE>.
	 *
	 * @param color         The icon color.
	 * @param borderColor   The icon border color.
	 * @param borderPainted <CODE>true</CODE> if the icon border must be painted.
	 *                      <CODE>false</CODE> otherwise.
	 */
	public ColorIcon( Color color, Color borderColor, boolean borderPainted ) {
		this(color, ColorIcon.DEFAULT_WIDTH, ColorIcon.DEFAULT_HEIGHT, borderColor, borderPainted, true);
	}

	/**
	 * Creates a new rectangular <CODE>ColorIcon</CODE> with border color
	 * and given icon <CODE>color</CODE> and size.
	 * The icon border will be painted.
	 *
	 * @param color  The icon color.
	 * @param width  The icon width.
	 * @param height The icon height.
	 */
	public ColorIcon( Color color, int width, int height ) {
		this(color, width, height, Color.black, true, true);
	}

	/**
	 * Creates a new rectangular <CODE>ColorIcon</CODE> with given size, icon <CODE>color</CODE>
	 * and <CODE>borderColor</CODE>.
	 * The icon border will be painted if <CODE>borderPainted == true</CODE>.
	 *
	 * @param color         The icon color.
	 * @param width         The icon width.
	 * @param height        The icon height.
	 * @param borderColor   The icon border color.
	 * @param borderPainted <CODE>true</CODE> if the icon border must be painted.
	 *                      <CODE>false</CODE> otherwise.
	 */
	public ColorIcon( Color color, int width, int height, Color borderColor, boolean borderPainted ) {
		this(color, width, height, borderColor, borderPainted, true);
	}

	/**
	 * Creates a new <CODE>ColorIcon</CODE> with default size and colors.
	 * The icon border will be painted.
	 *
	 * @param rectangular Tell if the icon shape must be rectangular ({@code true})
	 *                    of oval ({@code false}).
	 */
	public ColorIcon( boolean rectangular ) {
		this(Color.white, rectangular);
	}

	/**
	 * Creates a new <CODE>ColorIcon</CODE> with default size, border color
	 * and given icon <CODE>color</CODE>.
	 * The icon border will be painted.
	 *
	 * @param color       The icon color.
	 * @param rectangular Tell if the icon shape must be rectangular ({@code true})
	 *                    of oval ({@code false}).
	 */
	public ColorIcon( Color color, boolean rectangular ) {
		this(color, Color.black, true, rectangular);
	}

	/**
	 * Creates a new <CODE>ColorIcon</CODE> with default size,
	 * and given icon <CODE>color</CODE> and <CODE>borderColor</CODE>.
	 * The icon border will be painted if <CODE>borderPainted == true</CODE>.
	 *
	 * @param color         The icon color.
	 * @param borderColor   The icon border color.
	 * @param borderPainted <CODE>true</CODE> if the icon border must be painted.
	 *                      <CODE>false</CODE> otherwise.
	 * @param rectangular   Tell if the icon shape must be rectangular ({@code true})
	 *                      of oval ({@code false}).
	 */
	public ColorIcon( Color color, Color borderColor, boolean borderPainted, boolean rectangular ) {
		this(color, ColorIcon.DEFAULT_WIDTH, ColorIcon.DEFAULT_HEIGHT, borderColor, borderPainted, rectangular);
	}

	/**
	 * Creates a new <CODE>ColorIcon</CODE> with border color
	 * and given icon <CODE>color</CODE> and size.
	 * The icon border will be painted.
	 *
	 * @param color       The icon color.
	 * @param width       The icon width.
	 * @param height      The icon height.
	 * @param rectangular Tell if the icon shape must be rectangular ({@code true})
	 *                    of oval ({@code false}).
	 */
	public ColorIcon( Color color, int width, int height, boolean rectangular ) {
		this(color, width, height, Color.black, true, rectangular);
	}

	/**
	 * Creates a new <CODE>ColorIcon</CODE> with given size, icon <CODE>color</CODE>
	 * and <CODE>borderColor</CODE>.
	 * The icon border will be painted if <CODE>borderPainted == true</CODE>.
	 *
	 * @param color         The icon color.
	 * @param width         The icon width.
	 * @param height        The icon height.
	 * @param borderColor   The icon border color.
	 * @param borderPainted <CODE>true</CODE> if the icon border must be painted.
	 *                      <CODE>false</CODE> otherwise.
	 * @param rectangular   Tell if the icon shape must be rectangular ({@code true})
	 *                      of oval ({@code false}).
	 */
	public ColorIcon( Color color, int width, int height, Color borderColor, boolean borderPainted, boolean rectangular ) {
		this.color = color;
		this.width = width;
		this.height = height;
		this.borderColor = borderColor;
		this.borderPainted = borderPainted;
		this.rectangular = rectangular;
		this.propertySupport = new SwingPropertyChangeSupport(this);
	}

	/**
	 * Adds a <CODE>PropertyChangeListener</CODE> to the listener list.
	 * <P>
	 * If listener is <CODE>null</CODE>, no exception is thrown and no action is
	 * performed.
	 *
	 * @param listener The <CODE>PropertyChangeListener</CODE> to be added.
	 */
	public void addPropertyChangeListener( PropertyChangeListener listener ) {
		propertySupport.addPropertyChangeListener(listener);
	}

	/**
	 * Adds a <CODE>PropertyChangeListener</CODE> to the listener list for a
	 * specific property.
	 * <P>
	 * If listener is <CODE>null</CODE>, no exception is thrown and no action is
	 * performed.
	 *
	 * @param propertyName The name of the property to listen to.
	 * @param listener     The <CODE>PropertyChangeListener</CODE> to be added.
	 */
	public void addPropertyChangeListener( String propertyName, PropertyChangeListener listener ) {
		propertySupport.addPropertyChangeListener(propertyName, listener);
	}

	/**
	 * Support for reporting bound property changes for <CODE>Object</CODE>
	 * properties. This method can be called when a bound property has changed
	 * and it will send the appropriate <CODE>PropertyChangeEvent</CODE> to any
	 * registered <CODE>PropertyChangeListener</CODE>s.
	 *
	 * @param propertyName The property whose value has changed.
	 * @param oldValue     The property's previous value.
	 * @param newValue     The property's new value.
	 */
	protected void firePropertyChange( String propertyName, Object oldValue, Object newValue ) {
		propertySupport.firePropertyChange(propertyName, oldValue, newValue);
	}

	/**
	 * Support for reporting bound property changes for <CODE>boolean</CODE>
	 * properties. This method can be called when a bound property has changed
	 * and it will send the appropriate <CODE>PropertyChangeEvent</CODE> to any
	 * registered <CODE>PropertyChangeListener</CODE>s.
	 *
	 * @param propertyName The property whose value has changed.
	 * @param oldValue     The property's previous value.
	 * @param newValue     The property's new value.
	 */
	protected void firePropertyChange( String propertyName, boolean oldValue, boolean newValue ) {
		propertySupport.firePropertyChange(propertyName, oldValue, newValue);
	}

	/**
	 * Support for reporting bound property changes for <CODE>int</CODE>
	 * properties. This method can be called when a bound property has changed
	 * and it will send the appropriate <CODE>PropertyChangeEvent</CODE> to any
	 * registered <CODE>PropertyChangeListener</CODE>s.
	 *
	 * @param propertyName The property whose value has changed.
	 * @param oldValue     The property's previous value.
	 * @param newValue     The property's new value.
	 */
	protected void firePropertyChange( String propertyName, int oldValue, int newValue ) {
		propertySupport.firePropertyChange(propertyName, oldValue, newValue);
	}

	/**
	 * Returns the icon's border color.
	 *
	 * @return A <CODE>Color</CODE> object specifying the color of the icon border.
	 */
	public Color getBorderColor() {
		return borderColor;
	}

	/**
	 * Returns the icon's color.
	 *
	 * @return A <CODE>Color</CODE> object specifying the color of the icon.
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Returns the icon's height.
	 *
	 * @return An <CODE>int</CODE> specifying the fixed height of the icon.
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Returns the icon's height.
	 *
	 * @return An <CODE>int</CODE> specifying the fixed height of the icon.
	 */
	@Override
	public int getIconHeight() {
		return getHeight();
	}

	/**
	 * Returns the icon's width.
	 *
	 * @return An <CODE>int</CODE> specifying the fixed width of the icon.
	 */
	@Override
	public int getIconWidth() {
		return getWidth();
	}

	/**
	 * Returns an array of all the property change listeners registered on this
	 * component.
	 *
	 * @return All of this component's <CODE>PropertyChangeListener</CODE>s or
	 *         an empty array if no property change listeners are currently
	 *         registered.
	 */
	public PropertyChangeListener[] getPropertyChangeListeners() {
		return propertySupport.getPropertyChangeListeners();
	}

	/**
	 * Returns an array of all the listeners which have been associated with the
	 * named property.
	 *
	 * @param propertyName The name of the property whose listeners must be returned.
	 * @return All of the <CODE>PropertyChangeListener</CODE>s associated with
	 *         the named property or an empty array if no listeners have been
	 *         added.
	 */
	public PropertyChangeListener[] getPropertyChangeListeners( String propertyName ) {
		return propertySupport.getPropertyChangeListeners(propertyName);
	}

	/**
	 * Returns the icon's width.
	 *
	 * @return An <CODE>int</CODE> specifying the fixed width of the icon.
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Return <CODE>true</CODE> if the icon's border will be painted.
	 *
	 * @return An <CODE>boolean</CODE> specifying the if the icon's border will
	 *         be painted.
	 */
	public boolean isBorderPainted() {
		return borderPainted;
	}

	/**
	 * Return <CODE>true</CODE> if the icon's shape is rectangular.
	 *
	 * @return An <CODE>boolean</CODE> specifying the if the icon's shape is
	 *         rectangular or oval.
	 */
	public boolean isRectangular() {
		return rectangular;
	}

	/**
	 * Draw the icon at the specified location. The top-left corner of the icon
	 * is drawn at the point (<CODE>x</CODE>, <CODE>y</CODE>) in the coordinate
	 * space of the <CODE>graphics</CODE> context.
	 *
	 * @param component The component to be used as the observer if necessary.
	 * @param graphics  The graphics context.
	 * @param x         The X coordinate of the icon's top-left corner.
	 * @param y         The Y coordinate of the icon's top-left corner.
	 */
	@Override
	public void paintIcon( Component component, Graphics graphics, int x, int y ) {

		int w = getWidth();
		int h = getHeight();
		Graphics2D g = (Graphics2D) graphics.create();

		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		if ( isRectangular() ) {

			g.setColor(getColor());
			g.fillRect(x, y, w, h);

			if ( isBorderPainted() ) {
				g.setColor(getBorderColor());
				g.drawRect(x, y, w - 1, h - 1);
			}

		} else {

			g.setColor(getColor());
			g.fillOval(x, y, w, h);

			if ( isBorderPainted() ) {
				g.setColor(getBorderColor());
				g.drawOval(x, y, w, h);
			}

		}

	}

	/**
	 * Removes a <CODE>PropertyChangeListener</CODE> from the listener list.
	 * This method should be used to remove <CODE>PropertyChangeListeners</CODE>
	 * that were registered for all bound properties of this class.
	 *
	 * @param listener The <CODE>PropertyChangeListener</CODE> to be removed.
	 */
	public void removePropertyChangeListener( PropertyChangeListener listener ) {
		propertySupport.removePropertyChangeListener(listener);
	}

	/**
	 * Removes a <CODE>PropertyChangeListener</CODE> from the listener list for
	 * a specific property. This method should be used to remove
	 * <CODE>PropertyChangeListeners</CODE> that were registered for a specific
	 * bound property.
	 *
	 * @param propertyName The property name.
	 * @param listener     The <CODE>PropertyChangeListener</CODE> to be removed.
	 */
	public void removePropertyChangeListener( String propertyName, PropertyChangeListener listener ) {
		propertySupport.removePropertyChangeListener(propertyName, listener);
	}

	/**
	 * Sets the icon's border color.
	 *
	 * @param borderColor The new icon's border color.
	 */
	public void setBorderColor( Color borderColor ) {

		Color oldBorderColor = this.borderColor;

		this.borderColor = borderColor;

		firePropertyChange(ColorIcon.PROP_BORDER_COLOR, oldBorderColor, borderColor);

	}

	/**
	 * Sets the icon's border visibility status.
	 *
	 * @param borderPainted <CODE>true</CODE> if the icon's border must be painted.
	 */
	public void setBorderPainted( boolean borderPainted ) {

		boolean oldBorderPainted = this.borderPainted;

		this.borderPainted = borderPainted;

		firePropertyChange(ColorIcon.PROP_BORDER_PAINTED, oldBorderPainted, borderPainted);

	}

	/**
	 * Sets the icon's color.
	 *
	 * @param color The new icon's color.
	 */
	public void setColor( Color color ) {

		Color oldColor = this.color;

		this.color = color;

		firePropertyChange(ColorIcon.PROP_COLOR, oldColor, color);

	}

	/**
	 * Sets the new icon's height.
	 *
	 * @param height The new icon's height.
	 */
	public void setHeight( int height ) {

		int oldHeight = this.height;

		this.height = height;

		firePropertyChange(ColorIcon.PROP_HEIGHT, oldHeight, height);

	}

	/**
	 * Sets the icon's shape flag.
	 *
	 * @param rectangular <CODE>true</CODE> if the icon's shape must be rectangular.
	 */
	public void setRectangular( boolean rectangular ) {

		boolean oldRectangular = this.borderPainted;

		this.rectangular = rectangular;

		firePropertyChange(ColorIcon.PROP_RECTANGULAR, oldRectangular, rectangular);

	}

	/**
	 * Sets the new icon's width.
	 *
	 * @param width The new icon's width.
	 */
	public void setWidth( int width ) {

		int oldWidth = this.width;

		this.width = width;

		firePropertyChange(ColorIcon.PROP_WIDTH, oldWidth, width);

	}

}
