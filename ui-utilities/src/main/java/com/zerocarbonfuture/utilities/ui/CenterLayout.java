/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.ui;


import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.io.Serializable;


/**
 * This layout is very basic: it simply centers one component in its parent.
 * This can be quite handy sometimes such as when you want to draw a picture in
 * the center of something, but don't want to keep working out the offsets yourself.
 *
 * @author Claudio Rosati
 */
public class CenterLayout
	implements LayoutManager, Serializable {

	private static final long serialVersionUID = 2962313659421069773L;

	/**
	 * Adds the specified component to the layout. Not used by this class.
	 *
	 * @param name The name of the component.
	 * @param comp The component to be added.
	 */
	@Override
	public void addLayoutComponent( String name, Component comp ) {
	}

	/**
	 * Lays out the container. This method lets each component take
	 * its preferred size by reshaping the components in the
	 * target container in order to satisfy the constraints of
	 * this layout object.
	 *
	 * @param target The specified component being laid out.
	 * @see java.awt.Container
	 * @see java.awt.Container#doLayout
	 */
	@Override
	public void layoutContainer( Container target ) {

		synchronized ( target.getTreeLock() ) {

			Insets insets = target.getInsets();
			Dimension size = target.getSize();
			int w = size.width - (insets.left + insets.right);
			int h = size.height - (insets.top + insets.bottom);
			int count = target.getComponentCount();

			for ( int i = 0; i < count; i++ ) {

				Component m = target.getComponent(i);

				if ( m.isVisible() ) {

					Dimension d = m.getPreferredSize();

					m.setBounds((w - d.width) / 2, (h - d.height) / 2, d.width, d.height);

				}

			}

		}

	}

	/**
	 * Returns the minimum dimensions needed to layout the components
	 * contained in the specified target container.
	 *
	 * @param target The component which needs to be laid out.
	 * @return The minimum dimensions to lay out the subcomponents of the
	 *         specified container.
	 * @see #preferredLayoutSize
	 * @see java.awt.Container
	 * @see java.awt.Container#doLayout
	 */
	@Override
	public Dimension minimumLayoutSize( Container target ) {

		synchronized ( target.getTreeLock() ) {

			int w = 0;
			int h = 0;
			Insets insets = target.getInsets();
			int count = target.getComponentCount();

			if ( count > 0 ) {

				w = Integer.MAX_VALUE;
				h = Integer.MAX_VALUE;

				for ( int i = 0; i < count; i++ ) {

					Component m = target.getComponent(i);

					if ( m.isVisible() ) {

						Dimension d = m.getMinimumSize();

						if ( d.width < w ) {
							w = d.width;
						}
						if ( d.height < h ) {
							h = d.height;
						}

					}

				}

			}

			w += insets.left + insets.right;
			h += insets.top + insets.bottom;

			return new Dimension(w, h);

		}

	}

	/**
	 * Returns the preferred dimensions for this layout given the components
	 * in the specified target container.
	 *
	 * @param target The component which needs to be laid out.
	 * @return The preferred dimensions to lay out the subcomponents of the
	 *         specified container.
	 * @see java.awt.Container
	 * @see #minimumLayoutSize
	 * @see java.awt.Container#getPreferredSize
	 */
	@Override
	public Dimension preferredLayoutSize( Container target ) {

		synchronized ( target.getTreeLock() ) {

			int w = 0;
			int h = 0;
			Insets insets = target.getInsets();
			int count = target.getComponentCount();

			for ( int i = 0; i < count; i++ ) {

				Component m = target.getComponent(i);

				if ( m.isVisible() ) {

					Dimension d = m.getPreferredSize();

					if ( d.width > w ) {
						w = d.width;
					}
					if ( d.height > h ) {
						h = d.height;
					}

				}

			}

			w += insets.left + insets.right;
			h += insets.top + insets.bottom;

			return new Dimension(w, h);

		}

	}

	/**
	 * Removes the specified component from the layout. Not used by this class.
	 *
	 * @param comp The component to remove.
	 * @see java.awt.Container#removeAll
	 */
	@Override
	public void removeLayoutComponent( Component comp ) {
	}

}
