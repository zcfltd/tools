/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.ui;


import com.zerocarbonfuture.utilities.ColorUtilities;
import java.awt.Color;
import java.beans.PropertyChangeListener;
import javax.swing.JLabel;


/**
 * A <CODE>JLabel</CODE> that show a color icon and the color value in
 * RGB format.
 *
 * @author Claudio Rosati
 */
@SuppressWarnings( "serial" )
public class ColorLabel extends JLabel {


	/**
	 * The support icon.
	 */
	protected ColorIcon colorIcon = null;
	
	/**
	 * The square/circle discriminator.
	 */
	protected boolean square = true;
	
	/**
	 * Repaint icon.
	 */
	private final PropertyChangeListener borderListener = evt -> {
		repaint();
	};

	/**
	 * Update label text and repaint icon.
	 */
	private final PropertyChangeListener colorListener = evt -> {
		setText(ColorUtilities.getRGBString((Color) evt.getNewValue()));
	};

	/**
	 * Update layout.
	 */
	private final PropertyChangeListener sizeListener = evt -> {
		setIcon(null);
		setIcon(colorIcon);
	};


	/**
	 * Creates a new white <CODE>ColorLabel</CODE>.
	 */
	public ColorLabel() {
		this(Color.white);
	}

	/**
	 * Creates a new white <CODE>ColorLabel</CODE>.
	 *
	 * @param square <CODE>true</CODE> if the color icon must be a square,
	 *               <CODE>false</CODE> for a circle.
	 */
	public ColorLabel( boolean square ) {
		this(Color.white, square);
	}

	/**
	 * Creates a new <CODE>ColorLabel</CODE> with a given <CODE>color</CODE>.
	 *
	 * @param color The label icon's color.
	 */
	public ColorLabel( Color color ) {
		this(color, ColorUtilities.getRGBString(color));
	}

	/**
	 * Creates a new <CODE>ColorLabel</CODE> with a given <CODE>color</CODE>.
	 *
	 * @param color  The label icon's color.
	 * @param square <CODE>true</CODE> if the color icon must be a square,
	 *               <CODE>false</CODE> for a circle.
	 */
	public ColorLabel( Color color, boolean square ) {
		this(color, ColorUtilities.getRGBString(color), square);
	}

	/**
	 * Creates a new <CODE>ColorLabel</CODE> with a given <CODE>color</CODE>
	 * and <CODE>caption</CODE>.
	 *
	 * @param color   The label icon's color.
	 * @param caption The label caption. This should be used when <CODE>color</CODE>
	 *                is a predefined color, describable as "white", "red", ...
	 */
	public ColorLabel( Color color, String caption ) {
		this(color, caption, true);
	}

	/**
	 * Creates a new <CODE>ColorLabel</CODE> with a given <CODE>color</CODE>
	 * and <CODE>caption</CODE>.
	 *
	 * @param color   The label icon's color.
	 * @param caption The label caption. This should be used when <CODE>color</CODE>
	 *                is a predefined color, describable as "white", "red", ...
	 * @param square  <CODE>true</CODE> if the color icon must be a square,
	 *                <CODE>false</CODE> for a circle.
	 */
	@SuppressWarnings( "OverridableMethodCallInConstructor" )
	public ColorLabel( Color color, String caption, boolean square ) {

		super();

		setColor(color, caption, square);

	}

	/**
	 * Returns the color associated with this label.
	 *
	 * @return A <CODE>Color</CODE> object or <CODE>null</CODE>.
	 */
	public Color getColor() {
		if ( colorIcon != null ) {
			return colorIcon.getColor();
		} else {
			return null;
		}
	}

	public boolean isSquare() {
		return square;
	}

	/**
	 * Sets the color associated with this label.
	 * If the label was created without color, a new color icon will
	 * be created and displayed.
	 *
	 * @param color The new color associated with this label. The label text
	 *              will be changed accordingly.
	 *              If <CODE>color == null</CODE> nothing occurs.
	 */
	public void setColor( Color color ) {

		Color old = getColor();

		if ( color != null ) {

			if ( colorIcon == null ) {

				colorIcon = new ColorIcon(color, isSquare());

				colorIcon.addPropertyChangeListener(ColorIcon.PROP_BORDER_COLOR, borderListener);
				colorIcon.addPropertyChangeListener(ColorIcon.PROP_BORDER_PAINTED, borderListener);
				colorIcon.addPropertyChangeListener(ColorIcon.PROP_RECTANGULAR, borderListener);
				colorIcon.addPropertyChangeListener(ColorIcon.PROP_COLOR, colorListener);
				colorIcon.addPropertyChangeListener(ColorIcon.PROP_WIDTH, sizeListener);
				colorIcon.addPropertyChangeListener(ColorIcon.PROP_HEIGHT, sizeListener);

				setIcon(colorIcon);

			} else {
				colorIcon.setColor(color);
			}

		}

		firePropertyChange("color", old, color);

	}

	/**
	 * Sets the color and the text associated with this label.
	 * If the label was created without color, a new color icon will
	 * be created and displayed.
	 *
	 * @param color   The new color associated with this label. The label text
	 *                will be changed accordingly.
	 *                If <CODE>color == null</CODE> nothing occurs.
	 * @param caption The label caption. This should be used when <CODE>color</CODE>
	 *                is a predefined color, describable as "white", "red", ...
	 */
	public void setColor( Color color, String caption ) {
		setColor(color);
		setText(caption);
	}

	/**
	 * Sets the color and the text associated with this label.
	 * If the label was created without color, a new color icon will
	 * be created and displayed.
	 *
	 * @param color   The new color associated with this label. The label text
	 *                will be changed accordingly.
	 *                If <CODE>color == null</CODE> nothing occurs.
	 * @param caption The label caption. This should be used when <CODE>color</CODE>
	 *                is a predefined color, describable as "white", "red", ...
	 * @param square  <CODE>true</CODE> if the color icon must be a square,
	 *                <CODE>false</CODE> for a circle.
	 */
	public void setColor( Color color, String caption, boolean square ) {
		setSquare(square);
		setColor(color);
		setText(caption);
	}

	/**
	 * Sets the color icon shape.
	 *
	 * @param square <CODE>true</CODE> if the color icon must be a square,
	 *               <CODE>false</CODE> for a circle.
	 */
	public void setSquare( boolean square ) {

		boolean oldValue = isSquare();

		this.square = square;

		firePropertyChange("square", oldValue, square);

		if ( colorIcon != null ) {
			colorIcon.setRectangular(square);
			setIcon(null);
			setIcon(colorIcon);
		}

	}

}
