/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.ui;

import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.ListCellRenderer;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;

/**
 * This class implements common behavior for cell renderer interfaces.
 *
 * @param <E> The class of the element rendered by this renderer.
 * @author Claudio Rosati
 */
public abstract class BasicCellRenderer<E>
    implements ListCellRenderer<E>, TableCellRenderer, TreeCellRenderer {

    /**
     * Default rendered for list cells: an instance of {@link DefaultListCellRenderer}.
     */
    protected DefaultListCellRenderer defaultListRenderer = new DefaultListCellRenderer();
    
    /**
     * Default renderer for tree cells: an instance of {@link DefaultTableCellRenderer}.
     */
    protected DefaultTableCellRenderer defaultTableRenderer = new DefaultTableCellRenderer();

    /**
     * Default renderer for tree nodes: an instance of {@link DefaultTreeCellRenderer}.
     */
    protected DefaultTreeCellRenderer defaultTreeRenderer = new DefaultTreeCellRenderer();

    /**
     * Enabled status.
     */
    private boolean enabled = true;

    /**
     * The chain renderer, used when the list cell isn't an object with a specific renderer.
     */
    protected ListCellRenderer<E> listChainRenderer = null;

    /**
     * The chain renderer, used when the table value isn't an object with a specific renderer.
     */
    protected TableCellRenderer tableChainRenderer = null;

    /**
     * The chain renderer, used when the tree node isn't an object with a specific renderer.
     */
    protected TreeCellRenderer treeChainRenderer = null;

    /**
     * Creates a new {@code BasicCellRenderer}.
     */
    public BasicCellRenderer () {
        this(null, null, null);
    }

    /**
     * Creates a new {@code BasicCellRenderer}.
     * <P>
     * It is important to remember that if the cell value is not an object with
     * a specific renderer, then the given chain renderers
     * will be used to render the cell. This allow a chain of renderers to
     * be followed until a valid renderer is found for any given cell.
     *
     * @param renderer The chained {@code BasicCellRenderer}.
     */
    public BasicCellRenderer ( BasicCellRenderer<E> renderer ) {
        this.listChainRenderer = renderer;
        this.treeChainRenderer = renderer;
        this.tableChainRenderer = renderer;
    }

    /**
     * Creates a new {@code BasicCellRenderer}.
     * <P>
     * It is important to remember that if the cell value is not an object with
     * a specific renderer, then the given chain renderers
     * will be used to render the cell. This allow a chain of renderers to
     * be followed until a valid renderer is found for any given cell.
     *
     * @param listChainRenderer  The list cell renderer that provides default attributes.
     *                           If {@code listChainRenderer} is {@code null} a
     *                           private {@code DefaultListCellRenderer} object
     *                           will be created.
     * @param treeChainRenderer  The tree cell renderer that provides default attributes.
     *                           If {@code treeChainRenderer} is {@code null} a
     *                           private {@code DefaultTreeCellRenderer} object
     *                           will be created.
     * @param tableChainRenderer The table cell renderer that provides default attributes.
     *                           If {@code tableChainRenderer} is {@code null} a
     *                           private {@code DefaultTableCellRenderer} object
     *                           will be created.
     */
    public BasicCellRenderer (
        ListCellRenderer<E> listChainRenderer,
        TreeCellRenderer treeChainRenderer,
        TableCellRenderer tableChainRenderer
    ) {
        this.listChainRenderer = listChainRenderer;
        this.treeChainRenderer = treeChainRenderer;
        this.tableChainRenderer = tableChainRenderer;
    }

    /**
     * Copies relevant properties from the chain renderer to the
     * default one.
     */
    protected void copyListRendererProperties () {

        DefaultListCellRenderer dtcr = defaultListRenderer;

        if ( ( listChainRenderer != null ) && ( listChainRenderer instanceof DefaultListCellRenderer ) ) {

            DefaultListCellRenderer dr = (DefaultListCellRenderer) listChainRenderer;

            dtcr.setBackground(dr.getBackground());
            dtcr.setForeground(dr.getForeground());
            dtcr.setFont(dr.getFont());
            dtcr.setBorder(dr.getBorder());

        }

    }

    /**
     * Copies relevant properties from the chain renderer to the
     * default one.
     */
    protected void copyTableRendererProperties () {

        DefaultTableCellRenderer dtcr = defaultTableRenderer;

        if ( ( tableChainRenderer != null ) && ( tableChainRenderer instanceof DefaultTableCellRenderer ) ) {

            DefaultTableCellRenderer dr = (DefaultTableCellRenderer) tableChainRenderer;

            dtcr.setBackground(dr.getBackground());
            dtcr.setForeground(dr.getForeground());
            dtcr.setFont(dr.getFont());
            dtcr.setBorder(dr.getBorder());

        }

    }

    /**
     * Copies relevant properties from the chain renderer to the
     * default one.
     */
    protected void copyTreeRendererProperties () {

        DefaultTreeCellRenderer dtcr = defaultTreeRenderer;

        if ( ( treeChainRenderer != null ) && ( treeChainRenderer instanceof DefaultTreeCellRenderer ) ) {

            DefaultTreeCellRenderer dr = (DefaultTreeCellRenderer) treeChainRenderer;

            dtcr.setBackground(dr.getBackground());
            dtcr.setForeground(dr.getForeground());
            dtcr.setBackgroundNonSelectionColor(dr.getBackgroundNonSelectionColor());
            dtcr.setBackgroundSelectionColor(dr.getBackgroundSelectionColor());
            dtcr.setBorderSelectionColor(dr.getBorderSelectionColor());
            dtcr.setClosedIcon(dr.getClosedIcon());
            dtcr.setFont(dr.getFont());
            dtcr.setLeafIcon(dr.getLeafIcon());
            dtcr.setOpenIcon(dr.getOpenIcon());
            dtcr.setTextNonSelectionColor(dr.getTextNonSelectionColor());
            dtcr.setTextSelectionColor(dr.getTextSelectionColor());
            dtcr.setBorder(dr.getBorder());

        }

    }

    /**
     * Returns the default rendered for list cells:
     * an instance of {@link DefaultListCellRenderer}.
     *
     * @return The default rendered for list cells.
     */
    public DefaultListCellRenderer getDefaultListRenderer () {
        return defaultListRenderer;
    }

    /**
     * Returns the default renderer for tree cells:
     * an instance of {@link DefaultTableCellRenderer}.
     *
     * @return The default renderer for tree cells.
     */
    public DefaultTableCellRenderer getDefaultTableRenderer () {
        return defaultTableRenderer;
    }

    /**
     * Returns the default renderer for tree nodes:
     * an instance of {@link DefaultTreeCellRenderer}.
     *
     * @return The default renderer for tree nodes.
     */
    public DefaultTreeCellRenderer getDefaultTreeRenderer () {
        return defaultTreeRenderer;
    }

    /**
     * Return a component that has been configured to display the specified
     * {@code value}. That component's {@code paint} method is then
     * called to "render" the cell. If it is necessary to compute the dimensions
     * of a list because the list cells do not have a fixed size, this method is
     * called to generate a component on which {@code getPreferredSize} can
     * be invoked.
     *
     * @param list         The {@code JList} we're painting.
     * @param value        The value returned by {@code list.getModel().getElementAt(index)}.
     * @param index        The cells index.
     * @param isSelected   {@code true} if the specified cell was selected.
     * @param cellHasFocus {@code true} if the specified cell has the focus.
     * @return A component whose {@code paint} method will render the specified value.
     */
    @Override
    public Component getListCellRendererComponent ( JList<? extends E> list, E value, int index, boolean isSelected, boolean cellHasFocus ) {

        JLabel renderer = (JLabel) defaultListRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

        renderer.setIcon(null);
        renderer.setText(null);
        renderer.setToolTipText(null);

        if ( isValueAccepted(value) ) {

            copyListRendererProperties();
            updateRendererComponent(renderer, value);

            if ( renderer.getText() == null || renderer.getText().length() == 0 ) {
                renderer.setToolTipText(null);
            } else {
                renderer.setToolTipText(renderer.getText());
            }

            renderer.setEnabled(isEnabled());

            return renderer;

        } else {

            Component cellRenderer;

            if ( listChainRenderer != null ) {
                cellRenderer = listChainRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            } else {
                cellRenderer = defaultListRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            }

            cellRenderer.setEnabled(isEnabled());

            return cellRenderer;

        }

    }

    /**
     * Returns the chain renderer, used when the list cell
     * isn't an object with a specific renderer.
     *
     * @return The chain renderer.
     */
    public ListCellRenderer<E> getListChainRenderer () {
        return listChainRenderer;
    }

    /**
     * Returns the component used for drawing the cell. This method is used to
     * configure the renderer appropriately before drawing.
     *
     * @param table	     The {@code JTable} that is asking the renderer to draw;
     *                   can be {@code null}.
     * @param value	     The value of the cell to be rendered. It is up to the specific
     *                   renderer to interpret and draw the value. For example, if value
     *                   is the string "true", it could be rendered as a string or it
     *                   could be rendered as a check box that is checked. null is a
     *                   valid value.
     * @param isSelected {@code true} if the cell is to be rendered with the
     *                   selection highlighted; otherwise {@code false}.
     * @param hasFocus   If {@code true}, render cell appropriately. For example,
     *                   put a special border on the cell, if the cell can be edited,
     *                   render in the color used to indicate editing.
     * @param row        The row index of the cell being drawn. When drawing the header,
     *                   the value of row is -1.
     * @param column     The column index of the cell being drawn.
     * @return The component used for drawing the cell.
     */
    @Override
    public Component getTableCellRendererComponent (
        JTable table,
        Object value,
        boolean isSelected,
        boolean hasFocus,
        int row,
        int column
    ) {

        JLabel renderer = (JLabel) defaultTableRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        renderer.setIcon(null);
        renderer.setText(null);
        renderer.setToolTipText(null);

        if ( isValueAccepted(value) ) {

            copyTableRendererProperties();
            updateRendererComponent(renderer, value);

            if ( renderer.getText() == null || renderer.getText().length() == 0 ) {
                renderer.setToolTipText(null);
            } else {
                renderer.setToolTipText(renderer.getText());
            }

            renderer.setEnabled(isEnabled());

            return renderer;

        } else {

            Component cellRenderer;

            if ( tableChainRenderer != null ) {
                cellRenderer = tableChainRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            } else {
                cellRenderer = defaultTableRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }

            cellRenderer.setEnabled(isEnabled());

            return cellRenderer;

        }

    }

    /**
     * Returns the chain renderer, used when the table value
     * isn't an object with a specific renderer.
     *
     * @return The chain renderer.
     */
    public TableCellRenderer getTableChainRenderer () {
        return tableChainRenderer;
    }

    /**
     * Sets the value of the current tree cell to {@code value}. If {@code selected}
     * is {@code true}, the cell will be drawn as if selected. If {@code expanded}
     * is {@code true} the node is currently expanded and if {@code leaf} is
     * {@code true} the node represets a leaf anf if {@code hasFocus} is
     * {@code true} the node currently has focus. {@code tree} is the
     * {@code JTree} the receiver is being configured for. Returns the
     * {@code Component} that the renderer uses to draw the value.
     *
     * @param tree     The {@link JTree} to be rendered.
     * @param value    The value to be rendered as a {@code tree}'s node.
     * @param selected {@code true} if the node is selected.
     * @param expanded {@code true} if the node is expanded.
     * @param leaf     {@code true} if the node is a leaf.
     * @param row      The row index of the tree's node.
     * @param hasFocus {@code true} if the node has focus.
     * @return {@code Component} that the renderer uses to draw the value.
     */
    @Override
    public Component getTreeCellRendererComponent (
        JTree tree,
        Object value,
        boolean selected,
        boolean expanded,
        boolean leaf,
        int row,
        boolean hasFocus
    ) {

        Object objValue = ( value instanceof DefaultMutableTreeNode ) ? ( (DefaultMutableTreeNode) value ).getUserObject() : value;
        JLabel renderer = (JLabel) defaultTreeRenderer.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);

        renderer.setIcon(null);
        renderer.setText(null);
        renderer.setToolTipText(null);

        if ( isValueAccepted(objValue) ) {

            copyTreeRendererProperties();
            updateRendererComponent(renderer, objValue);

            if ( renderer.getText() == null || renderer.getText().length() == 0 ) {
                renderer.setToolTipText(null);
            } else {
                renderer.setToolTipText(renderer.getText());
            }

            renderer.setEnabled(isEnabled());

            return renderer;

        } else {

            Component cellRenderer;

            if ( treeChainRenderer != null ) {
                cellRenderer = treeChainRenderer.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
            } else {
                cellRenderer = defaultTreeRenderer.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
            }

            cellRenderer.setEnabled(isEnabled());

            return cellRenderer;

        }

    }

    /**
     * Returns the chain renderer, used when the tree
     * node isn't an object with a specific renderer.
     *
     * @return The chain renderer.
     */
    public TreeCellRenderer getTreeChainRenderer () {
        return treeChainRenderer;
    }

    /**
     * Returns the renderer enabled status.
     *
     * @return The renderer enabled status.
     */
    public boolean isEnabled () {
        return enabled;
    }

    /**
     * Derived classes should return {@code true} when given {@code value}
     * is an instance of the managed class type, {@code false} otherwise, or if
     * given {@code value} is {@code null}.
     *
     * @param value The value to be checked.
     * @return {@code true} when given {@code value}
     *         is an instance of the managed class type.
     */
    protected abstract boolean isValueAccepted ( Object value );

    /**
     * Sets the renderer enabled status.
     *
     * @param enabled The renderer enabled status.
     */
    public void setEnabled ( boolean enabled ) {
        this.enabled = enabled;
    }

    /**
     * The {@code renderer} component (a {@code JLabel}) must be
     * updated according the given {@code value}.
     *
     * @param renderer The current renderer component.
     * @param value    The current value.
     */
    protected abstract void updateRendererComponent ( JLabel renderer, Object value );

}
