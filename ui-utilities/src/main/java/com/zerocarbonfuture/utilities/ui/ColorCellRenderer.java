/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.ui;


import com.zerocarbonfuture.utilities.ColorUtilities;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.ListCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.TreeCellRenderer;


/**
 * This class implements many cell renderer interfaces. It serves as:
 * <UL>
 * <LI>a <CODE>TreeCellRendere</CODE> that renders <CODE>Color</CODE> tree nodes
 * using <CODE>ColorLabel</CODE>s;</LI>
 * <LI>a <CODE>TableCellRendere</CODE> that renders <CODE>Color</CODE> table values
 * using <CODE>ColorLabel</CODE>s.</LI>
 * </UL>
 *
 * @author Claudio Rosati
 */
public class ColorCellRenderer
	extends BasicCellRenderer<Color> {

	/**
	 * The color label used to render a cell.
	 */
	private static final ColorLabel COLOR_LABEL = new ColorLabel();

	/**
	 * Creates a new <CODE>ColorCellRenderer</CODE> that uses a
	 * <CODE>ColorLabel</CODE> to render <CODE>Color</CODE> cells.
	 */
	public ColorCellRenderer() {
		this(null, null, null);
	}

	/**
	 * Creates a new <CODE>ColorCellRenderer</CODE> that uses a
	 * <CODE>ColorLabel</CODE> to render <CODE>Color</CODE> cells.
	 * The <CODE>ColorLabel</CODE> attributes (font, background, ...) are
	 * taken from the given <CODE>renderer</CODE>.
	 * <P>
	 * It is important to remember that if the cell value is not a
	 * <CODE>Color</CODE> object, then the given <CODE>renderer</CODE>
	 * will be used to render the cell. This allow a chain of renderers to
	 * be followed until a valid renderer is found for any given cell.
	 *
	 * @param renderer The chained <CODE>BasicCellRenderer</CODE>.
	 */
	public ColorCellRenderer( BasicCellRenderer<Color> renderer ) {
		this(renderer, renderer, renderer);
	}

	/**
	 * Creates a new <CODE>ColorCellRenderer</CODE> that uses a
	 * <CODE>ColorLabel</CODE> to render <CODE>Color</CODE> list cells.
	 * The <CODE>ColorLabel</CODE> attributes (font, background, ...) are
	 * taken from the given <CODE>listChainRenderer</CODE>.
	 * <P>
	 * It is important to remember that if the list value is not a
	 * <CODE>Color</CODE> object, then the given <CODE>listChainRenderer</CODE>
	 * will be used to render the cell. This allow a chain of renderers to
	 * be followed until a valid renderer is found for any given tree node.
	 *
	 * @param listChainRenderer The list cell renderer that provides default attributes.
	 *                          If <CODE>listChainRenderer</CODE> is <CODE>null</CODE> a
	 *                          private <CODE>DefaultListCellRenderer</CODE> object
	 *                          will be created.
	 */
	public ColorCellRenderer( ListCellRenderer<Color> listChainRenderer ) {
		this(listChainRenderer, null, null);
	}

	/**
	 * Creates a new <CODE>ColorCellRenderer</CODE> that uses a
	 * <CODE>ColorLabel</CODE> to render <CODE>Color</CODE> tree nodes.
	 * The <CODE>ColorLabel</CODE> attributes (font, background, ...) are
	 * taken from the given <CODE>treeChainRenderer</CODE>.
	 * <P>
	 * It is important to remember that if the table value is not a
	 * <CODE>Color</CODE> object, then the given <CODE>treeChainRenderer</CODE>
	 * will be used to render the cell. This allow a chain of renderers to
	 * be followed until a valid renderer is found for any given tree node.
	 *
	 * @param treeChainRenderer The tree cell renderer that provides default attributes.
	 *                          If <CODE>treeChainRenderer</CODE> is <CODE>null</CODE> a
	 *                          private <CODE>DefaultTreeCellRenderer</CODE> object
	 *                          will be created.
	 */
	public ColorCellRenderer( TreeCellRenderer treeChainRenderer ) {
		this(null, treeChainRenderer, null);
	}

	/**
	 * Creates a new <CODE>ColorCellRenderer</CODE> that uses a
	 * <CODE>ColorLabel</CODE> to render <CODE>Color</CODE> table values.
	 * The <CODE>ColorLabel</CODE> attributes (font, background, ...) are
	 * taken from the given <CODE>tableChainRenderer</CODE>.
	 * <P>
	 * It is important to remember that if the table value is not a
	 * <CODE>Color</CODE> object, then the given <CODE>tableChainRenderer</CODE>
	 * will be used to render the cell. This allow a chain of renderers to
	 * be followed until a valid renderer is found for any given table value.
	 *
	 * @param tableChainRenderer The table cell renderer that provides default attributes.
	 *                           If <CODE>tableChainRenderer</CODE> is <CODE>null</CODE> a
	 *                           private <CODE>DefaultTableCellRenderer</CODE> object
	 *                           will be created.
	 */
	public ColorCellRenderer( TableCellRenderer tableChainRenderer ) {
		this(null, null, tableChainRenderer);
	}

	/**
	 * Creates a new <CODE>ColorCellRenderer</CODE> that uses a
	 * <CODE>ColorLabel</CODE> to render <CODE>Color</CODE> tree nodes, table
	 * and list cells.
	 * The <CODE>ColorLabel</CODE> attributes (font, background, ...) are
	 * taken from the given cell renderers.
	 * <P>
	 * It is important to remember that if the cell value is not a
	 * <CODE>Color</CODE> object, then the given chain renderers
	 * will be used to render the cell. This allow a chain of renderers to
	 * be followed until a valid renderer is found for any given cell.
	 *
	 * @param listChainRenderer  The list cell renderer that provides default attributes.
	 *                           If <CODE>listChainRenderer</CODE> is <CODE>null</CODE> a
	 *                           private <CODE>DefaultListCellRenderer</CODE> object
	 *                           will be created.
	 * @param treeChainRenderer  The tree cell renderer that provides default attributes.
	 *                           If <CODE>treeChainRenderer</CODE> is <CODE>null</CODE> a
	 *                           private <CODE>DefaultTreeCellRenderer</CODE> object
	 *                           will be created.
	 * @param tableChainRenderer The table cell renderer that provides default attributes.
	 *                           If <CODE>tableChainRenderer</CODE> is <CODE>null</CODE> a
	 *                           private <CODE>DefaultTableCellRenderer</CODE> object
	 *                           will be created.
	 */
	public ColorCellRenderer(
		ListCellRenderer<Color> listChainRenderer,
		TreeCellRenderer treeChainRenderer,
		TableCellRenderer tableChainRenderer
	) {
		super(listChainRenderer, treeChainRenderer, tableChainRenderer);
	}

	@Override
	protected boolean isValueAccepted( Object value ) {
		return (value != null)
			&& ((value instanceof Color) || (value instanceof ColorCellRenderer.ColoredObject));
	}

	@Override
	protected void updateRendererComponent( JLabel renderer, Object value ) {
		if ( value instanceof Color ) {
			ColorCellRenderer.COLOR_LABEL.setColor((Color) value);
			renderer.setIcon(ColorCellRenderer.COLOR_LABEL.getIcon());
			renderer.setText(ColorCellRenderer.COLOR_LABEL.getText());
		} else {
			ColorCellRenderer.COLOR_LABEL.setColor(((ColorCellRenderer.ColoredObject) value).getColor());
			renderer.setIcon(ColorCellRenderer.COLOR_LABEL.getIcon());
			renderer.setText(((ColorCellRenderer.ColoredObject) value).getObject().toString());
		}
	}

	/**
	 * This class allows the association of an object with a color.
	 */
	@SuppressWarnings( "PublicInnerClass" )
	public static class ColoredObject {

		private final Color color;
		private final Object object;

		public ColoredObject( Object object, Color color ) {
			this.object = object;
			this.color = color;
		}

		public Color getColor() {
			return color;
		}

		public Object getObject() {
			return object;
		}

		@Override
		public String toString() {
			if ( object != null && color != null ) {
				return object.toString() + " - " + ColorUtilities.getRGBString(color);
			} else if ( object == null ) {
				return ColorUtilities.getRGBString(color);
			} else {
				return super.toString();
			}
		}

	}	//	class ColoredObject

}	//	class ColorCellRenderer
