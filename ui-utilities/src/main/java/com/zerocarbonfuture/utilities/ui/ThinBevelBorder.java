/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.ui;


import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.border.AbstractBorder;


/**
 * A class which implements a simple 1-pixel wide bevel border.
 *
 * @author Claudio Rosati
 */
public class ThinBevelBorder extends AbstractBorder {

	/**
	 * Lowered bevel type.
	 */
	public static final int LOWERED = 1;
	/**
	 * Raised bevel type.
	 */
	public static final int RAISED = 0;

	private static final long serialVersionUID = -6853664496066193400L;
	private int bevelType;
	private Color highlightColor = null;
	private Color shadowColor = null;

	/**
	 * Creates a bevel border with the specified type, with a 1-pixel wide
	 * line width, and whose colors will be derived from the background
	 * color of the component passed into the paintBorder method
	 *
	 * @param bevelType The type of bevel for the border.
	 */
	public ThinBevelBorder( int bevelType ) {
		this.bevelType = bevelType;
	}

	/**
	 * Creates a bevel border with the specified type, highlight and
	 * shadow colors, and with a 1-pixel wide line width.
	 *
	 * @param bevelType The type of bevel for the border.
	 * @param highlight The color to use for the bevel highlight.
	 * @param shadow    The color to use for the bevel shadow.
	 */
	public ThinBevelBorder( int bevelType, Color highlight, Color shadow ) {

		this(bevelType);

		this.highlightColor = highlight;
		this.shadowColor = shadow;

	}

	/**
	 * Returns the insets of the border.
	 *
	 * @param c The component for which this border insets value applies.
	 * @return The insets of the border.
	 */
	@Override
	public Insets getBorderInsets( final Component c ) {
		return new Insets(1, 1, 1, 1);
	}

	/**
	 * Reinitialize the insets parameter with this border current Insets.
	 *
	 * @param c      The component for which this border insets value applies.
	 * @param insets The object to be reinitialized.
	 * @return The insets of the border.
	 */
	@Override
	@SuppressWarnings( "NestedAssignment" )
	public Insets getBorderInsets( final Component c, final Insets insets ) {

		insets.left = insets.top = insets.right = insets.bottom = 1;

		return insets;

	}

	public Color getHighlightColor() {
		return highlightColor;
	}

	public Color getHighlightColor( Component c ) {

		Color hc = getHighlightColor();

		return (hc != null) ? hc : c.getBackground().brighter();

	}

	public Color getShadowColor() {
		return shadowColor;
	}

	public Color getShadowColor( Component c ) {

		Color sc = getShadowColor();

		return (sc != null) ? sc : c.getBackground().darker().darker();

	}

	/**
	 * Paints the border for the specified component with the specified
	 * position and size.
	 *
	 * @param c      the component for which this border is being painted
	 * @param g      the paint graphics
	 * @param x      the x position of the painted border
	 * @param y      the y position of the painted border
	 * @param width  the width of the painted border
	 * @param height the height of the painted border
	 */
	@Override
	public void paintBorder( Component c, Graphics g, int x, int y, int width, int height ) {
		if ( bevelType == RAISED ) {
			paintBorder(g.create(), x, y, width, height, getHighlightColor(c), getShadowColor(c));
		} else if ( bevelType == LOWERED ) {
			paintBorder(g.create(), x, y, width, height, getShadowColor(c), getHighlightColor(c));
		}
	}

	protected void paintBorder( Graphics g, int x, int y, int w, int h, Color sColor, Color hColor ) {

		g.translate(x, y);

		g.setColor(sColor);
		g.drawLine(0, 0, 0, h - 1);
		g.drawLine(1, 0, w - 1, 0);

		g.setColor(hColor);
		g.drawLine(1, h - 1, w - 1, h - 1);
		g.drawLine(w - 1, 1, w - 1, h - 2);

	}

}
