/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.ui;


import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Vector;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;


/**
 * A {@link JComboBox} whose pop-up width is always wide enough to display
 * all items without clipping them.
 *
 * @param <E> The type of the elements of this combo box.
 * @author Santhosh Kumar (Original author)
 * @author Claudio Rosati
 */
public class WideComboBox<E> extends JComboBox<E> {

	private static final long serialVersionUID = 6066658202877439718L;

	private boolean layingOut = false;
	private boolean wide = true;

	/**
	 * Creates a {@link WideComboBox} with a default data model. The
	 * default data model is an empty list of objects. Use {@link #addItem(java.lang.Object)}
	 * to add items. By default the first item in the data model becomes
	 * selected.
	 */
	public WideComboBox() {
		super();
	}

	/**
	 * Creates a {@link WideComboBox} that contains the elements in the
	 * specified array. By default the first item in the array (and therefore
	 * the data model) becomes selected.
	 *
	 * @param items An array of objects to insert into the combo box.
	 */
	public WideComboBox( E items[] ) {
		super(items);
	}

	/**
	 * Creates a {@link WideComboBox} that contains the elements in the
	 * specified {@link Vector}. By default the first item in the vector and
	 * therefore the data model) becomes selected.
	 *
	 * @param items A {@link Vector} of objects to insert into the combo box.
	 */
	public WideComboBox( @SuppressWarnings( "UseOfObsoleteCollectionType" ) Vector<E> items ) {
		super(items);
	}

	/**
	 * Creates a {@link WideComboBox} that takes it's items from an
	 * existing {@link ComboBoxModel}. Since the {@link ComboBoxModel} is
	 * provided, a combo box created using this constructor does not create a
	 * default combo box model and may impact how the insert, remove and add
	 * methods behave.
	 *
	 * @param aModel The {@link ComboBoxModel} that provides the displayed list
	 *               of items.
	 */
	public WideComboBox( ComboBoxModel<E> aModel ) {
		super(aModel);
	}

	@Override
	public void doLayout() {

		try {

			layingOut = true;

			super.doLayout();

		} finally {
			layingOut = false;
		}

	}

	@Override
	public Dimension getSize() {

		Dimension dim = super.getSize();

		if ( !layingOut && isWide() ) {
			dim.width = Math.max(dim.width, getPreferredSize().width);
			dim.width = Math.min(dim.width, Toolkit.getDefaultToolkit().getScreenSize().width);
		}

		return dim;

	}

	/**
	 * Returns {@code true} if this combo box is displaying its pop-up menu
	 * in a way that avoid clipping the rendered items.
	 *
	 * @return {@code true} if wide displaying is enabled.
	 */
	public boolean isWide() {
		return wide;
	}

	/**
	 * Sets whether this combo box must display its pop-up menu in a way that
	 * avoid clipping the rendered items, or not.
	 *
	 * @param wide {@code true} if wide displaying is to be enabled.
	 */
	public void setWide( boolean wide ) {

		boolean oldValue = isWide();

		this.wide = wide;

		firePropertyChange("wide", oldValue, wide);

	}

}
