/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.ui;


import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.border.LineBorder;


/**
 * A class which implements a line border where each edge of the border can be
 * enabled or disabled.
 *
 * @author Claudio Rosati
 */
@SuppressWarnings( "serial" )
public class EdgeBorder extends LineBorder {

	public static final int LEFT_EDGE = 0x01;
	public static final int TOP_EDGE = 0x02;
	public static final int RIGHT_EDGE = 0x04;
	public static final int BOTTOM_EDGE = 0x08;
	public static final int NO_EDGEs = 0x00;
	public static final int HORIZONTAL_EDGEs = EdgeBorder.TOP_EDGE | EdgeBorder.BOTTOM_EDGE;
	public static final int VERTICAL_EDGEs = EdgeBorder.LEFT_EDGE | EdgeBorder.RIGHT_EDGE;
	public static final int TOP_LEFT_EDGEs = EdgeBorder.TOP_EDGE | EdgeBorder.LEFT_EDGE;
	public static final int BOTTOM_RIGHT_EDGEs = EdgeBorder.BOTTOM_EDGE | EdgeBorder.RIGHT_EDGE;
	public static final int ALL_EDGEs = EdgeBorder.HORIZONTAL_EDGEs | EdgeBorder.VERTICAL_EDGEs;
	private int edges = EdgeBorder.ALL_EDGEs;

	/**
	 * Creates a line border with the specified color and a
	 * thickness equals to 1.
	 *
	 * @param edges The indicator of the edges to be painted. It can be an OR-ed
	 *              combination of <CODE>EdgeBorder.LEFT_EDGE</CODE>,
	 *              <CODE>EdgeBorder.TOP_EDGE</CODE>, <CODE>EdgeBorder.BOTTOM_EDGE</CODE>
	 *              and <CODE>EdgeBorder.RIGHT_EDGE</CODE>, or one of the pre-built
	 *              <CODE>EdgeBorder.<I>X</I>_EDGES</CODE> constants.
	 * @param color The color for the border.
	 */
	public EdgeBorder( int edges, Color color ) {
		this(edges, color, 1);
	}

	/**
	 * Creates a line border with the specified color and thickness.
	 *
	 * @param edges     The indicator of the edges to be painted. It can be an OR-ed
	 *                  combination of <CODE>EdgeBorder.LEFT_EDGE</CODE>,
	 *                  <CODE>EdgeBorder.TOP_EDGE</CODE>, <CODE>EdgeBorder.BOTTOM_EDGE</CODE>
	 *                  and <CODE>EdgeBorder.RIGHT_EDGE</CODE>, or one of the pre-built
	 *                  <CODE>EdgeBorder.<I>X</I>_EDGES</CODE> constants.
	 * @param color     The color of the border.
	 * @param thickness The thickness of the border.
	 */
	public EdgeBorder( int edges, Color color, int thickness ) {
		super(color, thickness, false);
		this.edges = edges;
	}

	/**
	 * Returns the insets of the border.
	 *
	 * @param c The component for which this border insets value applies.
	 * @return The insets of the border.
	 */
	@Override
	public Insets getBorderInsets( final Component c ) {
		return new Insets(
			((edges & EdgeBorder.TOP_EDGE) != 0) ? thickness : 0,
			((edges & EdgeBorder.LEFT_EDGE) != 0) ? thickness : 0,
			((edges & EdgeBorder.BOTTOM_EDGE) != 0) ? thickness : 0,
			((edges & EdgeBorder.RIGHT_EDGE) != 0) ? thickness : 0);
	}

	/**
	 * Reinitialize the insets parameter with this border current Insets.
	 *
	 * @param c      The component for which this border insets value applies.
	 * @param insets The object to be reinitialized.
	 * @return The new insets.
	 */
	@Override
	public Insets getBorderInsets( final Component c, final Insets insets ) {

		insets.top = ((edges & EdgeBorder.TOP_EDGE) != 0) ? thickness : 0;
		insets.left = ((edges & EdgeBorder.LEFT_EDGE) != 0) ? thickness : 0;
		insets.bottom = ((edges & EdgeBorder.BOTTOM_EDGE) != 0) ? thickness : 0;
		insets.right = ((edges & EdgeBorder.RIGHT_EDGE) != 0) ? thickness : 0;

		return insets;

	}

	/**
	 * @return Whether or not the border is opaque.
	 */
	@Override
	public boolean isBorderOpaque() {
		return lineColor.getAlpha() == 255;
	}

	/**
	 * Paints the border for the specified component with the
	 * specified position and size.
	 *
	 * @param c      The component for which this border is being painted.
	 * @param g      The paint graphics.
	 * @param x      The x position of the painted border.
	 * @param y	     The y position of the painted border.
	 * @param width  The width of the painted border.
	 * @param height The height of the painted border.
	 */
	@Override
	public void paintBorder( Component c, Graphics g, int x, int y, int width, int height ) {

		int start, end, startOffset, endOffset;
		Color oldColor = g.getColor();

		g.setColor(lineColor);
		g.translate(x, y);

		//	TOP & BOTTOM
		{

			startOffset = ((edges & EdgeBorder.LEFT_EDGE) != 0) ? 1 : 0;
			endOffset = ((edges & EdgeBorder.RIGHT_EDGE) != 0) ? 1 : 0;

			if ( (edges & EdgeBorder.TOP_EDGE) != 0 ) {	//	Draw  TOP edge.

				start = 0;
				end = width - 1;

				for ( int i = 0; i < thickness; i++ ) {
					g.drawLine(start, i, end, i);
					start += startOffset;
					end -= endOffset;
				}

			}

			if ( (edges & EdgeBorder.BOTTOM_EDGE) != 0 ) {	//	Draw  BOTTOM edge.

				start = 0;
				end = width - 1;

				for ( int i = 0; i < thickness; i++ ) {
					g.drawLine(start, height - 1 - i, end, height - 1 - i);
					start += startOffset;
					end -= endOffset;
				}

			}

		}

		//	LEFT & RIGHT
		{

			startOffset = ((edges & EdgeBorder.TOP_EDGE) != 0) ? 1 : 0;
			endOffset = ((edges & EdgeBorder.BOTTOM_EDGE) != 0) ? 1 : 0;

			if ( (edges & EdgeBorder.LEFT_EDGE) != 0 ) {	//	Draw  LEFT edge.

				start = startOffset;
				end = height - 1 - endOffset;

				for ( int i = 0; i < thickness; i++ ) {
					g.drawLine(i, start, i, end);
					start += startOffset;
					end -= endOffset;
				}

			}

			if ( (edges & EdgeBorder.RIGHT_EDGE) != 0 ) {	//	Draw  RIGHT edge.

				start = startOffset;
				end = height - 1 - endOffset;

				for ( int i = 0; i < thickness; i++ ) {
					g.drawLine(width - 1 - i, start, width - 1 - i, end);
					start += startOffset;
					end -= endOffset;
				}

			}

		}

		g.translate(-x, -y);
		g.setColor(oldColor);

	}

}
