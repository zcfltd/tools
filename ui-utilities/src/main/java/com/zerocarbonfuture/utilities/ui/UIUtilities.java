/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities.ui;


import java.awt.EventQueue;
import java.lang.reflect.InvocationTargetException;
import javax.swing.SwingUtilities;


/**
 * Some user interface utilities.
 *
 * @author Claudio Rosati
 */
public class UIUtilities {

	/**
	 * Similar to {@link SwingUtilities#invokeAndWait(Runnable)}
	 * that works also from the event thread.
	 *
	 * @param doRun The task to be executed inside the event thread.
	 * @throws InterruptedException      If interrupted while waiting for
	 *                                   the event dispatching thread to
	 *                                   finish executing
	 *                                   <code>doRun.run()</code>.
	 * @throws InvocationTargetException If an exception is thrown
	 *                                   while running <code>doRun</code>.
	 * @see SwingUtilities#invokeAndWait(Runnable)
	 */
	public static void invokeAndWait( final Runnable doRun ) throws InterruptedException, InvocationTargetException {
		if ( EventQueue.isDispatchThread() ) {
			doRun.run();
		} else {
			EventQueue.invokeAndWait(doRun);
		}
	}

	/**
	 * Similar to {@link SwingUtilities#invokeLater(Runnable)}
	 * that works also from the event thread.
	 *
	 * @param doRun The task to be executed inside the event thread.
	 * @see SwingUtilities#invokeLater(Runnable)
	 */
	public static void invokeLater( final Runnable doRun ) {
		if ( EventQueue.isDispatchThread() ) {
			doRun.run();
		} else {
			EventQueue.invokeLater(doRun);
		}
	}

	private UIUtilities() {
	}

}
