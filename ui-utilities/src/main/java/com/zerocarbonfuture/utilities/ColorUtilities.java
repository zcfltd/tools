/*
 * Copyright (c) 2015, Zero Carbon Future and/or its affiliates. All rights reserved.
 * Zero Carbon Future PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.zerocarbonfuture.utilities;


import com.zerocarbonfuture.utilities.ui.ColorCellRenderer;
import com.zerocarbonfuture.utilities.ui.WideComboBox;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.Paint;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.swing.Icon;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.colorchooser.AbstractColorChooserPanel;


/**
 * Various utilities for colors.
 *
 * @author Claudio Rosati
 */
public class ColorUtilities {

	/**
	 * A perfect transparent color.
	 */
	public static final Color FULLY_TRANSPARENT = new Color(0, 0, 0, 0);

	//	Pastel colors definition.
	public static final Color P_ALLUMINIUM  = new Color(0.60F, 0.60F, 0.60F);
	public static final Color P_AQUA        = new Color(0.00F, 0.50F, 1.00F);
	public static final Color P_ASPARAGUS   = new Color(0.50F, 0.50F, 0.00F);
	public static final Color P_BANANA      = new Color(1.00F, 1.00F, 0.40F);
	public static final Color P_BLACK       = Color.black;
	public static final Color P_BLUE        = Color.blue;
	public static final Color P_BONDI       = new Color(0.60F, 1.00F, 1.00F);
	public static final Color P_BROWN       = new Color(0.40F, 0.20F, 0.00F);
	public static final Color P_BUBBLEGUM   = new Color(1.00F, 0.40F, 1.00F);
	public static final Color P_CANTALOUPE  = new Color(1.00F, 0.80F, 0.40F);
	public static final Color P_CARNATION   = new Color(1.00F, 0.40F, 0.80F);
	public static final Color P_CAYENNE     = new Color(0.50F, 0.00F, 0.00F);
	public static final Color P_CLOVER      = new Color(0.00F, 0.50F, 0.00F);
	public static final Color P_CYAN        = Color.cyan;
	public static final Color P_EGGPLANT    = new Color(0.25F, 0.00F, 0.50F);
	public static final Color P_FERN        = new Color(0.25F, 0.50F, 0.00F);
	public static final Color P_FLORA       = new Color(0.40F, 1.00F, 0.40F);
	public static final Color P_GRAPE       = new Color(0.50F, 0.00F, 1.00F);
	public static final Color P_GREEN       = Color.green;
	public static final Color P_HONEYDEW    = new Color(0.80F, 1.00F, 0.40F);
	public static final Color P_ICE         = new Color(0.40F, 1.00F, 1.00F);
	public static final Color P_IRON        = new Color(0.30F, 0.30F, 0.30F);
	public static final Color P_LAVENDER    = new Color(0.80F, 0.40F, 1.00F);
	public static final Color P_LEAD        = new Color(0.10F, 0.10F, 0.10F);
	public static final Color P_LEMON       = new Color(1.00F, 1.00F, 0.60F);
	public static final Color P_LIGHT_OCEAN = new Color(0.80F, 1.00F, 0.80F);
	public static final Color P_LIGHT_PLUM  = new Color(1.00F, 0.80F, 1.00F);
	public static final Color P_LIGHT_SKY   = new Color(0.80F, 1.00F, 1.00F);
	public static final Color P_LILLA       = new Color(0.80F, 0.60F, 1.00F);
	public static final Color P_LIME        = new Color(0.50F, 1.00F, 0.00F);
	public static final Color P_MAC_BLUE    = new Color(141, 200, 255);
	public static final Color P_MAC_GRAY    = new Color(197, 208, 220);
	public static final Color P_MAC_GREEN   = new Color(184, 247, 162);
	public static final Color P_MAC_ORANGE  = new Color(255, 202, 129);
	public static final Color P_MAC_PURPLE  = new Color(226, 177, 232);
	public static final Color P_MAC_RED     = new Color(255, 148, 141);
	public static final Color P_MAC_YELLOW  = new Color(255, 243, 144);
	public static final Color P_MAGENTA     = Color.magenta;
	public static final Color P_MAGNESIUM   = new Color(0.70F, 0.70F, 0.70F);
	public static final Color P_MAROON      = new Color(0.50F, 0.00F, 0.25F);
	public static final Color P_MERCURY     = new Color(0.90F, 0.90F, 0.90F);
	public static final Color P_MIDNIGHT    = new Color(0.00F, 0.00F, 0.50F);
	public static final Color P_MOCHA       = new Color(0.50F, 0.25F, 0.00F);
	public static final Color P_MOSS        = new Color(0.00F, 0.50F, 0.25F);
	public static final Color P_NICKEL      = new Color(0.50F, 0.50F, 0.50F);
	public static final Color P_OCEAN       = new Color(0.00F, 0.25F, 0.50F);
	public static final Color P_ORANGE      = Color.orange;
	public static final Color P_ORCHID      = new Color(0.40F, 0.40F, 1.00F);
	public static final Color P_PINK        = Color.pink;
	public static final Color P_PLUM        = new Color(0.50F, 0.00F, 0.50F);
	public static final Color P_RED         = Color.red;
	public static final Color P_ROSE        = new Color(1.00F, 0.80F, 0.80F);
	public static final Color P_SALMON      = new Color(1.00F, 0.40F, 0.40F);
	public static final Color P_SEA_FOAM    = new Color(0.00F, 1.00F, 0.50F);
	public static final Color P_SILVER      = new Color(0.80F, 0.80F, 0.80F);
	public static final Color P_SKY         = new Color(0.40F, 0.80F, 1.00F);
	public static final Color P_SPINDRIFT   = new Color(0.40F, 1.00F, 0.80F);
	public static final Color P_STEEL       = new Color(0.40F, 0.40F, 0.40F);
	public static final Color P_STRAWBERRY  = new Color(1.00F, 0.00F, 0.50F);
	public static final Color P_TAN         = new Color(1.00F, 0.89F, 0.75F);
	public static final Color P_TANGERINE   = new Color(1.00F, 0.50F, 0.00F);
	public static final Color P_TEAL        = new Color(0.00F, 0.50F, 0.50F);
	public static final Color P_TUNGSTEN    = new Color(0.20F, 0.20F, 0.20F);
	public static final Color P_TURQUOISE   = new Color(0.60F, 0.80F, 1.00F);
	public static final Color P_VIOLET      = new Color(0.80F, 0.80F, 1.00F);
	public static final Color P_WHITE       = Color.white;
	public static final Color P_YELLOW      = Color.yellow;

	//	X-Window colors definition.
	public static final Color X_ALICE_BLUE             = new Color(240, 248, 255);
	public static final Color X_ANTIQUE_WHITE          = new Color(250, 235, 215);
	public static final Color X_AQUA                   = new Color( 24, 255, 255);
	public static final Color X_AQUAMARINE             = new Color(127, 255, 212);
	public static final Color X_AZURE                  = new Color(240, 255, 255);
	public static final Color X_BEIGE                  = new Color(245, 245, 220);
	public static final Color X_BISQUE                 = new Color(255, 228, 196);
	public static final Color X_BLACK                  = Color.black;
	public static final Color X_BLANCHED_ALMOND        = new Color(255, 255, 205);
	public static final Color X_BLUE                   = Color.blue;
	public static final Color X_BLUE_VIOLET            = new Color(138,  43, 226);
	public static final Color X_BROWN                  = new Color(165,  42,  42);
	public static final Color X_BURLY_WOOD             = new Color(222, 184, 135);
	public static final Color X_CADET_BLUE             = new Color( 95, 158, 160);
	public static final Color X_CHARTREUSE             = new Color(127, 255,   0);
	public static final Color X_XHOXOLATE              = new Color(210, 105,  30);
	public static final Color X_CORAL                  = new Color(255, 127,  80);
	public static final Color X_CORNFLOWER_BLUE        = new Color(100, 149, 237);
	public static final Color x_CORNSILK               = new Color(255, 248, 220);
	public static final Color X_CRIMSON                = new Color(220,  20,  60);
	public static final Color X_CYAN                   = Color.cyan;
	public static final Color X_DARK_BLUE              = new Color(  0,   0, 139);
	public static final Color X_DARK_CYAN              = new Color(  0, 139, 139);
	public static final Color X_DARK_GOLDENROD         = new Color(184, 134,  11);
	public static final Color X_DARK_GRAY              = new Color(169, 169, 169);
	public static final Color X_DARK_GREEN             = new Color(  0, 100,   0);
	public static final Color X_DARK_KHAKI             = new Color(189, 183, 107);
	public static final Color X_DARK_MAGENTA           = new Color(139,   0, 139);
	public static final Color X_DARK_OLIVE_GREEN       = new Color( 85, 107,  47);
	public static final Color X_DARK_ORANGE            = new Color(255, 140,   0);
	public static final Color X_DARK_ORCHID            = new Color(153,  50, 204);
	public static final Color X_DARK_RED               = new Color(139,   0,   0);
	public static final Color X_DARK_SALMON            = new Color(233, 150, 122);
	public static final Color X_DARK_SEA_GREEN         = new Color(143, 188, 143);
	public static final Color X_DARK_SLATE_BLUE        = new Color( 72,  61, 139);
	public static final Color X_DARK_SLATE_GRAY        = new Color( 47,  79,  79);
	public static final Color X_DARK_TURQUOISE         = new Color(  0, 206, 209);
	public static final Color X_DARK_VIOLET            = new Color(148,   0, 211);
	public static final Color X_DEEP_PINK              = new Color(255,  20, 147);
	public static final Color X_DEEP_SKY_BLUE          = new Color(  0, 191, 255);
	public static final Color X_DIM_GRAY               = new Color(105, 105, 105);
	public static final Color X_DODGER_BLUE            = new Color( 30, 144, 255);
	public static final Color X_FIREBRICK              = new Color(178,  34,  34);
	public static final Color X_FLORAL_WHITE           = new Color(255, 250, 240);
	public static final Color X_FOREST_GREEN           = new Color( 34, 139,  34);
	public static final Color X_FUCHSIA                = new Color(255,  24, 255);
	public static final Color X_GAINSBORO              = new Color(220, 220, 220);
	public static final Color X_GHOST_WHITE            = new Color(248, 248, 255);
	public static final Color X_GOLD                   = new Color(255, 215,   0);
	public static final Color X_GOLDENROD              = new Color(218, 165,  32);
	public static final Color X_GRAY                   = new Color(136, 136, 136);
	public static final Color X_GREEN                  = new Color(  0, 136,   0);
	public static final Color X_GREEN_YELLOW           = new Color(173, 255,  47);
	public static final Color X_HONEYDEW               = new Color(240, 255, 240);
	public static final Color X_HOT_PINK               = new Color(255, 105, 180);
	public static final Color X_INDIAN_RED             = new Color(205,  92,  92);
	public static final Color X_INDIGO                 = new Color( 75,   0, 130);
	public static final Color X_IVORY                  = new Color(255, 240, 240);
	public static final Color X_KHAKI                  = new Color(240, 230, 140);
	public static final Color X_LAVENDER               = new Color(230, 230, 250);
	public static final Color X_LAVENDERBLUSH          = new Color(255, 240, 245);
	public static final Color X_LAWN_GREEN             = new Color(124, 252,   0);
	public static final Color X_LEMONCHIFFON           = new Color(255, 250, 205);
	public static final Color X_LIGHT_BLUE             = new Color(173, 216, 230);
	public static final Color X_LIGHT_CORAL            = new Color(240, 128, 128);
	public static final Color X_LIGHT_CYAN             = new Color(224, 255, 255);
	public static final Color X_LIGHT_GOLDENROD_YELLOW = new Color(250, 250, 210);
	public static final Color X_LIGHT_GREEN            = new Color(144, 238, 144);
	public static final Color X_LIGHT_GRAY             = new Color(211, 211, 211);
	public static final Color X_LIGHT_PINK             = new Color(255, 182, 193);
	public static final Color X_LIGHT_SALMON           = new Color(255, 160, 122);
	public static final Color X_LIGHT_SEA_GREEN        = new Color( 32, 178, 170);
	public static final Color X_LIGHT_SKY_BLUE         = new Color(135, 206, 250);
	public static final Color X_LIGHT_SLATE_GRAY       = new Color(119, 136, 153);
	public static final Color X_LIGHT_STEEL_BLUE       = new Color(176, 196, 222);
	public static final Color X_LIGHT_YELLOW           = new Color(255, 255, 224);
	public static final Color X_LIME                   = Color.green;
	public static final Color X_LIME_GREEN             = new Color( 50, 205,  50);
	public static final Color X_LINEN                  = new Color(250, 240, 230);
	public static final Color X_MAGENTA                = Color.magenta;
	public static final Color X_MAROON                 = new Color(136,   0,   0);
	public static final Color X_MEDIUM_AQUAMARINE      = new Color(102, 205, 170);
	public static final Color X_MEDIUM_BLUE            = new Color(  0,   0, 205);
	public static final Color X_MEDIUM_ORCHID          = new Color(186,  85, 211);
	public static final Color X_MEDIUM_PURPLE          = new Color(147, 112, 219);
	public static final Color X_MEDIUM_SEA_GREEN       = new Color( 60, 179, 113);
	public static final Color X_MEDIUM_SLATE_BLUE      = new Color(123, 104, 238);
	public static final Color X_MEDIUM_SPRING_GREEN    = new Color(  0, 250, 154);
	public static final Color X_MEDIUM_TURQUOISE       = new Color( 72, 209, 204);
	public static final Color X_MEDIUM_VIOLET_RED      = new Color(199,  21, 133);
	public static final Color X_MIDNIGHT_BLUE          = new Color( 25,  25, 112);
	public static final Color X_MINTCREAM              = new Color(245, 255, 250);
	public static final Color X_MISTY_ROSE             = new Color(255, 228, 225);
	public static final Color X_MOCASSIN               = new Color(255, 228, 181);
	public static final Color X_NAVAJO_WHITE           = new Color(255, 222, 173);
	public static final Color X_NAVY                   = new Color(  0,   0, 136);
	public static final Color X_OLDLACE                = new Color(253, 245, 230);
	public static final Color X_OLIVE                  = new Color(136, 136,   0);
	public static final Color X_OLIVEdRAB              = new Color(107, 142,  35);
	public static final Color X_ORANGE                 = new Color(255, 165,   0);
	public static final Color X_ORANGE_RED             = new Color(255,  69,   0);
	public static final Color X_ORCHID                 = new Color(218, 112, 214);
	public static final Color X_PALE_GOLDENROD         = new Color(238, 232, 170);
	public static final Color X_PALE_GREEN             = new Color(152, 251, 152);
	public static final Color X_PALE_TURQUOISE         = new Color(175, 238, 238);
	public static final Color X_PALE_VIOLET_RED        = new Color(219, 112, 147);
	public static final Color X_PAPAYAWHIP             = new Color(255, 239, 213);
	public static final Color X_PEACHPUFF              = new Color(255, 218, 185);
	public static final Color X_PERU                   = new Color(205, 133,  63);
	public static final Color X_PINK                   = new Color(255, 192, 203);
	public static final Color X_PLUM                   = new Color(221, 160, 221);
	public static final Color X_POWDER_BLUE            = new Color(176, 224, 230);
	public static final Color X_PURPLE                 = new Color(136,   0, 136);
	public static final Color X_RED                    = Color.red;
	public static final Color X_ROSY_BROWN             = new Color(188, 143, 143);
	public static final Color X_ROYAL_BLUE             = new Color( 65, 105, 225);
	public static final Color X_SADDLE_BROWN           = new Color(139,  69,  19);
	public static final Color X_SALMON                 = new Color(250, 128, 114);
	public static final Color X_SANDY_BROWN            = new Color(244, 164,  96);
	public static final Color X_SEA_GREEN              = new Color( 46, 139,  87);
	public static final Color X_SEA_SHELL              = new Color(255, 245, 238);
	public static final Color X_SIENNA                 = new Color(160,  82,  45);
	public static final Color X_SILVER                 = new Color(200, 200, 200);
	public static final Color X_SKY_BLUE               = new Color(135, 206, 235);
	public static final Color X_SLATE_BLUE             = new Color(106,  90, 205);
	public static final Color X_SLATE_GRAY             = new Color(112, 128, 144);
	public static final Color X_SNOW                   = new Color(255, 250, 250);
	public static final Color X_SPRING_GREEN           = new Color(  0, 255, 127);
	public static final Color X_STEEL_BLUE             = new Color( 70, 138, 180);
	public static final Color X_TAN                    = new Color(210, 180, 140);
	public static final Color X_TEAL                   = new Color(  0, 136, 136);
	public static final Color X_THISTLE                = new Color(216, 191, 216);
	public static final Color X_TOMATO                 = new Color(253,  99,  71);
	public static final Color X_TURQUOISE              = new Color( 64, 224, 208);
	public static final Color X_VIOLET                 = new Color(238, 130, 238);
	public static final Color X_WHEAT                  = new Color(245, 222, 179);
	public static final Color X_WHITE                  = Color.white;
	public static final Color X_WHITE_SMOKE            = new Color(245, 245, 245);
	public static final Color X_YELLOW                 = Color.yellow;
	public static final Color X_YELLOW_GREEN           = new Color(154, 205,  50);

	/**
	 * The resource bundle used for colors names.
	 */
	private static final String COLORS_BUNDLE = "com/zerocarbonfuture/utilities/Colors";

	/**
	 * Pastels color list.
	 */
    private static final Color[] PASTELS_COLORS = {
        ColorUtilities.P_ALLUMINIUM,
        ColorUtilities.P_AQUA,
        ColorUtilities.P_ASPARAGUS,
        ColorUtilities.P_BANANA,
        ColorUtilities.P_BLACK,
        ColorUtilities.P_BLUE,
        ColorUtilities.P_BONDI,
        ColorUtilities.P_BROWN,
        ColorUtilities.P_BUBBLEGUM,
        ColorUtilities.P_CANTALOUPE,
        ColorUtilities.P_CARNATION,
        ColorUtilities.P_CAYENNE,
        ColorUtilities.P_CLOVER,
        ColorUtilities.P_CYAN,
        ColorUtilities.P_EGGPLANT,
        ColorUtilities.P_FERN,
        ColorUtilities.P_FLORA,
        ColorUtilities.P_GRAPE,
        ColorUtilities.P_GREEN,
        ColorUtilities.P_HONEYDEW,
        ColorUtilities.P_ICE,
        ColorUtilities.P_IRON,
        ColorUtilities.P_LAVENDER,
        ColorUtilities.P_LEAD,
        ColorUtilities.P_LEMON,
        ColorUtilities.P_LIGHT_OCEAN,
        ColorUtilities.P_LIGHT_PLUM,
        ColorUtilities.P_LIGHT_SKY,
        ColorUtilities.P_LILLA,
        ColorUtilities.P_LIME,
        ColorUtilities.P_MAC_BLUE,
        ColorUtilities.P_MAC_GRAY,
        ColorUtilities.P_MAC_GREEN,
        ColorUtilities.P_MAC_ORANGE,
        ColorUtilities.P_MAC_PURPLE,
        ColorUtilities.P_MAC_RED,
        ColorUtilities.P_MAC_YELLOW,
        ColorUtilities.P_MAGENTA,
        ColorUtilities.P_MAGNESIUM,
        ColorUtilities.P_MAROON,
        ColorUtilities.P_MERCURY,
        ColorUtilities.P_MIDNIGHT,
        ColorUtilities.P_MOCHA,
        ColorUtilities.P_MOSS,
        ColorUtilities.P_NICKEL,
        ColorUtilities.P_OCEAN,
        ColorUtilities.P_ORANGE,
        ColorUtilities.P_ORCHID,
        ColorUtilities.P_PINK,
        ColorUtilities.P_PLUM,
        ColorUtilities.P_RED,
        ColorUtilities.P_ROSE,
        ColorUtilities.P_SALMON,
        ColorUtilities.P_SEA_FOAM,
        ColorUtilities.P_SILVER,
        ColorUtilities.P_SKY,
        ColorUtilities.P_SPINDRIFT,
        ColorUtilities.P_STEEL,
        ColorUtilities.P_STRAWBERRY,
        ColorUtilities.P_TAN,
        ColorUtilities.P_TANGERINE,
        ColorUtilities.P_TEAL,
        ColorUtilities.P_TUNGSTEN,
        ColorUtilities.P_TURQUOISE,
        ColorUtilities.P_VIOLET,
        ColorUtilities.P_WHITE,
        ColorUtilities.P_YELLOW
    };

	/**
	 * X-Window color list.
	 */
    private static final Color[] X_COLORS = {
        ColorUtilities.X_ALICE_BLUE,
        ColorUtilities.X_ANTIQUE_WHITE,
        ColorUtilities.X_AQUA,
        ColorUtilities.X_AQUAMARINE,
        ColorUtilities.X_AZURE,
        ColorUtilities.X_BEIGE,
        ColorUtilities.X_BISQUE,
        ColorUtilities.X_BLACK,
        ColorUtilities.X_BLANCHED_ALMOND,
        ColorUtilities.X_BLUE,
        ColorUtilities.X_BLUE_VIOLET,
        ColorUtilities.X_BROWN,
        ColorUtilities.X_BURLY_WOOD,
        ColorUtilities.X_CADET_BLUE,
        ColorUtilities.X_CHARTREUSE,
        ColorUtilities.X_XHOXOLATE,
        ColorUtilities.X_CORAL,
        ColorUtilities.X_CORNFLOWER_BLUE,
        ColorUtilities.x_CORNSILK,
        ColorUtilities.X_CRIMSON,
        ColorUtilities.X_CYAN,
        ColorUtilities.X_DARK_BLUE,
        ColorUtilities.X_DARK_CYAN,
        ColorUtilities.X_DARK_GOLDENROD,
        ColorUtilities.X_DARK_GRAY,
        ColorUtilities.X_DARK_GREEN,
        ColorUtilities.X_DARK_KHAKI,
        ColorUtilities.X_DARK_MAGENTA,
        ColorUtilities.X_DARK_OLIVE_GREEN,
        ColorUtilities.X_DARK_ORANGE,
        ColorUtilities.X_DARK_ORCHID,
        ColorUtilities.X_DARK_RED,
        ColorUtilities.X_DARK_SALMON,
        ColorUtilities.X_DARK_SEA_GREEN,
        ColorUtilities.X_DARK_SLATE_BLUE,
        ColorUtilities.X_DARK_SLATE_GRAY,
        ColorUtilities.X_DARK_TURQUOISE,
        ColorUtilities.X_DARK_VIOLET,
        ColorUtilities.X_DEEP_PINK,
        ColorUtilities.X_DEEP_SKY_BLUE,
        ColorUtilities.X_DIM_GRAY,
        ColorUtilities.X_DODGER_BLUE,
        ColorUtilities.X_FIREBRICK,
        ColorUtilities.X_FLORAL_WHITE,
        ColorUtilities.X_FOREST_GREEN,
        ColorUtilities.X_FUCHSIA,
        ColorUtilities.X_GAINSBORO,
        ColorUtilities.X_GHOST_WHITE,
        ColorUtilities.X_GOLD,
        ColorUtilities.X_GOLDENROD,
        ColorUtilities.X_GRAY,
        ColorUtilities.X_GREEN,
        ColorUtilities.X_GREEN_YELLOW,
        ColorUtilities.X_HONEYDEW,
        ColorUtilities.X_HOT_PINK,
        ColorUtilities.X_INDIAN_RED,
        ColorUtilities.X_INDIGO,
        ColorUtilities.X_IVORY,
        ColorUtilities.X_KHAKI,
        ColorUtilities.X_LAVENDER,
        ColorUtilities.X_LAVENDERBLUSH,
        ColorUtilities.X_LAWN_GREEN,
        ColorUtilities.X_LEMONCHIFFON,
        ColorUtilities.X_LIGHT_BLUE,
        ColorUtilities.X_LIGHT_CORAL,
        ColorUtilities.X_LIGHT_CYAN,
        ColorUtilities.X_LIGHT_GOLDENROD_YELLOW,
        ColorUtilities.X_LIGHT_GREEN,
        ColorUtilities.X_LIGHT_GRAY,
        ColorUtilities.X_LIGHT_PINK,
        ColorUtilities.X_LIGHT_SALMON,
        ColorUtilities.X_LIGHT_SEA_GREEN,
        ColorUtilities.X_LIGHT_SKY_BLUE,
        ColorUtilities.X_LIGHT_SLATE_GRAY,
        ColorUtilities.X_LIGHT_STEEL_BLUE,
        ColorUtilities.X_LIGHT_YELLOW,
        ColorUtilities.X_LIME,
        ColorUtilities.X_LIME_GREEN,
        ColorUtilities.X_LINEN,
        ColorUtilities.X_MAGENTA,
        ColorUtilities.X_MAROON,
        ColorUtilities.X_MEDIUM_AQUAMARINE,
        ColorUtilities.X_MEDIUM_BLUE,
        ColorUtilities.X_MEDIUM_ORCHID,
        ColorUtilities.X_MEDIUM_PURPLE,
        ColorUtilities.X_MEDIUM_SEA_GREEN,
        ColorUtilities.X_MEDIUM_SLATE_BLUE,
        ColorUtilities.X_MEDIUM_SPRING_GREEN,
        ColorUtilities.X_MEDIUM_TURQUOISE,
        ColorUtilities.X_MEDIUM_VIOLET_RED,
        ColorUtilities.X_MIDNIGHT_BLUE,
        ColorUtilities.X_MINTCREAM,
        ColorUtilities.X_MISTY_ROSE,
        ColorUtilities.X_MOCASSIN,
        ColorUtilities.X_NAVAJO_WHITE,
        ColorUtilities.X_NAVY,
        ColorUtilities.X_OLDLACE,
        ColorUtilities.X_OLIVE,
        ColorUtilities.X_OLIVEdRAB,
        ColorUtilities.X_ORANGE,
        ColorUtilities.X_ORANGE_RED,
        ColorUtilities.X_ORCHID,
        ColorUtilities.X_PALE_GOLDENROD,
        ColorUtilities.X_PALE_GREEN,
        ColorUtilities.X_PALE_TURQUOISE,
        ColorUtilities.X_PALE_VIOLET_RED,
        ColorUtilities.X_PAPAYAWHIP,
        ColorUtilities.X_PEACHPUFF,
        ColorUtilities.X_PERU,
        ColorUtilities.X_PINK,
        ColorUtilities.X_PLUM,
        ColorUtilities.X_POWDER_BLUE,
        ColorUtilities.X_PURPLE,
        ColorUtilities.X_RED,
        ColorUtilities.X_ROSY_BROWN,
        ColorUtilities.X_ROYAL_BLUE,
        ColorUtilities.X_SADDLE_BROWN,
        ColorUtilities.X_SALMON,
        ColorUtilities.X_SANDY_BROWN,
        ColorUtilities.X_SEA_GREEN,
        ColorUtilities.X_SEA_SHELL,
        ColorUtilities.X_SIENNA,
        ColorUtilities.X_SILVER,
        ColorUtilities.X_SKY_BLUE,
        ColorUtilities.X_SLATE_BLUE,
        ColorUtilities.X_SLATE_GRAY,
        ColorUtilities.X_SNOW,
        ColorUtilities.X_SPRING_GREEN,
        ColorUtilities.X_STEEL_BLUE,
        ColorUtilities.X_TAN,
        ColorUtilities.X_TEAL,
        ColorUtilities.X_THISTLE,
        ColorUtilities.X_TOMATO,
        ColorUtilities.X_TURQUOISE,
        ColorUtilities.X_VIOLET,
        ColorUtilities.X_WHEAT,
        ColorUtilities.X_WHITE,
        ColorUtilities.X_WHITE_SMOKE,
        ColorUtilities.X_YELLOW,
        ColorUtilities.X_YELLOW_GREEN
    };
    /**
     * Standard color list.
     */
    private static final Color[] STANDARD_COLORS = {
        Color.magenta,
        Color.pink,
        Color.red,
        Color.orange,
        Color.yellow,
        Color.green,
        Color.cyan,
        Color.blue,
        Color.black,
        Color.darkGray,
        Color.gray,
        Color.lightGray,
        Color.white
    };

    /**
     * Pastels color names list.
     */
    private static final String[] PASTELS_COLOR_NAMES = {
        colorString("ColorUtilities.<init>.<pastels>.Alluminium"),
        colorString("ColorUtilities.<init>.<pastels>.Aqua"),
        colorString("ColorUtilities.<init>.<pastels>.Asparagus"),
        colorString("ColorUtilities.<init>.<pastels>.Banana"),
        colorString("ColorUtilities.<init>.<standard>.Black"),
        colorString("ColorUtilities.<init>.<standard>.Blue"),
        colorString("ColorUtilities.<init>.<pastels>.Bondi"),
        colorString("ColorUtilities.<init>.<pastels>.Brown"),
        colorString("ColorUtilities.<init>.<pastels>.Bubblegum"),
        colorString("ColorUtilities.<init>.<pastels>.Cantaloupe"),
        colorString("ColorUtilities.<init>.<pastels>.Carnation"),
        colorString("ColorUtilities.<init>.<pastels>.Cayenne"),
        colorString("ColorUtilities.<init>.<pastels>.Clover"),
        colorString("ColorUtilities.<init>.<standard>.Cyan"),
        colorString("ColorUtilities.<init>.<pastels>.Eggplant"),
        colorString("ColorUtilities.<init>.<pastels>.Fern"),
        colorString("ColorUtilities.<init>.<pastels>.Flora"),
        colorString("ColorUtilities.<init>.<pastels>.Grape"),
        colorString("ColorUtilities.<init>.<standard>.Green"),
        colorString("ColorUtilities.<init>.<pastels>.Honeydew"),
        colorString("ColorUtilities.<init>.<pastels>.Ice"),
        colorString("ColorUtilities.<init>.<pastels>.Iron"),
        colorString("ColorUtilities.<init>.<pastels>.Lavender"),
        colorString("ColorUtilities.<init>.<pastels>.Lead"),
        colorString("ColorUtilities.<init>.<pastels>.Lemon"),
        colorString("ColorUtilities.<init>.<pastels>.LightOcean"),
        colorString("ColorUtilities.<init>.<pastels>.LightPlum"),
        colorString("ColorUtilities.<init>.<pastels>.LightSky"),
        colorString("ColorUtilities.<init>.<pastels>.Lilla"),
        colorString("ColorUtilities.<init>.<pastels>.Lime"),
        colorString("ColorUtilities.<init>.<pastels>.MacBlue"),
        colorString("ColorUtilities.<init>.<pastels>.MacGray"),
        colorString("ColorUtilities.<init>.<pastels>.MacGreen"),
        colorString("ColorUtilities.<init>.<pastels>.MacOrange"),
        colorString("ColorUtilities.<init>.<pastels>.MacPurple"),
        colorString("ColorUtilities.<init>.<pastels>.MacRed"),
        colorString("ColorUtilities.<init>.<pastels>.MacYellow"),
        colorString("ColorUtilities.<init>.<standard>.Magenta"),
        colorString("ColorUtilities.<init>.<pastels>.Magnesium"),
        colorString("ColorUtilities.<init>.<pastels>.Maroon"),
        colorString("ColorUtilities.<init>.<pastels>.Mercury"),
        colorString("ColorUtilities.<init>.<pastels>.Midnight"),
        colorString("ColorUtilities.<init>.<pastels>.Mocha"),
        colorString("ColorUtilities.<init>.<pastels>.Moss"),
        colorString("ColorUtilities.<init>.<pastels>.Nickel"),
        colorString("ColorUtilities.<init>.<pastels>.Ocean"),
        colorString("ColorUtilities.<init>.<standard>.Orange"),
        colorString("ColorUtilities.<init>.<pastels>.Orchid"),
        colorString("ColorUtilities.<init>.<standard>.Pink"),
        colorString("ColorUtilities.<init>.<pastels>.Plum"),
        colorString("ColorUtilities.<init>.<standard>.Red"),
        colorString("ColorUtilities.<init>.<pastels>.Rose"),
        colorString("ColorUtilities.<init>.<pastels>.Salmon"),
        colorString("ColorUtilities.<init>.<pastels>.SeaFoam"),
        colorString("ColorUtilities.<init>.<pastels>.Silver"),
        colorString("ColorUtilities.<init>.<pastels>.Sky"),
        colorString("ColorUtilities.<init>.<pastels>.Spindrift"),
        colorString("ColorUtilities.<init>.<pastels>.Steel"),
        colorString("ColorUtilities.<init>.<pastels>.Strawberry"),
        colorString("ColorUtilities.<init>.<pastels>.Tan"),
        colorString("ColorUtilities.<init>.<pastels>.Tangerine"),
        colorString("ColorUtilities.<init>.<pastels>.Teal"),
        colorString("ColorUtilities.<init>.<pastels>.Tungsten"),
        colorString("ColorUtilities.<init>.<pastels>.Turquoise"),
        colorString("ColorUtilities.<init>.<pastels>.Violet"),
        colorString("ColorUtilities.<init>.<standard>.White"),
        colorString("ColorUtilities.<init>.<standard>.Yellow")
    };

    /**
     * X-Window color names list.
     */
    private static final String[] X_COLOR_NAMES = {
        colorString("ColorUtilities.<init>.<X>.AliceBlue"),
        colorString("ColorUtilities.<init>.<X>.AntiqueWhite"),
        colorString("ColorUtilities.<init>.<X>.Aqua"),
        colorString("ColorUtilities.<init>.<X>.Aquamarine"),
        colorString("ColorUtilities.<init>.<X>.Azure"),
        colorString("ColorUtilities.<init>.<X>.Beige"),
        colorString("ColorUtilities.<init>.<X>.Bisque"),
        colorString("ColorUtilities.<init>.<X>.Black"),
        colorString("ColorUtilities.<init>.<X>.BlanchedAlmond"),
        colorString("ColorUtilities.<init>.<X>.Blue"),
        colorString("ColorUtilities.<init>.<X>.BlueViolet"),
        colorString("ColorUtilities.<init>.<X>.Brown"),
        colorString("ColorUtilities.<init>.<X>.BurlyWood"),
        colorString("ColorUtilities.<init>.<X>.CadetBlue"),
        colorString("ColorUtilities.<init>.<X>.Chartreuse"),
        colorString("ColorUtilities.<init>.<X>.Chocolate"),
        colorString("ColorUtilities.<init>.<X>.Coral"),
        colorString("ColorUtilities.<init>.<X>.CornflowerBlue"),
        colorString("ColorUtilities.<init>.<X>.Cornsilk"),
        colorString("ColorUtilities.<init>.<X>.Crimson"),
        colorString("ColorUtilities.<init>.<X>.Cyan"),
        colorString("ColorUtilities.<init>.<X>.DarkBlue"),
        colorString("ColorUtilities.<init>.<X>.DarkCyan"),
        colorString("ColorUtilities.<init>.<X>.DarkGoldenrod"),
        colorString("ColorUtilities.<init>.<X>.DarkGray"),
        colorString("ColorUtilities.<init>.<X>.DarkGreen"),
        colorString("ColorUtilities.<init>.<X>.DarkKhaki"),
        colorString("ColorUtilities.<init>.<X>.DarkMagenta"),
        colorString("ColorUtilities.<init>.<X>.DarkOliveGreen"),
        colorString("ColorUtilities.<init>.<X>.DarkOrange"),
        colorString("ColorUtilities.<init>.<X>.DarkOrchid"),
        colorString("ColorUtilities.<init>.<X>.DarkRed"),
        colorString("ColorUtilities.<init>.<X>.DarkSalmon"),
        colorString("ColorUtilities.<init>.<X>.DarkSeaGreen"),
        colorString("ColorUtilities.<init>.<X>.DarkSlateBlue"),
        colorString("ColorUtilities.<init>.<X>.DarkSlateGray"),
        colorString("ColorUtilities.<init>.<X>.DarkTurquoise"),
        colorString("ColorUtilities.<init>.<X>.DarkViolet"),
        colorString("ColorUtilities.<init>.<X>.DeepPink"),
        colorString("ColorUtilities.<init>.<X>.DeepSkyBlue"),
        colorString("ColorUtilities.<init>.<X>.DimGray"),
        colorString("ColorUtilities.<init>.<X>.DodgerBlue"),
        colorString("ColorUtilities.<init>.<X>.Firebrick"),
        colorString("ColorUtilities.<init>.<X>.FloralWhite"),
        colorString("ColorUtilities.<init>.<X>.ForestGreen"),
        colorString("ColorUtilities.<init>.<X>.Fuchsia"),
        colorString("ColorUtilities.<init>.<X>.Gainsboro"),
        colorString("ColorUtilities.<init>.<X>.GhostWhite"),
        colorString("ColorUtilities.<init>.<X>.Gold"),
        colorString("ColorUtilities.<init>.<X>.Goldenrod"),
        colorString("ColorUtilities.<init>.<X>.Gray"),
        colorString("ColorUtilities.<init>.<X>.Green"),
        colorString("ColorUtilities.<init>.<X>.GreenYellow"),
        colorString("ColorUtilities.<init>.<X>.Honeydew"),
        colorString("ColorUtilities.<init>.<X>.HotPink"),
        colorString("ColorUtilities.<init>.<X>.IndianRed"),
        colorString("ColorUtilities.<init>.<X>.Indigo"),
        colorString("ColorUtilities.<init>.<X>.Ivory"),
        colorString("ColorUtilities.<init>.<X>.Khaki"),
        colorString("ColorUtilities.<init>.<X>.Lavender"),
        colorString("ColorUtilities.<init>.<X>.Lavenderblush"),
        colorString("ColorUtilities.<init>.<X>.LawnGreen"),
        colorString("ColorUtilities.<init>.<X>.Lemonchiffon"),
        colorString("ColorUtilities.<init>.<X>.LightBlue"),
        colorString("ColorUtilities.<init>.<X>.LightCoral"),
        colorString("ColorUtilities.<init>.<X>.LightCyan"),
        colorString("ColorUtilities.<init>.<X>.LightGoldenrodYellow"),
        colorString("ColorUtilities.<init>.<X>.LightGreen"),
        colorString("ColorUtilities.<init>.<X>.LightGrey"),
        colorString("ColorUtilities.<init>.<X>.LightPink"),
        colorString("ColorUtilities.<init>.<X>.LightSalmon"),
        colorString("ColorUtilities.<init>.<X>.LightSeaGreen"),
        colorString("ColorUtilities.<init>.<X>.LightSkyBlue"),
        colorString("ColorUtilities.<init>.<X>.LightSlateGray"),
        colorString("ColorUtilities.<init>.<X>.LightSteelBlue"),
        colorString("ColorUtilities.<init>.<X>.LightYellow"),
        colorString("ColorUtilities.<init>.<X>.Lime"),
        colorString("ColorUtilities.<init>.<X>.LimeGreen"),
        colorString("ColorUtilities.<init>.<X>.Linen"),
        colorString("ColorUtilities.<init>.<X>.Magenta"),
        colorString("ColorUtilities.<init>.<X>.Maroon"),
        colorString("ColorUtilities.<init>.<X>.MediumAquamarine"),
        colorString("ColorUtilities.<init>.<X>.MediumBlue"),
        colorString("ColorUtilities.<init>.<X>.MediumOrchid"),
        colorString("ColorUtilities.<init>.<X>.MediumPurple"),
        colorString("ColorUtilities.<init>.<X>.MediumSeaGreen"),
        colorString("ColorUtilities.<init>.<X>.MediumSlateBlue"),
        colorString("ColorUtilities.<init>.<X>.MediumSpringGreen"),
        colorString("ColorUtilities.<init>.<X>.MediumTurquoise"),
        colorString("ColorUtilities.<init>.<X>.MediumVioletRed"),
        colorString("ColorUtilities.<init>.<X>.MidnightBlue"),
        colorString("ColorUtilities.<init>.<X>.Mintcream"),
        colorString("ColorUtilities.<init>.<X>.MistyRose"),
        colorString("ColorUtilities.<init>.<X>.Moccasin"),
        colorString("ColorUtilities.<init>.<X>.NavajoWhite"),
        colorString("ColorUtilities.<init>.<X>.Navy"),
        colorString("ColorUtilities.<init>.<X>.Oldlace"),
        colorString("ColorUtilities.<init>.<X>.Olive"),
        colorString("ColorUtilities.<init>.<X>.Olivedrab"),
        colorString("ColorUtilities.<init>.<X>.Orange"),
        colorString("ColorUtilities.<init>.<X>.OrangeRed"),
        colorString("ColorUtilities.<init>.<X>.Orchid"),
        colorString("ColorUtilities.<init>.<X>.PaleGoldenrod"),
        colorString("ColorUtilities.<init>.<X>.PaleGreen"),
        colorString("ColorUtilities.<init>.<X>.PaleTurquoise"),
        colorString("ColorUtilities.<init>.<X>.PaleVioletRed"),
        colorString("ColorUtilities.<init>.<X>.Papayawhip"),
        colorString("ColorUtilities.<init>.<X>.Peachpuff"),
        colorString("ColorUtilities.<init>.<X>.Peru"),
        colorString("ColorUtilities.<init>.<X>.Pink"),
        colorString("ColorUtilities.<init>.<X>.Plum"),
        colorString("ColorUtilities.<init>.<X>.PowderBlue"),
        colorString("ColorUtilities.<init>.<X>.Purple"),
        colorString("ColorUtilities.<init>.<X>.Red"),
        colorString("ColorUtilities.<init>.<X>.RosyBrown"),
        colorString("ColorUtilities.<init>.<X>.RoyalBlue"),
        colorString("ColorUtilities.<init>.<X>.SaddleBrown"),
        colorString("ColorUtilities.<init>.<X>.Salmon"),
        colorString("ColorUtilities.<init>.<X>.SandyBrown"),
        colorString("ColorUtilities.<init>.<X>.SeaGreen"),
        colorString("ColorUtilities.<init>.<X>.SeaShell"),
        colorString("ColorUtilities.<init>.<X>.Sienna"),
        colorString("ColorUtilities.<init>.<X>.Silver"),
        colorString("ColorUtilities.<init>.<X>.SkyBlue"),
        colorString("ColorUtilities.<init>.<X>.SlateBlue"),
        colorString("ColorUtilities.<init>.<X>.SlateGray"),
        colorString("ColorUtilities.<init>.<X>.Snow"),
        colorString("ColorUtilities.<init>.<X>.SpringGreen"),
        colorString("ColorUtilities.<init>.<X>.SteelBlue"),
        colorString("ColorUtilities.<init>.<X>.Tan"),
        colorString("ColorUtilities.<init>.<X>.Teal"),
        colorString("ColorUtilities.<init>.<X>.Thistle"),
        colorString("ColorUtilities.<init>.<X>.Tomato"),
        colorString("ColorUtilities.<init>.<X>.Turquoise"),
        colorString("ColorUtilities.<init>.<X>.Violet"),
        colorString("ColorUtilities.<init>.<X>.Wheat"),
        colorString("ColorUtilities.<init>.<X>.White"),
        colorString("ColorUtilities.<init>.<X>.WhiteSmoke"),
        colorString("ColorUtilities.<init>.<X>.Yellow"),
        colorString("ColorUtilities.<init>.<X>.YellowGreen")
    };

    /**
     * Standard color names list.
     */
    private static final String[] STANDARD_COLOR_NAMES = {
        colorString("ColorUtilities.<init>.<standard>.Magenta"),
        colorString("ColorUtilities.<init>.<standard>.Pink"),
        colorString("ColorUtilities.<init>.<standard>.Red"),
        colorString("ColorUtilities.<init>.<standard>.Orange"),
        colorString("ColorUtilities.<init>.<standard>.Yellow"),
        colorString("ColorUtilities.<init>.<standard>.Green"),
        colorString("ColorUtilities.<init>.<standard>.Cyan"),
        colorString("ColorUtilities.<init>.<standard>.Blue"),
        colorString("ColorUtilities.<init>.<standard>.Black"),
        colorString("ColorUtilities.<init>.<standard>.DarkGray"),
        colorString("ColorUtilities.<init>.<standard>.Gray"),
        colorString("ColorUtilities.<init>.<standard>.LightGray"),
        colorString("ColorUtilities.<init>.<standard>.White")
    };

    private static final Comparator<Color> NAME_COMPARATOR = new ColorComparator() {
        @Override
        public int compare ( Color c1, Color c2 ) {
            return getRGBString(c1).compareTo(getRGBString(c2));
        }
    };

    private static final Comparator<Color> RED_COMPARATOR = new ColorComparator() {
        @Override
        public int compare ( Color c1, Color c2 ) {
            return ( c1.getRed() < c2.getRed() ) ? -1
                : ( ( c1.getRed() > c2.getRed() ) ? 1
                : ( ( c1.getGreen() < c2.getGreen() ) ? -1
                : ( ( c1.getGreen() > c2.getGreen() ) ? 1
                : ( ( c1.getBlue() < c2.getBlue() ) ? -1
                : ( ( c1.getBlue() > c2.getBlue() ) ? 1 : 0 ) ) ) ) );
        }
    };

    private static final Comparator<Color> GREEN_COMPARATOR = new ColorComparator() {
        @Override
        public int compare ( Color c1, Color c2 ) {
            return ( c1.getGreen() < c2.getGreen() ) ? -1
                : ( ( c1.getGreen() > c2.getGreen() ) ? 1
                : ( ( c1.getBlue() < c2.getBlue() ) ? -1
                : ( ( c1.getBlue() > c2.getBlue() ) ? 1
                : ( ( c1.getRed() < c2.getRed() ) ? -1
                : ( ( c1.getRed() > c2.getRed() ) ? 1 : 0 ) ) ) ) );
        }
    };

    private static final Comparator<Color> BLUE_COMPARATOR = new ColorComparator() {
        @Override
        public int compare ( Color c1, Color c2 ) {
            return ( c1.getBlue() < c2.getBlue() ) ? -1
                : ( ( c1.getBlue() > c2.getBlue() ) ? 1
                : ( ( c1.getRed() < c2.getRed() ) ? -1
                : ( ( c1.getRed() > c2.getRed() ) ? 1
                : ( ( c1.getGreen() < c2.getGreen() ) ? -1
                : ( ( c1.getGreen() > c2.getGreen() ) ? 1 : 0 ) ) ) ) );
        }
    };

    private static final Comparator<Color> HUE_COMPARATOR = new ColorComparator() {
        @Override
        public int compare ( Color c1, Color c2 ) {
            float[] hsb1 = Color.RGBtoHSB(c1.getRed(), c1.getGreen(), c1.getBlue(), null);
            float[] hsb2 = Color.RGBtoHSB(c2.getRed(), c2.getGreen(), c2.getBlue(), null);
            return ( hsb1[0] < hsb2[0] ) ? -1
                : ( ( hsb1[0] > hsb2[0] ) ? 1
                : ( ( hsb1[1] < hsb2[1] ) ? -1
                : ( ( hsb1[1] > hsb2[1] ) ? 1
                : ( ( hsb1[2] < hsb2[2] ) ? -1
                : ( ( hsb1[2] > hsb2[2] ) ? 1 : 0 ) ) ) ) );
        }
    };

    private static final Comparator<Color> SATURATION_COMPARATOR = new ColorComparator() {
        @Override
        public int compare ( Color c1, Color c2 ) {
            float[] hsb1 = Color.RGBtoHSB(c1.getRed(), c1.getGreen(), c1.getBlue(), null);
            float[] hsb2 = Color.RGBtoHSB(c2.getRed(), c2.getGreen(), c2.getBlue(), null);
            return ( hsb1[1] < hsb2[1] ) ? -1
                : ( ( hsb1[1] > hsb2[1] ) ? 1
                : ( ( hsb1[2] < hsb2[2] ) ? -1
                : ( ( hsb1[2] > hsb2[2] ) ? 1
                : ( ( hsb1[0] < hsb2[0] ) ? -1
                : ( ( hsb1[0] > hsb2[0] ) ? 1 : 0 ) ) ) ) );
        }
    };

    private static final Comparator<Color> BRIGHTNESS_COMPARATOR = new ColorComparator() {
        @Override
        public int compare ( Color c1, Color c2 ) {
            float[] hsb1 = Color.RGBtoHSB(c1.getRed(), c1.getGreen(), c1.getBlue(), null);
            float[] hsb2 = Color.RGBtoHSB(c2.getRed(), c2.getGreen(), c2.getBlue(), null);
            return ( hsb1[2] < hsb2[2] ) ? -1
                : ( ( hsb1[2] > hsb2[2] ) ? 1
                : ( ( hsb1[0] < hsb2[0] ) ? -1
                : ( ( hsb1[0] > hsb2[0] ) ? 1
                : ( ( hsb1[1] < hsb2[1] ) ? -1
                : ( ( hsb1[1] > hsb2[1] ) ? 1 : 0 ) ) ) ) );
        }
    };

    /**
     * Avoid direct instantiation.
     */
    protected ColorUtilities () {
    }

    /**
     * Converts the components of a color, as specified by the HSB
     * model, to an equivalent set of values for the default RGB model.
     * <p>
     * The {@code saturation} and {@code brightness} components
     * should be floating-point values between zero and one
     * (numbers in the range 0.0-1.0). The {@code hue} component
     * can be any floating-point number. The floor of this number is
     * subtracted from it to create a fraction between 0 and 1. This
     * fractional number is then multiplied by 360 to produce the hue
     * angle in the HSB color model.
     * <p>
     * The integer that is returned by {@code HSBtoRGB} encodes the
     * value of a color in bits 0-23 of an integer value that is the same
     * format used by the method {@link java.awt.Color#getRGB()}.
     * This integer can be supplied as an argument to the
     * {@code Color} constructor that takes a single integer argument.
     *
     * @param hue        The hue component of the color.
     * @param saturation The saturation of the color.
     * @param brightness The brightness of the color.
     * @return The RGB value of the color with the indicated hue,
     *         saturation, and brightness.
     */
    public static Color HSBtoRGB ( float hue, float saturation, float brightness ) {

        int rgb[] = HSBtoRGB(hue, saturation, brightness, null);

        return new Color(rgb[0], rgb[1], rgb[2]);

    }

    /**
     * Converts the components of a color, as specified by the HSB
     * model, to an equivalent set of values for the default RGB model.
     * <p>
     * The {@code saturation} and {@code brightness} components
     * should be floating-point values between zero and one
     * (numbers in the range 0.0-1.0). The {@code hue} component
     * can be any floating-point number. The floor of this number is
     * subtracted from it to create a fraction between 0 and 1. This
     * fractional number is then multiplied by 360 to produce the hue
     * angle in the HSB color model.
     * <p>
     * The integer that is returned by {@code HSBtoRGB} encodes the
     * value of a color in bits 0-23 of an integer value that is the same
     * format used by the method {@link java.awt.Color#getRGB()}.
     * This integer can be supplied as an argument to the
     * {@code Color} constructor that takes a single integer argument.
     *
     * @param hue        The hue component of the color.
     * @param saturation The saturation of the color.
     * @param brightness The brightness of the color.
     * @param rgb        A pre-allocated array of ints; can be {@code null}.
     * @return An array of 3 integer RGB components.
     */
    @SuppressWarnings( "AssignmentToMethodParameter" )
    public static int[] HSBtoRGB ( float hue, float saturation, float brightness, int rgb[] ) {

        int rgbValue = Color.HSBtoRGB(hue, saturation, brightness);

        if ( rgb == null || rgb.length < 3 ) {
            rgb = new int[3];
        }

        rgb[0] = ( rgbValue & 0x00FF0000 ) >> 16;
        rgb[1] = ( rgbValue & 0x0000FF00 ) >> 8;
        rgb[2] = ( rgbValue & 0x000000FF );

        return rgb;

    }

    /**
     * Returns the RGB equivalent of a given HSL (Hue/Saturation/Luminance)
     * color.
     *
     * @param h The hue component, between 0.0 and 1.0.
     * @param s The saturation component, between 0.0 and 1.0.
     * @param l The luminance component, between 0.0 and 1.0.
     * @return A new {@code Color} object equivalent to the HSL components.
     */
    public static Color HSLtoRGB ( float h, float s, float l ) {
        return org.jdesktop.swingx.graphics.ColorUtilities.HSLtoRGB(h, s, l);
    }

    /**
     * Returns the RGB equivalent of a given HSL (Hue/Saturation/Luminance)
     * color. All three RGB components are integers between 0 and 255.
     *
     * @param h   The hue component, between 0.0 and 1.0.
     * @param s   The saturation component, between 0.0 and 1.0.
     * @param l   The luminance component, between 0.0 and 1.0.
     * @param rgb A pre-allocated array of ints; can be null.
     * @return {@code rgb} if non-null, a new array of 3 ints otherwise.
     * @throws IllegalArgumentException If {@code rgb} has a length lower
     *                                  than 3.
     */
    public static int[] HSLtoRGB ( float h, float s, float l, int[] rgb ) {
        return org.jdesktop.swingx.graphics.ColorUtilities.HSLtoRGB(h, s, l, rgb);
    }

    /**
     * Converts the components of a color, as specified by the default RGB
     * model, to an equivalent set of values for hue, saturation, and
     * brightness that are the three components of the HSB model.
     * <p>
     * If the {@code hsbvals} argument is {@code null}, then a
     * new array is allocated to return the result. Otherwise, the method
     * returns the array {@code hsbvals}, with the values put into
     * that array.
     *
     * @param r       The red component of the color.
     * @param g       The green component of the color.
     * @param b       The blue component of the color.
     * @param hsbvals The array used to return the
     *                three HSB values, or {@code null}.
     * @return An array of three elements containing the hue, saturation,
     *         and brightness (in that order), of the color with
     *         the indicated red, green, and blue components.
     */
    public static float[] RGBtoHSB ( int r, int g, int b, float[] hsbvals ) {
        return Color.RGBtoHSB(r, g, b, hsbvals);
    }

    /**
     * Converts the components of a color, as specified by the default RGB
     * model, to an equivalent set of values for hue, saturation, and
     * brightness that are the three components of the HSB model.
     *
     * @param r The red component of the color.
     * @param g The green component of the color.
     * @param b The blue component of the color.
     * @return An array of three elements containing the hue, saturation,
     *         and brightness (in that order), of the color with
     *         the indicated red, green, and blue components.
     */
    public static float[] RGBtoHSB ( int r, int g, int b ) {
        return Color.RGBtoHSB(r, g, b, null);
    }

    /**
     * Converts the components of a color, as specified by the default RGB
     * model, to an equivalent set of values for hue, saturation, and
     * brightness that are the three components of the HSB model.
     * <p>
     * If the {@code hsbvals} argument is {@code null}, then a
     * new array is allocated to return the result. Otherwise, the method
     * returns the array {@code hsbvals}, with the values put into
     * that array.
     *
     * @param color   The color.
     * @param hsbvals The array used to return the
     *                three HSB values, or {@code null}.
     * @return An array of three elements containing the hue, saturation,
     *         and brightness (in that order), of the color with
     *         the indicated red, green, and blue components.
     */
    public static float[] RGBtoHSB ( Color color, float[] hsbvals ) {
        return Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), hsbvals);
    }

    /**
     * Converts the components of a color, as specified by the default RGB
     * model, to an equivalent set of values for hue, saturation, and
     * brightness that are the three components of the HSB model.
     *
     * @param color The color.
     * @return An array of three elements containing the hue, saturation,
     *         and brightness (in that order), of the color with
     *         the indicated red, green, and blue components.
     */
    public static float[] RGBtoHSB ( Color color ) {
        return Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
    }

    /**
     * Returns the HSL (Hue/Saturation/Luminance) equivalent of a given
     * RGB color. All three HSL components are between 0.0 and 1.0.
     *
     * @param color The RGB color to convert.
     * @return A new array of 3 floats corresponding to the HSL components.
     */
    public static float[] RGBtoHSL ( Color color ) {
        return org.jdesktop.swingx.graphics.ColorUtilities.RGBtoHSL(color);
    }

    /**
     * Returns the HSL (Hue/Saturation/Luminance) equivalent of a given
     * RGB color. All three HSL components are between 0.0 and 1.0.
     *
     * @param color The RGB color to convert.
     * @param hsl   A pre-allocated array of floats; can be null.
     * @return {@code hsl} if non-null, a new array of 3 floats otherwise.
     * @throws IllegalArgumentException If {@code hsl} has a length lower
     *                                  than 3.
     */
    public static float[] RGBtoHSL ( Color color, float[] hsl ) {
        return org.jdesktop.swingx.graphics.ColorUtilities.RGBtoHSL(color, hsl);
    }

    /**
     * Returns the HSL (Hue/Saturation/Luminance) equivalent of a given
     * RGB color. All three HSL components are between 0.0 and 1.0.
     *
     * @param r The red component, between 0 and 255.
     * @param g The green component, between 0 and 255.
     * @param b The blue component, between 0 and 255.
     * @return A new array of 3 floats corresponding to the HSL components.
     */
    public static float[] RGBtoHSL ( int r, int g, int b ) {
        return org.jdesktop.swingx.graphics.ColorUtilities.RGBtoHSL(r, g, b);
    }

    /**
     * Returns the HSL (Hue/Saturation/Luminance) equivalent of a given
     * RGB color. All three HSL components are floats between 0.0 and 1.0.
     *
     * @param r   The red component, between 0 and 255.
     * @param g   The green component, between 0 and 255.
     * @param b   The blue component, between 0 and 255.
     * @param hsl A pre-allocated array of floats; can be null.
     * @return {@code hsl} if non-null, a new array of 3 floats otherwise.
     * @throws IllegalArgumentException If {@code hsl} has a length lower
     *                                  than 3.
     */
    public static float[] RGBtoHSL ( int r, int g, int b, float[] hsl ) {
        return org.jdesktop.swingx.graphics.ColorUtilities.RGBtoHSL(r, g, b, hsl);
    }

    /**
     * Make an even blend between two colors.
     *
     * @param color1 First color to blend.
     * @param color2 Second color to blend.
     * @return Blended color.
     */
    public static Color blend ( Color color1, Color color2 ) {
        return blend(color1, color2, 0.5);
    }

    /**
     * Blend two colors.
     *
     * @param color1 First color to blend.
     * @param color2 Second color to blend.
     * @param ratio  Blend ratio. 0.5 will give even blend, 1.0 will return
     *               color1, 0.0 will return color2 and so on.
     * @return Blended color.
     */
    public static Color blend ( Color color1, Color color2, double ratio ) {

        float r = (float) ratio;
        float ir = (float) 1.0 - r;
        float rgb1[] = new float[3];
        float rgb2[] = new float[3];

        color1.getColorComponents(rgb1);
        color2.getColorComponents(rgb2);

        Color color = new Color(
            rgb1[0] * r + rgb2[0] * ir,
            rgb1[1] * r + rgb2[1] * ir,
            rgb1[2] * r + rgb2[2] * ir
        );

        return color;

    }

    /**
     * Returns the string corresponding to the given {@code key} into the
     * {@link #COLORS_BUNDLE} resource.
     *
     * @param key The resource key string.
     * @return The string corresponding to the given {@code key}.
     */
    private static String colorString ( String key ) {
        return ResourceBundle.getBundle(COLORS_BUNDLE, Locale.getDefault(), ColorUtilities.class.getClassLoader()).getString(key);
    }

    /**
     * Returns a color chooser panel for pastel colors.
     * The returned panel should be registered with a swing {@code JColorChooser}
     * by means of the {@code addChooserPanel} method.
     *
     * @return A color chooser panel for pastel colors.
     */
    public static AbstractColorChooserPanel createPastelChooserPanel () {
        return new PastelChooserPanel();
    }

    /**
     * Returns a color chooser panel for X-Window colors.
     * The returned panel should be registered with a swing {@code JColorChooser}
     * by means of the {@code addChooserPanel} method.
     *
     * @return A color chooser panel for X-Window colors.
     */
    public static AbstractColorChooserPanel createXChooserPanel () {
        return new XChooserPanel();
    }

    /**
     * Make a color darker.
     *
     * @param color    color to make darker.
     * @param fraction Darkness fraction. If 0.0 the color remains the same.
     *                 If 1.0 black is returned.
     * @return Darker color.
     */
    public static Color darker ( Color color, double fraction ) {

        int red = (int) Math.round(color.getRed() * ( 1.0 - fraction ));
        int green = (int) Math.round(color.getGreen() * ( 1.0 - fraction ));
        int blue = (int) Math.round(color.getBlue() * ( 1.0 - fraction ));
        int alpha = color.getAlpha();

        if ( red < 0 ) {
            red = 0;
        } else if ( red > 255 ) {
            red = 255;
        }

        if ( green < 0 ) {
            green = 0;
        } else if ( green > 255 ) {
            green = 255;
        }

        if ( blue < 0 ) {
            blue = 0;
        } else if ( blue > 255 ) {
            blue = 255;
        }

        return new Color(red, green, blue, alpha);

    }

    /**
     * Returns a string representation of {@code color} as a
     * RGB integer value.
     *
     * @param color The color to be represented as {@code String}.
     * @return A string representation of the given color.
     */
    public static String formatColor ( Color color ) {

        if ( color == null ) {
            return "";
        }

        //	Check for pastel colors.
        for ( int i = 0; i < getPastelColorsCount(); i++ ) {
            if ( getPastelColor(i).equals(color) ) {
                return getPastelColorName(i);
            }
        }

        //	Check for X-Window colors.
        for ( int i = 0; i < getXColorsCount(); i++ ) {
            if ( getXColor(i).equals(color) ) {
                return getXColorName(i);
            }
        }

        //	Check for standard colors.
        for ( int i = 0; i < getStandardColorsCount(); i++ ) {
            if ( getStandardColor(i).equals(color) ) {
                return getStandardColorName(i);
            }
        }

        return getHexRGBString(color);

    }

    /**
     * Returns a {@code Comparator} that sorts {@code Color} objects
     * by their blue component.
     *
     * @return A {@code Comparator} that sorts {@code Color} objects
     *         by their blue component.
     */
    public static Comparator<Color> getBlueComparator () {
        return BLUE_COMPARATOR;
    }

    /**
     * Returns a {@code Comparator} that sorts {@code Color} objects
     * by their brightness value.
     *
     * @return A {@code Comparator} that sorts {@code Color} objects
     *         by their brightness value.
     */
    public static Comparator<Color> getBrightnessComparator () {
        return BRIGHTNESS_COMPARATOR;
    }

    /**
     * Obtain a {@link Paint} instance which draws a checker
     * background of gray and white.
     * <P>
     * <B>Note:</B> The returned instance may be shared.
     *
     * @return A {@link Paint} implementation.
     */
    public static Paint getCheckerPaint () {
        return org.jdesktop.swingx.util.PaintUtils.getCheckerPaint();
    }

    /**
     * Obtain a {@link Paint} instance which draws a checker
     * background of gray and white.
     * <P>
     * <B>Note:</B> The returned instance may be shared.
     *
     * @param c1   The first color of the checker.
     * @param c2   The second color of the checker.
     * @param size The size in pixels of the checker.
     * @return A {@link Paint} implementation.
     */
    public static Paint getCheckerPaint ( Color c1, Color c2, int size ) {
        return org.jdesktop.swingx.util.PaintUtils.getCheckerPaint(c1, c2, size);
    }

    /**
     * Return the "distance" between two colors. The RGB entries are taken
     * to be coordinates in a 3D space [0.0, 1.0], and this method returns
     * the distance between the coordinates for the first and second color.
     *
     * @param r1 First color's red component.
     * @param g1 First color's green component.
     * @param b1 First color's blue component.
     * @param r2 Second color's red component.
     * @param g2 Second color's green component.
     * @param b2 Second color's blue component.
     * @return Distance between colors.
     */
    public static double getDistance ( double r1, double g1, double b1, double r2, double g2, double b2 ) {

        double a = r2 - r1;
        double b = g2 - g1;
        double c = b2 - b1;

        return Math.sqrt(a * a + b * b + c * c);

    }

    /**
     * Return the "distance" between two colors. The RGBA entries are taken
     * to be coordinates in a 4D space [0.0, 1.0], and this method returns
     * the distance between the coordinates for the first and second color.
     *
     * @param r1 First color's red component.
     * @param g1 First color's green component.
     * @param b1 First color's blue component.
     * @param a1 First color's alpha component.
     * @param r2 Second color's red component.
     * @param g2 Second color's green component.
     * @param b2 Second color's blue component.
     * @param a2 Second color's alpha component.
     * @return Distance between colors.
     */
    public static double getDistance ( double r1, double g1, double b1, double a1, double r2, double g2, double b2, double a2 ) {

        double a = r2 - r1;
        double b = g2 - g1;
        double c = b2 - b1;
        double d = a2 - a1;

        return Math.sqrt(a * a + b * b + c * c + d * d);

    }

    /**
     * Return the "distance" between two colors.
     *
     * @param color1 First color [r,g,b] or [r,g,b,a].
     * @param color2 Second color [r,g,b] or [r,g,b,a].
     * @return Distance between colors.
     */
    public static double getDistance ( double[] color1, double[] color2 ) {
        if ( color1.length >= 4 && color2.length >= 4 ) {
            return getDistance(color1[0], color1[1], color1[2], color1[3], color2[0], color2[1], color2[2], color2[3]);
        } else {
            return getDistance(color1[0], color1[1], color1[2], color2[0], color2[1], color2[2]);
        }
    }

    /**
     * Return the "distance" between two colors.
     *
     * @param color1 First color.
     * @param color2 Second color.
     * @return Distance between colors.
     */
    public static double getDistance ( Color color1, Color color2 ) {

        float rgb1[] = new float[4];
        float rgb2[] = new float[4];

        color1.getRGBComponents(rgb1);
        color2.getRGBComponents(rgb2);

        return getDistance(rgb1[0], rgb1[1], rgb1[2], rgb1[3], rgb2[0], rgb2[1], rgb2[2], rgb2[3]);

    }

    /**
     * Returns a {@code Comparator} that sorts {@code Color} objects
     * by their green component.
     *
     * @return A {@code Comparator} that sorts {@code Color} objects
     *         by their green component.
     */
    public static Comparator<Color> getGreenComparator () {
        return GREEN_COMPARATOR;
    }

    /**
     * Returns a string representation of {@code color} as hexadecimal
     * RGB {@code String}.
     *
     * @param color The color to be represented as {@code String}.
     * @return A string in the following format: {@code 0xRRGGBB}.
     *         If {@code color == null} an empty string is returned.
     */
    public static String getHexRGBString ( Color color ) {

        if ( color == null ) {
            return "";
        }

        int r = color.getRed();
        int g = color.getGreen();
        int b = color.getBlue();

        return "0x"
            + ( ( r < 16 ) ? "0" : "" ) + Integer.toHexString(r).toUpperCase()
            + ( ( g < 16 ) ? "0" : "" ) + Integer.toHexString(g).toUpperCase()
            + ( ( b < 16 ) ? "0" : "" ) + Integer.toHexString(b).toUpperCase();

    }

    /**
     * Returns a string representation of {@code color} as hexadecimal
     * RGB {@code String} in HTML format.
     *
     * @param color The color to be represented as {@code String}.
     * @return A string in the following format: {@code #RRGGBB}.
     *         If {@code color == null} an empty string is returned.
     */
    public static String getHtmlRGBString ( Color color ) {

        if ( color == null ) {
            return "";
        }

        int r = color.getRed();
        int g = color.getGreen();
        int b = color.getBlue();

        return "#"
            + ( ( r < 16 ) ? "0" : "" ) + Integer.toHexString(r).toUpperCase()
            + ( ( g < 16 ) ? "0" : "" ) + Integer.toHexString(g).toUpperCase()
            + ( ( b < 16 ) ? "0" : "" ) + Integer.toHexString(b).toUpperCase();

    }

    /**
     * Returns a {@code Comparator} that sorts {@code Color} objects
     * by their hue value.
     *
     * @return A {@code Comparator} that sorts {@code Color} objects
     *         by their hue value.
     */
    public static Comparator<Color> getHueComparator () {
        return HUE_COMPARATOR;
    }

    /**
     * Returns a {@code Comparator} that sorts {@code Color} objects
     * by their names.
     *
     * @return A {@code Comparator} that sorts {@code Color} objects
     *         by their names.
     */
    public static Comparator<Color> getNameComparator () {
        return NAME_COMPARATOR;
    }

    /**
     * Returns the pastel color corresponding to the given {@code index}
     * if {@code index &gt;=0 && index &lt;} {@link ColorUtilities#getPastelColorsCount()},
     * or {@code null}.
     *
     * @param index The index inside the pastel colors table.
     * @return The {@code index}-th pastel {@link Color} or {@code null}.
     */
    public static Color getPastelColor ( int index ) {
        if ( index < 0 || index >= getPastelColorsCount() ) {
            return null;
        } else {
            return PASTELS_COLORS[index];
        }
    }

    /**
     * If {@code color} is a pastel color then returns an index that can be
     * used to refer the {@code color} inside the array/{@code Vector}
     * returned by the {@code getPastelColorsArray}/{@code getPastelColorsVector}
     * method. If {@code color} isn't a pastel color (i.e. the {@code isPastelColor}
     * method returns {@code false}), -1 is returned.
     *
     * @param color The {@link Color} to be checked.
     * @return The index inside the pastel colors table or -1.
     */
    public static int getPastelColorIndex ( Color color ) {
        if ( color != null ) {
            for ( int i = 0; i < getPastelColorsCount(); i++ ) {
                if ( getPastelColor(i).equals(color) ) {
                    return i;
                }
            }
        }
        return - 1;
    }

    /**
     * Returns the pastel color name corresponding to the given {@code index}
     * if {@code index >=0 && index < getPastelColorsCount()},
     * or {@code null}.
     *
     * @param index The index inside the pastel colors table.
     * @return The name of {@code index}-th pastel {@link Color} or {@code null}.
     */
    public static String getPastelColorName ( int index ) {
        if ( index < 0 || index >= getPastelColorsCount() ) {
            return null;
        } else {
            return PASTELS_COLOR_NAMES[index];
        }
    }

    /**
     * Returns the array of pastel colors.
     *
     * @return The pastel {@link Color}s table.
     */
	@SuppressWarnings( "ReturnOfCollectionOrArrayField" )
    public static Color[] getPastelColorsArray () {
        return PASTELS_COLORS;
    }

    /**
     * Returns the number of pastel colors.
     *
     * @return The number of pastel colors.
     */
    public static int getPastelColorsCount () {
        return PASTELS_COLORS.length;
    }

    /**
     * Returns the vector of pastel colors.
     *
     * @return The pastel {@link Color}s table.
     */
    @SuppressWarnings( "UseOfObsoleteCollectionType" )
    public static Vector<Color> getPastelColorsVector () {

        Vector<Color> retValue = new Vector<>(getPastelColorsCount());

        for ( int i = 0; i < getPastelColorsCount(); i++ ) {
            retValue.add(getPastelColor(i));
        }

        return retValue;

    }

    /**
     * Returns a {@code Comparator} that sorts {@code Color} objects
     * by their red component.
     *
     * @return A {@code Comparator} that sorts {@code Color} objects
     *         by their red component.
     */
    public static Comparator<Color> getRedComparator () {
        return RED_COMPARATOR;
    }

    /**
     * Returns a string representation of {@code color} as comma-separated
     * RGB components inside square brackets.
     *
     * @param color The color to be represented as {@code String}.
     * @return A string in the following format: {@code [r, g, b]}.
     *         If {@code color == null} an empty string is returned.
     */
    public static String getRGBString ( Color color ) {

        if ( color == null ) {
            return "";
        }

        //	Check for pastel colors.
        for ( int i = 0; i < getPastelColorsCount(); i++ ) {
            if ( getPastelColor(i).equals(color) ) {
                return getPastelColorName(i);
            }
        }

        //	Check for X-Window colors.
        for ( int i = 0; i < getXColorsCount(); i++ ) {
            if ( getXColor(i).equals(color) ) {
                return getXColorName(i);
            }
        }

        //	Check for standard colors.
        for ( int i = 0; i < getStandardColorsCount(); i++ ) {
            if ( getStandardColor(i).equals(color) ) {
                return getStandardColorName(i);
            }
        }

        //	Create RGB string.
        StringBuilder retValue = new StringBuilder(15);

        retValue.append("[");
        retValue.append(color.getRed());
        retValue.append(", ");
        retValue.append(color.getGreen());
        retValue.append(", ");
        retValue.append(color.getBlue());
        retValue.append("]");

        return retValue.toString();

    }

    /**
     * Returns a string representation of {@code color} as comma-separated
     * RGBA components inside square brackets.
     *
     * @param color The color to be represented as {@code String}.
     * @return A string in the following format: {@code [r, g, b, a]}.
     *         If {@code color == null} an empty string is returned.
     */
    public static String getRGBAString ( Color color ) {

        if ( color == null ) {
            return "";
        }

        //	Check for pastel colors.
        for ( int i = 0; i < getPastelColorsCount(); i++ ) {
            if ( getPastelColor(i).equals(color) ) {
                return getPastelColorName(i);
            }
        }

        //	Check for X-Window colors.
        for ( int i = 0; i < getXColorsCount(); i++ ) {
            if ( getXColor(i).equals(color) ) {
                return getXColorName(i);
            }
        }

        //	Check for standard colors.
        for ( int i = 0; i < getStandardColorsCount(); i++ ) {
            if ( getStandardColor(i).equals(color) ) {
                return getStandardColorName(i);
            }
        }

        //	Create RGBA string.
        StringBuilder retValue = new StringBuilder(20);

        retValue.append("[");
        retValue.append(color.getRed());
        retValue.append(", ");
        retValue.append(color.getGreen());
        retValue.append(", ");
        retValue.append(color.getBlue());
        retValue.append(", ");
        retValue.append(color.getAlpha());
        retValue.append("]");

        return retValue.toString();

    }

    /**
     * Returns a {@code Comparator} that sorts {@code Color} objects
     * by their saturation value.
     *
     * @return A {@code Comparator} that sorts {@code Color} objects
     *         by their saturation value.
     */
    public static Comparator<Color> getSaturationComparator () {
        return SATURATION_COMPARATOR;
    }

    /**
     * Returns the standard color corresponding to the given {@code index}
     * if {@code index >=0 && index < getStandardColorsCount()},
     * or {@code null}.
     *
     * @param index The index inside the standard colors table.
     * @return The standard color corresponding to the given {@code index}
     *         or {@code null}.
     */
    public static Color getStandardColor ( int index ) {
        if ( index < 0 || index >= getStandardColorsCount() ) {
            return null;
        } else {
            return STANDARD_COLORS[index];
        }
    }

    /**
     * If {@code color} is a standard color then returns an index that can be
     * used to refer the {@code color} inside the array/{@code Vector}
     * returned by the {@code getStandardColorsArray}/{@code getStandardColorsVector}
     * method. If {@code color} isn't a standard color (i.e. the {@code isStandardColor}
     * method returns {@code false}), -1 is returned.
     *
     * @param color The color to be checked.
     * @return The index inside the standard colors table or -1.
     */
    public static int getStandardColorIndex ( Color color ) {
        if ( color != null ) {
            for ( int i = 0; i < getStandardColorsCount(); i++ ) {
                if ( getStandardColor(i).equals(color) ) {
                    return i;
                }
            }
        }
        return - 1;
    }

    /**
     * Returns the standard color name corresponding to the given {@code index}
     * if {@code index >=0 && index < getStandardColorsCount()},
     * or {@code null}.
     *
     * @param index The index inside the standard colors table.
     * @return The name of standard color corresponding to the given {@code index}
     *         or {@code null}.
     */
    public static String getStandardColorName ( int index ) {
        if ( index < 0 || index >= getStandardColorsCount() ) {
            return null;
        } else {
            return STANDARD_COLOR_NAMES[index];
        }
    }

    /**
     * Returns the array of standard colors.
     *
     * @return The standard {@link  Color}s table.
     */
	@SuppressWarnings( "ReturnOfCollectionOrArrayField" )
    public static Color[] getStandardColorsArray () {
        return STANDARD_COLORS;
    }

    /**
     * Returns the number of standard colors.
     *
     * @return The number of standard colors.
     */
    public static int getStandardColorsCount () {
        return STANDARD_COLORS.length;
    }

    /**
     * Returns the vector of standard colors.
     *
     * @return The standard {@link  Color}s table.
     */
    @SuppressWarnings( "UseOfObsoleteCollectionType" )
    public static Vector<Color> getStandardColorsVector () {

        Vector<Color> retValue = new Vector<>(getStandardColorsCount());

        for ( int i = 0; i < getStandardColorsCount(); i++ ) {
            retValue.add(getStandardColor(i));
        }

        return retValue;

    }

    /**
     * Returns the X-Window color corresponding to the given {@code index}
     * if {@code index >=0 && index < getXColorsCount()},
     * or {@code null}.
     *
     * @param index The index inside the X-Window colors table.
     * @return The color corresponding to the given {@code index}
     *         or {@code null}.
     */
    public static Color getXColor ( int index ) {
        if ( index < 0 || index >= getXColorsCount() ) {
            return null;
        } else {
            return X_COLORS[index];
        }
    }

    /**
     * If {@code color} is a X-Window color then returns an index that can be
     * used to refer the {@code color} inside the array/{@code Vector}
     * returned by the {@code getXColorsArray}/{@code getXColorsVector}
     * method. If {@code color} isn't a X-WIndow color (i.e. the {@code isXColor}
     * method returns {@code false}), -1 is returned.
     *
     * @param color The color to be checked.
     * @return The index inside the X-Window colors table or -1.
     */
    public static int getXColorIndex ( Color color ) {
        if ( color != null ) {
            for ( int i = 0; i < getXColorsCount(); i++ ) {
                if ( getXColor(i).equals(color) ) {
                    return i;
                }
            }
        }
        return - 1;
    }

    /**
     * Returns the X-WIndow color name corresponding to the given {@code index}
     * if {@code index >=0 && index < getXColorsCount()},
     * or {@code null}.
     *
     * @param index The index inside the X-Window colors table.
     * @return The name of color corresponding to the given {@code index}
     *         or {@code null}.
     */
    public static String getXColorName ( int index ) {
        if ( index < 0 || index >= getXColorsCount() ) {
            return null;
        } else {
            return X_COLOR_NAMES[index];
        }
    }

    /**
     * Returns the array of X-WIndow colors.
     *
     * @return The X-WIndow {@link Color}s table.
     */
	@SuppressWarnings( "ReturnOfCollectionOrArrayField" )
    public static Color[] getXColorsArray () {
        return X_COLORS;
    }

    /**
     * Returns the number of X-WIndow colors.
     *
     * @return The number of X-WIndow colors.
     */
    public static int getXColorsCount () {
        return X_COLORS.length;
    }

    /**
     * Returns the vector of X-WIndow colors.
     *
     * @return The X-WIndow {@link Color}s table.
     */
    @SuppressWarnings( "UseOfObsoleteCollectionType" )
    public static Vector<Color> getXColorsVector () {

        Vector<Color> retValue = new Vector<>(getXColorsCount());

        for ( int i = 0; i < getXColorsCount(); i++ ) {
            retValue.add(getXColor(i));
        }

        return retValue;

    }

    /**
     * Returns the inverted {@link Color} of the given one. Transparency (alpha)
     * is not changed.
     *
     * @param color The color to be inverted.
     * @return The inverted {@link Color} of the given one.
     */
    public static Color invert ( Color color ) {

        if ( color == null ) {
            return null;
        }

        int r = color.getRed();
        int g = color.getGreen();
        int b = color.getBlue();
        int a = color.getAlpha();

        return new Color(255 - r, 255 - g, 255 - b, a);

    }

    /**
     * Check if a color is more dark than light. Useful if an entity of
     * this color is to be labeled: Use white label on a "dark" color and
     * black label on a "light" color.
     *
     * @param r Red component of the color to check.
     * @param g Green component of the color to check.
     * @param b Blue component of the color to check.
     * @return {@code true} if this is a "dark" color, {@code false}
     *         otherwise.
     */
    public static boolean isDark ( double r, double g, double b ) {

        // Measure distance to white and black respectively
        double dWhite = getDistance(r, g, b, 1.0, 1.0, 1.0);
        double dBlack = getDistance(r, g, b, 0.0, 0.0, 0.0);

        return dBlack < dWhite;

    }

    /**
     * Check if a color is more dark than light. Useful if an entity of
     * this color is to be labeled: Use white label on a "dark" color and
     * black label on a "light" color.
     *
     * @param r Red component of the color to check.
     * @param g Green component of the color to check.
     * @param b Blue component of the color to check.
     * @param a Alpha component of the color to check.
     * @return {@code true} if this is a "dark" color, {@code false}
     *         otherwise.
     */
    public static boolean isDark ( double r, double g, double b, double a ) {

        // Measure distance to white and black respectively
        double dWhite = getDistance(r, g, b, 1.0 - a, 1.0, 1.0, 1.0, 1.0);
        double dBlack = getDistance(r, g, b, 1.0 - a, 0.0, 0.0, 0.0, 0.0);

        return dBlack < dWhite;

    }

    /**
     * Check if a color is more dark than light. Useful if an entity of
     * this color is to be labeled: Use white label on a "dark" color and
     * black label on a "light" color.
     *
     * @param color color to check.
     * @return {@code true} if this is a "dark" color, {@code false}
     *         otherwise.
     */
    public static boolean isDark ( Color color ) {

        float r = color.getRed() / 255.0F;
        float g = color.getGreen() / 255.0F;
        float b = color.getBlue() / 255.0F;
        float a = color.getAlpha() / 255.0F;

        return isDark(r, g, b, a);

    }

    /**
     * Returns {@code true} if {@code color} is grey, i.e. the
     * 3 components (red, green and blue) are equal.
     *
     * @param color The {@link Color} to be checked.
     * @return {@code true} it the given {@code color}'s red, green and blue
     *         components are equal.
     */
    public static boolean isGray ( Color color ) {
        if ( color != null ) {
            return ( color.getRed() == color.getGreen() ) && ( color.getRed() == color.getBlue() );
        }
        return false;
    }

    /**
     * Returns {@code true} if {@code color} is a pastel color.
     *
     * @param color The {@link Color} to be checked.
     * @return {@code true} if {@code color} is a pastel color.
     */
    public static boolean isPastelColor ( Color color ) {
        if ( color != null ) {
            for ( int i = 0; i < getPastelColorsCount(); i++ ) {
                if ( getPastelColor(i).equals(color) ) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns {@code true} if {@code color} is a standard color.
     *
     * @param color The {@link Color} to be checked.
     * @return {@code true} if {@code color} is a standard color.
     */
    public static boolean isStandardColor ( Color color ) {
        if ( color != null ) {
            for ( int i = 0; i < getStandardColorsCount(); i++ ) {
                if ( getStandardColor(i).equals(color) ) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns {@code true} if {@code color} is a X-Window color.
     *
     * @param color The {@link Color} to be checked.
     * @return {@code true} if {@code color} is a X-Window color.
     */
    public static boolean isXColor ( Color color ) {
        if ( color != null ) {
            for ( int i = 0; i < getXColorsCount(); i++ ) {
                if ( getXColor(i).equals(color) ) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Make a color lighter.
     *
     * @param color    color to make lighter.
     * @param fraction Lightness fraction. If 0.0 the color remains the same.
     * @return Lighter color.
     */
    public static Color lighter ( Color color, double fraction ) {

        int red = (int) Math.round(color.getRed() * ( 1.0 + fraction ));
        int green = (int) Math.round(color.getGreen() * ( 1.0 + fraction ));
        int blue = (int) Math.round(color.getBlue() * ( 1.0 + fraction ));
        int alpha = color.getAlpha();

        if ( red < 0 ) {
            red = 0;
        } else if ( red > 255 ) {
            red = 255;
        }

        if ( green < 0 ) {
            green = 0;
        } else if ( green > 255 ) {
            green = 255;
        }

        if ( blue < 0 ) {
            blue = 0;
        } else if ( blue > 255 ) {
            blue = 255;
        }

        return new Color(red, green, blue, alpha);

    }

    /**
     * Creates a new {@code Color} object with the same red, green and
     * blue component of the {@code source} color, but without the alpha one.
     *
     * @param source The {@code Color} that must be made transparent.
     * @return A newly created {@code Color}, opaque version of
     *         {@code source}.
     */
    public static Color makeOpaque ( Color source ) {
        return new Color(
            source.getRed(),
            source.getGreen(),
            source.getBlue()
        );
    }

    /**
     * Creates a new {@code Color} object with the same red, green and
     * blue component of the {@code source} color, and the given
     * {@code alpha} component.
     *
     * @param source The {@code Color} that must be made transparent.
     * @param alpha  The transparency index (0: full transparent, 255: opaque).
     * @return A newly created {@code Color}, transparent version of
     *         {@code source}.
     */
    public static Color makeTransparent ( Color source, int alpha ) {
        return new Color(
            source.getRed(),
            source.getGreen(),
            source.getBlue(),
            Math.max(0, Math.min(255, alpha)) //	Clamp alpha between 0 and 255.
        );
    }

    /**
     * Returns the {@code source} color with the <I>alpha</I> component
     * modified adding the given {@code offset}.
     *
     * @param source The original {@code color}.
     * @param offset The <I>alpha</I> component offset.
     * @return The {@code source} color with the <I>alpha</I> component
     *         modified by the given {@code offset}.
     */
    public static Color offsetAlpha ( Color source, int offset ) {
        return offsetRGBA(source, 0, 0, 0, offset);
    }

    /**
     * Returns the {@code source} color with the <I>blue</I> component
     * modified adding the given {@code offset}.
     *
     * @param source The original {@code color}.
     * @param offset The <I>blue</I> component offset.
     * @return The {@code source} color with the <I>blue</I> component
     *         modified by the given {@code offset}.
     */
    public static Color offsetBlue ( Color source, int offset ) {
        return offsetRGB(source, 0, 0, offset);
    }

    /**
     * Returns the {@code source} color with the <I>brightness</I> component
     * modified adding the given {@code offset}.
     *
     * @param source The original {@code color}.
     * @param offset The <I>brightness</I> component offset.
     * @return The {@code source} color with the <I>brightness</I> component
     *         modified by the given {@code offset}.
     */
    public static Color offsetBrightness ( Color source, float offset ) {
        return offsetHSB(source, 0.0F, 0.0F, offset);
    }

    /**
     * Returns the {@code source} color with the <I>green</I> component
     * modified adding the given {@code offset}.
     *
     * @param source The original {@code color}.
     * @param offset The <I>green</I> component offset.
     * @return The {@code source} color with the <I>green</I> component
     *         modified by the given {@code offset}.
     */
    public static Color offsetGreen ( Color source, int offset ) {
        return offsetRGB(source, 0, offset, 0);
    }

    /**
     * Returns the {@code source} color with its components
     * modified adding the given offsets.
     *
     * @param source  The original {@code color}.
     * @param hOffset The <I>hue</I> component offset.
     * @param sOffset The <I>saturation</I> component offset.
     * @param bOffset The <I>brightness</I> component offset.
     * @return The {@code source} color with its components
     *         modified adding the given offsets.
     */
    public static Color offsetHSB ( Color source, float hOffset, float sOffset, float bOffset ) {
        return offsetHSBA(source, hOffset, sOffset, bOffset, 0);
    }

    /**
     * Returns the {@code source} color with its components
     * modified adding the given offsets.
     *
     * @param source  The original {@code color}.
     * @param hOffset The <I>hue</I> component offset.
     * @param sOffset The <I>saturation</I> component offset.
     * @param bOffset The <I>brightness</I> component offset.
     * @param aOffset The <I>alpha</I> component offset.
     * @return The {@code source} color with its components
     *         modified adding the given offsets.
     */
    public static Color offsetHSBA ( Color source, float hOffset, float sOffset, float bOffset, int aOffset ) {

        float hsb[] = Color.RGBtoHSB(source.getRed(), source.getGreen(), source.getBlue(), new float[3]);
        Color retValue = Color.getHSBColor(
            hOffset + hsb[0],
            Math.max(Math.min(sOffset + hsb[1], 1.0F), 0.0F),
            Math.max(Math.min(bOffset + hsb[2], 1.0F), 0.0F)
        );

        return new Color(
            retValue.getRed(),
            retValue.getGreen(),
            retValue.getBlue(),
            Math.max(Math.min(aOffset + source.getAlpha(), 255), 0)
        );

    }

    /**
     * Returns the {@code source} color with the <I>hue</I> component
     * modified adding the given {@code offset}.
     *
     * @param source The original {@code color}.
     * @param offset The <I>hue</I> component offset.
     * @return The {@code source} color with the <I>hue</I> component
     *         modified by the given {@code offset}.
     */
    public static Color offsetHue ( Color source, float offset ) {
        return offsetHSB(source, offset, 0.0F, 0.0F);
    }

    /**
     * Returns the {@code source} color with the <I>red</I> component
     * modified adding the given {@code offset}.
     *
     * @param source The original {@code color}.
     * @param offset The <I>red</I> component offset.
     * @return The {@code source} color with the <I>red</I> component
     *         modified by the given {@code offset}.
     */
    public static Color offsetRed ( Color source, int offset ) {
        return offsetRGB(source, offset, 0, 0);
    }

    /**
     * Returns the {@code source} color with its components
     * modified adding the given offsets.
     *
     * @param source  original {@code color}.
     * @param rOffset The <I>red</I> component offset.
     * @param gOffset The <I>green</I> component offset.
     * @param bOffset The <I>blue</I> component offset.
     * @return The {@code source} color with its components
     *         modified by the given offsets.
     */
    public static Color offsetRGB ( Color source, int rOffset, int gOffset, int bOffset ) {
        return offsetRGBA(source, rOffset, gOffset, bOffset, 0);
    }

    /**
     * Returns the {@code source} color with its components
     * modified adding the given offsets.
     *
     * @param source  original {@code color}.
     * @param rOffset The <I>red</I> component offset.
     * @param gOffset The <I>green</I> component offset.
     * @param bOffset The <I>blue</I> component offset.
     * @param aOffset The <I>alpha</I> component offset.
     * @return The {@code source} color with its components
     *         modified by the given offsets.
     */
    public static Color offsetRGBA ( Color source, int rOffset, int gOffset, int bOffset, int aOffset ) {
        return new Color(
            Math.max(Math.min(rOffset + source.getRed(), 255), 0),
            Math.max(Math.min(gOffset + source.getGreen(), 255), 0),
            Math.max(Math.min(bOffset + source.getBlue(), 255), 0),
            Math.max(Math.min(aOffset + source.getAlpha(), 255), 0)
        );
    }

    /**
     * Returns the {@code source} color with the <I>saturation</I> component
     * modified adding the given {@code offset}.
     *
     *
     * @param source The original {@code color}.
     * @param offset The <I>saturation</I> component offset.
     * @return The {@code source} color with the <I>saturation</I> component
     *         modified by the given {@code offset}.
     */
    public static Color offsetSaturation ( Color source, float offset ) {
        return offsetHSB(source, 0.0F, offset, 0.0F);
    }

    /**
     * Parse the given {@code String} for a standard or a pastel color or
     * for an integer representing a 24-bit opaque color specification. This
     * method handles string formats that are used to represent octal and
     * hexadecimal numbers. Returns {@code null} if the string doesn't
     * correspond to a color.
     *
     * @param color The color string representation.
     * @return The decoded {@link Color} or (@code null}.
     */
    public static Color parseColor ( String color ) {

        for ( int i = 0; i < PASTELS_COLOR_NAMES.length; i++ ) {
            if ( PASTELS_COLOR_NAMES[i].equalsIgnoreCase(color) ) {
                return PASTELS_COLORS[i];
            }
        }

        for ( int i = 0; i < X_COLOR_NAMES.length; i++ ) {
            if ( X_COLOR_NAMES[i].equalsIgnoreCase(color) ) {
                return X_COLORS[i];
            }
        }

        for ( int i = 0; i < STANDARD_COLOR_NAMES.length; i++ ) {
            if ( STANDARD_COLOR_NAMES[i].equalsIgnoreCase(color) ) {
                return STANDARD_COLORS[i];
            }
        }

        try {
            return Color.decode(color);
        } catch ( NumberFormatException nfex ) {
        }

        return null;

    }

    /**
     * Shows a modal color-chooser dialog and blocks until the
     * dialog is hidden. If the user presses the "OK" button, then
     * this method hides/disposes the dialog and returns the selected color.
     * If the user presses the "Cancel" button or closes the dialog without
     * pressing "OK", then this method hides/disposes the dialog and returns
     * {@code null}.
     *
     * @param component    The parent {@code Component} for the dialog.
     * @param title        The {@code String} containing the dialog's title.
     * @param initialColor The initial {@link Color} set when the color-chooser is shown.
     * @return The selected color or {@code null} if the user opted out.
     * @exception HeadlessException If {@code GraphicsEnvironment.isHeadless()}
     *                              returns {@code true}.
     * @see java.awt.GraphicsEnvironment#isHeadless
     */
    public static Color showDialog ( Component component, String title, Color initialColor )
        throws HeadlessException {

        final JColorChooser pane = new JColorChooser(initialColor != null ? initialColor : Color.white);

        pane.addChooserPanel(createPastelChooserPanel());
        pane.addChooserPanel(createXChooserPanel());

        ColorTracker tracker = new ColorTracker(pane);
        JDialog dialog = JColorChooser.createDialog(component, title, true, pane, tracker, null);

        dialog.addWindowListener(
            new WindowAdapter() {
                @Override
                public void windowClosing ( WindowEvent e ) {
                    e.getWindow().setVisible(false);
                }
            }
        );
        dialog.addComponentListener(
            new ComponentAdapter() {
                @Override
                public void componentHidden ( ComponentEvent e ) {
                    ( (Window) e.getComponent() ).dispose();
                }
            }
        );

        dialog.setVisible(true);	//	Blocks until user brings dialog down...

        return tracker.getColor();

    }

    /**
     * Defines a {@code JColorChooser} panel.
     */
    @SuppressWarnings( "serial" )
    protected static abstract class ChooserPanel
        extends AbstractColorChooserPanel {

        private static final String SORT_BY_BLUE = "Blue Component";
        private static final String SORT_BY_BRIGHTNESS = "Brightness";
        private static final String SORT_BY_GREEN = "Green Component";
        private static final String SORT_BY_HUE = "Hue";
        private static final String SORT_BY_NAME = "Name";
        private static final String SORT_BY_RED = "Red Component";
        private static final String SORT_BY_SATURATION = "Saturation";
        private static final String SORT_MODES[] = new String[] {
            ChooserPanel.SORT_BY_NAME,
            ChooserPanel.SORT_BY_HUE,
            ChooserPanel.SORT_BY_SATURATION,
            ChooserPanel.SORT_BY_BRIGHTNESS,
            ChooserPanel.SORT_BY_RED,
            ChooserPanel.SORT_BY_GREEN,
            ChooserPanel.SORT_BY_BLUE
        };
 
        private JList<Color> colorList = null;

        private final Color colors[];
        @SuppressWarnings( "UseOfObsoleteCollectionType" )
        private final Vector<Color> colorsVector;
        private boolean updating = false;

        ChooserPanel ( Color[] colors, @SuppressWarnings( "UseOfObsoleteCollectionType" ) Vector<Color> colorsVector ) {

            super();

            this.colors = colors;
            this.colorsVector = colorsVector;

            setLayout(new BorderLayout(6, 6));

        }

        @Override
        protected void buildChooser () {

            colorList = new JList<>(colors);

            colorList.setLayoutOrientation(JList.VERTICAL_WRAP);
            colorList.setCellRenderer(new ColorCellRenderer());
            colorList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            colorList.addListSelectionListener(e -> {
                if ( !isUpdating() ) {
                    getColorSelectionModel().setSelectedColor(colorList.getSelectedValue());
                }
            });

            JPanel northPane = new JPanel(new FlowLayout(FlowLayout.CENTER));
            JScrollPane scrollPane = new JScrollPane(colorList);
            JLabel sortCaption = new JLabel(colorString("ColorUtilities.ChooserPanel.buildChooser.sortCaption"));
            JComboBox<String> sortCombo = new WideComboBox<>(SORT_MODES);
            JPanel sortPanel = new JPanel(new BorderLayout(6, 3));

            sortCombo.addItemListener(e -> {
                if ( e.getStateChange() == ItemEvent.SELECTED ) {
                    sortList((String) e.getItem());
                }
            });
            sortCombo.setSelectedItem(ChooserPanel.SORT_BY_NAME);

            sortPanel.add(sortCaption, BorderLayout.WEST);
            sortPanel.add(sortCombo, BorderLayout.CENTER);

            northPane.add(sortPanel);

            scrollPane.setPreferredSize(new Dimension(400, 162));

            add(northPane, BorderLayout.NORTH);
            add(scrollPane, BorderLayout.CENTER);

            getColorSelectionModel().addChangeListener(e -> {
                updateChooser();
            });

        }

        @Override
        public Icon getLargeDisplayIcon () {
            return null;
        }

        @Override
        public Icon getSmallDisplayIcon () {
            return null;
        }

        protected boolean isUpdating () {
            return updating;
        }

        protected void setUpdating ( boolean updating ) {
            this.updating = updating;
        }

        private void sortList ( String mode ) {

            @SuppressWarnings( "UseOfObsoleteCollectionType" )
            Vector<Color> list = colorsVector;

            if ( ChooserPanel.SORT_BY_NAME.equals(mode) ) {
                Collections.sort(list, getNameComparator());
            }
            
            if ( null != mode ) {
                switch ( mode ) {
                    case ChooserPanel.SORT_BY_RED:
                        Collections.sort(list, getRedComparator());
                        Collections.reverse(list);
                        break;
                    case ChooserPanel.SORT_BY_GREEN:
                        Collections.sort(list, getGreenComparator());
                        Collections.reverse(list);
                        break;
                    case ChooserPanel.SORT_BY_BLUE:
                        Collections.sort(list, getBlueComparator());
                        Collections.reverse(list);
                        break;
                    case ChooserPanel.SORT_BY_HUE:
                        Collections.sort(list, getHueComparator());
                        break;
                    case ChooserPanel.SORT_BY_SATURATION:
                        Collections.sort(list, getSaturationComparator());
                        break;
                    case ChooserPanel.SORT_BY_BRIGHTNESS:
                        Collections.sort(list, getBrightnessComparator());
                        break;
                }
            }

            colorList.setListData(list);

            updateChooser();

        }

        @Override
        public void updateChooser () {
            setUpdating(true);
            colorList.setSelectedValue(getColorFromModel(), true);
            setUpdating(false);
        }

    }	
    /**
     * An abstract class for color comparison.
     */
    @SuppressWarnings( "EqualsAndHashcode" )
    protected static abstract class ColorComparator
        implements Comparator<Color> {

        @Override
        @SuppressWarnings( "EqualsWhichDoesntCheckParameterClass" )
        public boolean equals ( Object obj ) {
            return false;
        }

    };

    /**
     * Utility used by {@code showDialog}.
     */
    protected static class ColorTracker
        implements ActionListener {

        JColorChooser chooser;
        Color color;

        ColorTracker ( JColorChooser c ) {
            chooser = c;
        }

        @Override
        public void actionPerformed ( ActionEvent e ) {
            color = chooser.getColor();
        }

        public Color getColor () {
            return color;
        }

    }	//	class ColorTracker

    /**
     * Defines a {@code JColorChooser} panel.
     */
    @SuppressWarnings( "serial" )
    protected static class PastelChooserPanel
        extends ChooserPanel {

        PastelChooserPanel () {
            super(
                getPastelColorsArray(),
                getPastelColorsVector()
            );
        }

        @Override
        public String getDisplayName () {
            return colorString("ColorUtilities.PastelChooserPanel.panelName");
        }

        @Override
        public int getDisplayedMnemonicIndex () {
            return Integer.parseInt(colorString("ColorUtilities.PastelChooserPanel.panelMnemonicIndex"));
        }

        @Override
        public int getMnemonic () {
            return colorString("ColorUtilities.PastelChooserPanel.panelMnemonic").charAt(0);
        }

    }	
    /**
     * Defines a {@code JColorChooser} panel.
     */
    @SuppressWarnings( "serial" )
    protected static class XChooserPanel
        extends ChooserPanel {

        XChooserPanel () {
            super(
                getXColorsArray(),
                getXColorsVector()
            );
        }

        @Override
        public String getDisplayName () {
            return colorString("ColorUtilities.XChooserPanel.panelName");
        }

        @Override
        public int getDisplayedMnemonicIndex () {
            return Integer.parseInt(colorString("ColorUtilities.XChooserPanel.panelMnemonicIndex"));
        }

        @Override
        public int getMnemonic () {
            return colorString("ColorUtilities.XChooserPanel.panelMnemonic").charAt(0);
        }

    }	
}	//	class ColorUtilities

